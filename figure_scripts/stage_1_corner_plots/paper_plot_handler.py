"""
Corner plot code

TODO fix that the data shown on the titles also contains the info of the red thing.
"""

import json
import os

import corner
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot
from hmc_project_code.functions.plotting.utility import align_axes
from hmc_project_code.projects.action_angle_scripts.functions.plot_routines.plot_sample_comparison_paper import (
    plot_sample_comparison_paper,
)
from tabulate import tabulate

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


###
def readout_sequence_results(filename):
    """
    Function to read out sample-set of samplers
    """

    with open(filename, "r") as f:
        data = json.loads(f.read())

    sequence_results = []

    # loop over all the individual sample set iterations
    for sample_set_i in data["samplers"]["aahmc_through_map"]["sample_set"].keys():
        sample_new_shape = {}

        # print(data.keys())
        # print(data['real_samples_set'].keys())

        # #
        # result_dict["real_samples_set"] = {}

        # # Add loop to get them
        # sample_set = {}
        # for i in range(n_samples):
        #     real_sample = np.random.multivariate_normal(
        #         mean=mu, cov=cov, size=sample_size
        #     )
        #     second_real_sample = np.random.multivariate_normal(
        #         mean=mu, cov=cov, size=sample_size
        #     )

        #     sample_set[i] = {
        #         "real_sample": real_sample,
        #         "second_real_sample": second_real_sample,
        #     }

        # result_dict["real_samples_set"]["sample_set"] = sample_set

        # Readout real sample
        sample_new_shape["real_sample"] = np.array(
            data["real_samples_set"]["sample_set"][sample_set_i]["real_sample"]
        )
        sample_new_shape["name_real_sample"] = "True"

        # Readout second real sample
        sample_new_shape["second_real_sample"] = np.array(
            data["real_samples_set"]["sample_set"][sample_set_i]["second_real_sample"]
        )
        sample_new_shape["name_second_real_sample"] = "True-2"

        # Nuts direct sampled:
        sample_new_shape["sample_1"] = np.array(
            data["samplers"]["nuts_on_target"]["sample_set"][sample_set_i]["samples"]
        )
        sample_new_shape["times_sample_1"] = data["samplers"]["nuts_on_target"][
            "sample_set"
        ][sample_set_i]["runtimes_array"]
        sample_new_shape["name_sample_1"] = "nuts_direct"
        sample_new_shape["display_name_sample_1"] = "NUTS"

        # # Nuts through map sampled:
        # sample_new_shape["sample_2"] = np.array(
        #     data["samplers"]["nuts_through_map"]["sample_set"][sample_set_i]["samples"]
        # )
        # sample_new_shape["times_sample_2"] = data["samplers"]["nuts_through_map"][
        #     "sample_set"
        # ][sample_set_i]["runtimes_array"]
        # sample_new_shape["name_sample_2"] = "nuts_through_map"
        # sample_new_shape["display_name_sample_2"] = "NUTS + map"

        # # Nuts through map sampled:
        # sample_new_shape["base_sample_2"] = np.array(
        #     data["samplers"]["nuts_through_map"]["sample_set"][sample_set_i][
        #         "samples_base"
        #     ]
        # )
        # sample_new_shape["name_base_sample_2"] = "nuts_through_map_base"
        # sample_new_shape["display_name_base_sample_2"] = "NUTS through map base"

        # AAHMC through map sampled:
        sample_new_shape["sample_3"] = np.array(
            data["samplers"]["aahmc_through_map"]["sample_set"][sample_set_i]["samples"]
        )
        sample_new_shape["times_sample_3"] = data["samplers"]["aahmc_through_map"][
            "sample_set"
        ][sample_set_i]["runtimes_array"]
        sample_new_shape["name_sample_3"] = "aahmc_through_map"
        sample_new_shape["display_name_sample_3"] = "AAHMC"

        # AAHMC through map sampled:
        sample_new_shape["base_sample_3"] = np.array(
            data["samplers"]["aahmc_through_map"]["sample_set"][sample_set_i][
                "samples_base"
            ]
        )
        sample_new_shape["name_base_sample_3"] = "aahmc_through_map base"
        sample_new_shape["display_name_base_sample_3"] = "AAHMC base"

        #
        sequence_results.append(sample_new_shape)

    return sequence_results


# def plot_sample_panels(sequence_result_set, plot_settings={}):
#     """
#     function to plot panels containg base and target samples for a sequence result dict.
#     """

#     ##################
#     # Set up figure logic
#     fig = plt.figure(figsize=(8, 20))
#     gs = fig.add_gridspec(nrows=5, ncols=2)

#     #
#     ax_nuts_direct = fig.add_subplot(gs[0, 1])

#     ax_nuts_map_base = fig.add_subplot(gs[1, 0])
#     ax_nuts_map_target = fig.add_subplot(gs[1, 1])

#     ax_aahmc_map_base = fig.add_subplot(gs[2, 0])
#     ax_aahmc_map_target = fig.add_subplot(gs[2, 1])

#     ax_real = fig.add_subplot(gs[3, 1])
#     ax_real_two = fig.add_subplot(gs[4, 1])

#     # Plot data:
#     ax_nuts_direct.scatter(
#         sequence_result_set["sample_1"][:, 0], sequence_result_set["sample_1"][:, 1]
#     )

#     # NUTS sampled
#     ax_nuts_map_base.scatter(
#         sequence_result_set["base_sample_2"][:, 0],
#         sequence_result_set["base_sample_2"][:, 1],
#     )
#     ax_nuts_map_target.scatter(
#         sequence_result_set["sample_2"][:, 0], sequence_result_set["sample_2"][:, 1]
#     )

#     # AAHMC sampled
#     ax_aahmc_map_base.scatter(
#         sequence_result_set["base_sample_3"][:, 0],
#         sequence_result_set["base_sample_3"][:, 1],
#     )
#     ax_aahmc_map_target.scatter(
#         sequence_result_set["sample_3"][:, 0], sequence_result_set["sample_3"][:, 1]
#     )

#     #
#     ax_real.scatter(
#         sequence_result_set["real_sample"][:, 0],
#         sequence_result_set["real_sample"][:, 1],
#     )
#     ax_real_two.scatter(
#         sequence_result_set["second_real_sample"][:, 0],
#         sequence_result_set["second_real_sample"][:, 1],
#     )

#     #
#     real_axes_list = [
#         ax_nuts_direct,
#         ax_nuts_map_target,
#         ax_aahmc_map_target,
#         ax_real,
#         ax_real_two,
#     ]
#     align_axes(fig, axes_list=real_axes_list, which_axis="x")
#     align_axes(fig, axes_list=real_axes_list, which_axis="y")

#     #
#     base_axes_list = [ax_nuts_map_base, ax_aahmc_map_base]
#     align_axes(fig, axes_list=base_axes_list, which_axis="x")
#     align_axes(fig, axes_list=base_axes_list, which_axis="y")

#     fig.tight_layout()

#     # Add info and plot the figure
#     show_and_save_plot(fig, plot_settings)


# def handle_paper_plot_sampler_comparison(filename, output_filename):
#     """
#     general paper plot handler
#     """

#     #
#     sequence_results = readout_sequence_results(filename)

#     plot_sample_comparison_paper(
#         sequence_results=sequence_results,
#         add_mean_evolution=False,
#         add_variance_evolution=False,
#         add_skewness_evolution=False,
#         add_kurtosis_evolution=False,
#         add_wasserstein_evolution=True,
#         add_ESS_evolution=False,
#         add_time_evolution=True,
#         add_confidence_interval_overlap=False,
#         verbose=1,
#         log_xscale=True,
#         plot_settings={
#             "show_plot": False,
#             "output_name": output_filename,
#             "add_second_confidence_interval": False,
#             "fontsize_axislabel": 80,
#             "fontsize_title": 80,
#             "fontsize_legend": 40,
#             "size_ticklabels": 60,
#             "include_subplot_titles": False,
#             "custom_time_evolution_ylims": [1e-2, 1e3],
#             "custom_time_evolution_yticks": [1e-2, 1e-1, 1e-0, 1e1, 1e2, 1e3],
#             "time_evolution_show_ratio": True,
#         },
#     )


def calculate_cov_matrix(samples):
    """
    Function to calculate the covariance matrix for a sample
    """

    #
    M = samples.shape[1]

    # Calculate the covariance matrix
    cov_matrix = np.cov(samples, rowvar=False)

    # You can convert the covariance matrix to a pandas DataFrame for better visualization
    cov_df = pd.DataFrame(
        cov_matrix,
        columns=[f"dim_{i+1}" for i in range(M)],
        index=[f"dim_{i+1}" for i in range(M)],
    )

    return cov_df.to_numpy()


def plot_cornerplot_comparison(filename, burning_length, sampler_name, plot_settings):
    """
    Function to plot the cornerplot-comparison between the true sample and the sampler results
    """

    ##########
    # Get sqeuence results
    sequence_results = readout_sequence_results(filename)

    # Get sample keys
    sample_keys = [
        key for key in sequence_results[0].keys() if key.startswith("sample_")
    ]
    display_names = {
        key: sequence_results[0]["display_name_{}".format(key)] for key in sample_keys
    }
    inverted_names = {value: key for key, value in display_names.items()}

    #######
    # Extract real samples
    real_sample_list = []
    for sequence in sequence_results:
        # load
        samples = np.array(sequence["real_sample"])

        #
        real_sample_list.append(samples)

    # Stack real samples
    real_sample_stacked = np.vstack(real_sample_list)

    #######
    # Extract real samples
    sampler_sample_list = []
    for sequence in sequence_results:
        #
        samples = np.array(sequence[inverted_names[sampler_name]])

        # slice if necessary
        samples = samples[burnin_length:]

        #
        sampler_sample_list.append(samples)

    # Stack real samples
    sampler_sample_stacked = np.vstack(sampler_sample_list)

    ############
    # Calculate statistics
    mean_vector_true_sample = np.mean(real_sample_stacked, axis=0)
    cov_matrix_true_sample = calculate_cov_matrix(samples=real_sample_stacked)

    mean_vector_sampler_sample = np.mean(sampler_sample_stacked, axis=0)
    cov_matrix_sampler_sample = calculate_cov_matrix(samples=sampler_sample_stacked)

    ########
    #
    print("True sample:")
    print("Mean vector:")
    print(mean_vector_true_sample)
    print("Covariance matrix:")
    # print(cov_matrix_true_sample)
    print(tabulate(cov_matrix_true_sample, floatfmt=".2f"))

    print("Sampler sample:")
    print("Mean vector:")
    print(mean_vector_sampler_sample)
    print("Covariance matrix:")
    # print(cov_matrix_sampler_sample)
    print(tabulate(cov_matrix_sampler_sample, floatfmt=".2f"))

    # TODO: implement something that plots the data on the figure
    # print(tabulate(cov_matrix_true_sample, tablefmt="latex", floatfmt=".2f"))
    # print(" \\\\\n".join([" & ".join(map('{0:.3f}'.format, line)) for line in a]))

    figure = plt.figure(figsize=(20, 20))

    #######################
    # Plot it.
    corner.corner(
        real_sample_stacked,
        quantiles=[0.16, 0.5, 0.84],
        show_titles=True,
        title_kwargs={"fontsize": 32},
        plot_density=False,
        plot_datapoints=False,
        color="black",
        fig=figure,
        weights=np.ones(len(real_sample_stacked)) / len(real_sample_stacked),
    )

    corner.corner(
        sampler_sample_stacked,
        quantiles=[0.16, 0.5, 0.84],
        show_titles=True,
        title_kwargs={"fontsize": 32},
        plot_density=False,
        plot_datapoints=False,
        fig=figure,
        color="red",
        weights=np.ones(len(sampler_sample_stacked)) / len(sampler_sample_stacked),
    )

    figure.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(figure, plot_settings)


if __name__ == "__main__":
    # Get filename
    hmc_data_root = os.path.join(os.getenv("PROJECT_DATA_ROOT"), "hmc_data")

    ##########
    # List MVN names
    MVN_names = [
        # "simple_2dMVN_0_sleep",
        # "simple_2dMVN_0.01_sleep",
        # "simple_3dMVN_0_sleep",
        # "simple_3dMVN_0.01_sleep",
        "simple_4dMVN_0_sleep",
        "simple_4dMVN_0.01_sleep",
    ]

    likelihood_types = [
        "autodiff",
        "numdiff",
    ]

    sampler_names = ["NUTS", "AAHMC"]

    ####################
    # Loop over all distributions
    for MVN in MVN_names:
        for likelihood_type in likelihood_types:
            filename = "/home/david/data_projects/AAHMC_data/stage_1_results/sampler_comparison_{}/{}_samplers.json".format(
                MVN, likelihood_type
            )
            for sampler_name in sampler_names:
                burnin_length = 500

                plot_cornerplot_comparison(
                    filename=filename,
                    sampler_name=sampler_name,
                    burning_length=burnin_length,
                    plot_settings={
                        "output_name": os.path.join(
                            this_file_dir,
                            "plots",
                            "{}_{}_{}_sampler_cornerplot_comparison.pdf".format(
                                MVN, likelihood_type, sampler_name
                            ),
                        )
                    },
                )

    # ###########
    # # simple 2d MVN Autodiff 0 sleep
    # filename = "/home/david/data_projects/AAHMC_data/stage_1_results/sampler_comparison_simple_2dMVN_0_sleep/autodiff_samplers.json"

    # #########
    # # simple_4dMVN_0.01_sleep numdiff
    # filename = "/home/david/data_projects/AAHMC_data/stage_1_results/sampler_comparison_simple_4dMVN_0.01_sleep/numdiff_samplers.json"
    # sampler_name = "AAHMC"
    # burnin_length = 500

    # plot_cornerplot_comparison(
    #     filename=filename,
    #     sampler_name=sampler_name,
    #     burning_length=burnin_length,
    #     plot_settings={
    #         "output_filename": os.path.join(this_file_dir, "plots", "simple_4dMVN_0.01_sleep_numdiff_{}_sampler_cornerplot_comparison.pdf".format(sampler_name))
    #     }
    # )
