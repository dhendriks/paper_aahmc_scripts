"""
Function to plot the sampling and the cumulative time

two main parts:
- sampling
  - W2 distances
  -

- times:
  - normal cumulative times
  - cumulative times plus map building
"""

import json
import os

import matplotlib.pyplot as plt
import numpy as np
from david_phd_functions.plotting import custom_mpl_settings
from hmc_project_code.functions.analysis.analysis_functions import get_wdist_multid
from scipy import interpolate

custom_mpl_settings.load_mpl_rc()


def get_median_percentiles(data):
    """
    Function to return the median and percentiles from the data
    """

    median = np.percentile(data, [50], axis=0)[0]
    percentiles = np.percentile(data, [15.89, 84.1, 2.27, 97.725], axis=0)

    return median, percentiles


def readout_data(dirname, diff_type, hexnum):
    """
    Function to readout the data
    """

    filename = f"{dirname}/{diff_type}_samplers_{hexnum}.json"
    map_building_filename = f"{dirname}/{diff_type}_map_building_{hexnum}.json"

    # extract sampling data
    with open(filename, "r") as f:
        data = json.loads(f.read())

    # extract map building data
    with open(map_building_filename, "r") as f:
        map_building_data = json.loads(f.read())

    nuts_data = data["samplers"]["nuts_on_target"]
    nuts_runtimes = nuts_data["sample_set"]["0"]["runtimes_array"]
    nuts_samples = np.array(nuts_data["sample_set"]["0"]["samples"])

    aahmc_data = data["samplers"]["aahmc_through_map"]
    aahmc_runtimes = aahmc_data["sample_set"]["0"]["runtimes_array"]
    aahmc_samples = np.array(aahmc_data["sample_set"]["0"]["samples"])

    real_data = np.array(data["real_samples_set"]["sample_set"]["0"]["real_sample"])
    second_real_data = np.array(
        data["real_samples_set"]["sample_set"]["0"]["second_real_sample"]
    )

    nuts_cumulative_times = np.cumsum(nuts_runtimes)
    aahmc_cumulative_times = np.cumsum(aahmc_runtimes)
    aahmc_cumulative_times_including_map_building = (
        aahmc_cumulative_times + map_building_data["total_time"]
    )

    realisation_data = {
        #
        "nuts_data": nuts_data,
        "nuts_runtimes": nuts_runtimes,
        "nuts_samples": nuts_samples,
        "nuts_cumulative_times": nuts_cumulative_times,
        #
        "aahmc_data": aahmc_data,
        "aahmc_runtimes": aahmc_runtimes,
        "aahmc_samples": aahmc_samples,
        "aahmc_cumulative_times": aahmc_cumulative_times,
        "aahmc_cumulative_times_including_map_building": aahmc_cumulative_times_including_map_building,
        #
        "real_data": real_data,
        "second_real_data": second_real_data,
    }

    return realisation_data


def get_w2_dists(realisation_data, indices):
    """
    Function to get W2 dists
    """

    w2_dists_nuts = []
    w2_dists_aahmc = []
    w2_dists_self = []

    for index in indices:

        w2dist_aahmc = get_wdist_multid(
            realisation_data["real_data"][:index].T,
            realisation_data["aahmc_samples"][:index].T,
        )
        w2_dists_aahmc.append(w2dist_aahmc)

        w2dist_nuts = get_wdist_multid(
            realisation_data["real_data"][:index].T,
            realisation_data["nuts_samples"][:index].T,
        )
        w2_dists_nuts.append(w2dist_nuts)

        w2dist_self = get_wdist_multid(
            realisation_data["real_data"][:index].T,
            realisation_data["second_real_data"][:index].T,
        )
        w2_dists_self.append(w2dist_self)

    w2_dist_data = {
        "w2_dists_nuts": np.array(w2_dists_nuts),
        "w2_dists_aahmc": np.array(w2_dists_aahmc),
        "w2_dists_self": np.array(w2_dists_self),
    }

    return w2_dist_data


def calculate_equivalent_sample_size_data(w2_dist_data, indices, target_sample_size):
    """
    Function to calculate the equivalent sample-size data
    """

    # Set up interpolators
    aahmc_interpolator = interpolate.interp1d(
        indices,
        w2_dist_data["w2_dists_aahmc"],
        bounds_error=False,
        fill_value=0,
    )

    nuts_interpolator = interpolate.interp1d(
        w2_dist_data["w2_dists_nuts"],
        indices,
        bounds_error=False,
        fill_value=0,
    )

    # # set target sample size
    # target_sample_size = 1000
    # print(f"Target sample size: {target_sample_size}")

    # calculate (through interpolation) what the aahmc w2 distance at that sample size is
    w2_dist_aahmc_at_target_sample_size = aahmc_interpolator(target_sample_size)
    # print(f"W-2 distance AAHMC at target sample size: {w2_dist_aahmc_at_target_sample_size}")

    # calculate the sample size NUTS has to have for that equivalent W2 distance
    sample_size_nuts_at_equivalent_w2_dist = nuts_interpolator(
        w2_dist_aahmc_at_target_sample_size
    )
    # print(f"Sample size NUTS at equivalent W-2 distance: {sample_size_nuts_at_equivalent_w2_dist}")

    # calculate ratios
    ratio_equivalent_sample_size = (
        sample_size_nuts_at_equivalent_w2_dist / target_sample_size
    )
    # print(f"Ratio sample size NUTS/AAHMC at W-2 dist of AAHMC at sample size {target_sample_size}: {ratio_equivalent_sample_size}")

    equivalent_sample_size_data = {
        "target_sample_size": target_sample_size,
        "w2_dist_aahmc_at_target": w2_dist_aahmc_at_target_sample_size,
        "sample_size_nuts_at_equivalent_w2_dist": sample_size_nuts_at_equivalent_w2_dist,
        "ratio_equivalent_sample_size": ratio_equivalent_sample_size,
    }

    return equivalent_sample_size_data


def get_available_hexnums(dirname):
    """
    Function to get the availabe hexnums
    """

    # get available hexnums
    content = os.listdir(dirname)
    available_hexes = []

    for el in content:
        hexnum = el.split("_")[-1].split(".")[0]
        if not hexnum in available_hexes:
            available_hexes.append(hexnum)

    return available_hexes


#
# dirname = "/home/david/data_projects/AAHMC_data/stage_1_results/sampler_comparison_simple_4dMVN_full_example_0.00025_sleep"


#
dirname = "/home/david/data_projects/AAHMC_data/stage_1_results/sampler_comparison_simple_2dMVN_full_example_0.00025_sleep"
diff_type = "numdiff"


available_hexnums = get_available_hexnums(dirname)


ratios = []

for hexnum in available_hexnums:

    # readout data
    realisation_data = readout_data(dirname, diff_type, hexnum)

    #
    stepsize = 10
    indices = np.arange(1, len(realisation_data["nuts_cumulative_times"]), stepsize)[
        :200
    ]

    # calculate W2 distances
    w2_dist_data = get_w2_dists(realisation_data, indices)

    # calculate equivalent sample size info
    equivalent_sample_size_data = calculate_equivalent_sample_size_data(
        w2_dist_data, indices, target_sample_size=1000
    )
    print(equivalent_sample_size_data)

    ratios.append(equivalent_sample_size_data["ratio_equivalent_sample_size"])

#
median_percentile_dict = get_median_percentiles(ratios)
print(median_percentile_dict)

#
equivalent_line_plot_args = {"color": "black", "linestyle": "dashed", "alpha": 0.5}
plt.plot(
    [target_sample_size, target_sample_size],
    [0, w2_dist_aahmc_at_target_sample_size],
    **equivalent_line_plot_args,
)
plt.plot(
    [target_sample_size, sample_size_nuts_at_equivalent_w2_dist],
    [w2_dist_aahmc_at_target_sample_size, w2_dist_aahmc_at_target_sample_size],
    **equivalent_line_plot_args,
)
plt.plot(
    [sample_size_nuts_at_equivalent_w2_dist, sample_size_nuts_at_equivalent_w2_dist],
    [0, w2_dist_aahmc_at_target_sample_size],
    **equivalent_line_plot_args,
)

plt.plot(indices, w2_dists_aahmc, label="aahmc")
plt.plot(indices, w2_dists_nuts, label="nuts")
plt.plot(indices, w2_dists_self, label="self")
plt.legend()

plt.xscale("log")
plt.yscale("log")
plt.show()
quit()

plt.plot(indices, nuts_cumulative_times[indices], label="nuts")

plt.plot(
    indices,
    aahmc_cumulative_times_including_map_building[indices],
    label="aahmc + building",
)
plt.plot(indices, aahmc_cumulative_times[indices], label="aahmc")
plt.legend()

plt.xscale("log")
plt.yscale("log")
plt.show()
