from paper_plot_handler import plot_correlation_quality_measure_w2_distance

if __name__ == "__main__":
    max_file_i = 100

    global_plot_settings = {
        "scatter_show": True,
        "bin_contour_show": False,
        "scatter_alpha": 0.1,
        "bin_statistics_alpha": 1,
        "bin_limit_alpha": 0.9,
        "custom_xlims": [-0.05, 1.05],
        "custom_ylims": [-5, 4.5],
    }

    # # 100 samples
    # output_dir = "/home/david/data_projects/AAHMC_data/stage_2_results/map_quality_tests/server/simple_2dMVN/simple_2dMVN_100_checks/autodiff/"
    # plot_settings = {**global_plot_settings, "output_name": "simple_2dMVN_autodiff_100.pdf"}
    # plot_correlation_quality_measure_w2_distance(
    #     output_dir=output_dir, plot_settings=plot_settings, max_file_i=max_file_i
    # )

    # 100 samples original acceptance prob style
    output_dir = "/home/david/data_projects/AAHMC_data/stage_2_results/map_quality_tests/server/simple_2dMVN/simple_2dMVN_100_checks/autodiff/"
    plot_settings = {
        **global_plot_settings,
        "output_name": "simple_2dMVN_autodiff_100_acceptance_probability_style.pdf",
    }
    plot_correlation_quality_measure_w2_distance(
        output_dir=output_dir,
        plot_settings=plot_settings,
        quality_measure_method="acceptance_probability_style",
        max_file_i=max_file_i,
    )

    # 100 samples conservation fraction style style
    output_dir = "/home/david/data_projects/AAHMC_data/stage_2_results/map_quality_tests/server/simple_2dMVN/simple_2dMVN_100_checks/autodiff/"
    plot_settings = {
        **global_plot_settings,
        "output_name": "simple_2dMVN_autodiff_100_conservation_fraction_style.pdf",
    }
    plot_correlation_quality_measure_w2_distance(
        output_dir=output_dir,
        plot_settings=plot_settings,
        quality_measure_method="conservation_fraction_style",
        max_file_i=max_file_i,
    )

    # 1000 samples
    output_dir = "/home/david/data_projects/AAHMC_data/stage_2_results/map_quality_tests/server/simple_2dMVN/simple_2dMVN_1000_checks/autodiff/"
    plot_settings = {
        **global_plot_settings,
        "output_name": "simple_2dMVN_autodiff_1000.pdf",
    }
    plot_correlation_quality_measure_w2_distance(
        output_dir=output_dir, plot_settings=plot_settings, max_file_i=max_file_i
    )
