"""
Function to plot the following


there will be 2 versions of this plot.
- One that just stores things as a function of the training epoch to see if we understand things and if quality agrees in both ways of measuring
- One that stores things as a function of target map quality

the data should contain the following:
- at every snapshot (based on epoch time, map quality):
    - time spent training map
    - mean quality of map (according to our metric)
    - mean quality of sample (w2- distance compared to true)
- we do this for both an autodifferentiable model as well as a numerical differentiation model

TODO: allow a panel to write ratios to a subpanel
"""

import functools
import os
import pickle

import matplotlib.pyplot as plt
import numpy as np
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot
from hmc_project_code.functions.map_quality_heuristic.plot_map_quality_vs_w2_distance import (
    extract_data_from_quality_measures,
)
from hmc_project_code.functions.plotting.utility import align_axes

from paper_aahmc_scripts.figure_scripts.stage_2_compare_sampling_qualities.utils import (
    add_correlation_panel,
    handle_data_extraction_for_given_dataset,
    handle_plot_argument_logic,
)

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

plt.rcParams["lines.solid_capstyle"] = "round"


# General plot handlers
def plot_grand_quality_measure_correlation(
    dataset_dict,
    plot_settings,
    quality_measure_method="new_acceptance_probability_style",
    max_file_i=1e9,
    sleep_time=0,
    increasing_only=False,
    add_sample_quality_panel=True,
    add_training_time_panel=True,
    add_training_steps_panel=True,
    add_quality_change_panel=True,
    indicate_bins=True,
    return_fig=False,
):
    """
    Main figure to construct a multi-panel plot that shows the correlation of map quality with other quanties.

    NOTE: this routine cannot be used to provide a subpanel with the ratio yet
    """

    use_averaged_w2 = False
    bins = np.arange(0, 1.1, 0.1)

    #
    plot_config = handle_plot_argument_logic(plot_settings=plot_settings)

    ##################
    # Extract the data
    for key in dataset_dict.keys():
        print("extracting data for {}".format(key))
        dataset_dict[key]["extracted_data"] = handle_data_extraction_for_given_dataset(
            output_dir=dataset_dict[key]["output_dir"],
            sleep_time=sleep_time,
            max_file_i=max_file_i,
            use_averaged_w2=use_averaged_w2,
            quality_measure_method=quality_measure_method,
            increasing_only=increasing_only,
        )

    ##################
    # Set up figure logic
    rows = (
        add_sample_quality_panel
        + add_training_time_panel
        + add_training_steps_panel
        + add_quality_change_panel
    )

    fig = plt.figure(figsize=(10, 10 * rows))
    gs = fig.add_gridspec(nrows=rows, ncols=1)

    axes_dict = {}
    axes_list = []

    #
    row_i = 0
    if add_sample_quality_panel:
        axes_dict["sample_quality_panel"] = fig.add_subplot(gs[row_i, 0])
        axes_list.append(axes_dict["sample_quality_panel"])
        row_i += 1
    if add_training_time_panel:
        axes_dict["training_time_panel"] = fig.add_subplot(gs[row_i, 0])
        axes_list.append(axes_dict["training_time_panel"])
        row_i += 1
    if add_training_steps_panel:
        axes_dict["training_steps_panel"] = fig.add_subplot(gs[row_i, 0])
        axes_list.append(axes_dict["training_steps_panel"])
        row_i += 1
    if add_quality_change_panel:
        axes_dict["quality_change_panel"] = fig.add_subplot(gs[row_i, 0])
        axes_list.append(axes_dict["quality_change_panel"])
        row_i += 1

    ################################
    # Plot the panels

    ###########
    # Bind the function call (DRY)
    bound_add_correlation_panel = functools.partial(
        add_correlation_panel,
        plot_settings=plot_settings,
        fig=fig,
        bins=bins,
        indicate_bins=indicate_bins,
        **plot_config,
    )

    panel_i = 0
    if add_sample_quality_panel:
        bound_add_correlation_panel(
            panel_type="sample_quality",
            dataset_dict=dataset_dict,
            axis=axes_dict["sample_quality_panel"],
            x_arr_key="all_quality_measures",
            y_arr_key="all_log10diffs_w2_dist",
            add_labels=True if panel_i == 0 else False,
        )
        panel_i += 1

    ###########
    # Show correlation between time spent until that point and map-quality
    if add_training_time_panel:

        bound_add_correlation_panel(
            panel_type="training_time",
            dataset_dict=dataset_dict,
            axis=axes_dict["training_time_panel"],
            x_arr_key="all_quality_measures",
            y_arr_key="all_log10total_training_time",
            add_labels=True if panel_i == 0 else False,
        )
        panel_i += 1

    ###########
    # Show correlation between number of training steps until that point and map-quality
    if add_training_steps_panel:
        bound_add_correlation_panel(
            panel_type="training_steps",
            dataset_dict=dataset_dict,
            axis=axes_dict["training_steps_panel"],
            x_arr_key="all_quality_measures",
            y_arr_key="all_log10num_training_steps",
            add_labels=True if panel_i == 0 else False,
        )
        panel_i += 1

    ###########
    # Show correlation between map-quality and the change in quality that lead up to that point
    if add_quality_change_panel:

        # if show_ratio:
        #     all_change_quality_measures = all_change_quality_measure_vs_training_steps

        #####
        # Draw zero-line
        axes_dict["quality_change_panel"].axhline(0, zorder=-1000, alpha=0.25)

        #####
        bound_add_correlation_panel(
            panel_type="quality_change",
            dataset_dict=dataset_dict,
            axis=axes_dict["quality_change_panel"],
            x_arr_key="all_current_quality_measures",
            y_arr_key="all_change_quality_measures",
            add_labels=True if panel_i == 0 else False,
        )
        panel_i += 1

    # ###########
    # #
    # time_per_training_step = (all_total_training_time) / np.array(all_num_training_steps)
    # print(time_per_training_step)

    #########
    # Makeup
    # TODO: put into function
    if add_sample_quality_panel:
        axes_dict["sample_quality_panel"].set_ylabel(
            r"$\rm{log}_{10}|W_{2,\rm{sampler}}-W_{2,\rm{True}}|$"
        )
        if "custom_xlims" in plot_settings:
            axes_dict["sample_quality_panel"].set_xlim(plot_settings["custom_xlims"])
        if "custom_ylims" in plot_settings:
            axes_dict["sample_quality_panel"].set_ylim(plot_settings["custom_ylims"])

    if add_training_time_panel:
        axes_dict["training_time_panel"].set_ylabel("log10(Time-spent / s)")
        if sleep_time > 0:
            axes_dict["training_time_panel"].set_ylabel(
                r"log10(Time-spent / $\tau_{s}$)"
            )

    if add_training_steps_panel:
        axes_dict["training_steps_panel"].set_ylabel("log10(Map training-steps")

    if add_quality_change_panel:
        axes_dict["quality_change_panel"].set_ylabel(
            "Difference current and previous\nquality measure"
            + r"$Q_{\rm{cur}}$-$Q_{\rm{prev}}$"
        )

    for axis in axes_list[:-1]:
        axis.set_xticklabels([])
    axes_list[-1].set_xlabel("Quality measure Q")

    # Add legend
    axes_list[0].legend(loc="best")

    if return_fig:
        return fig, axes_dict, axes_list

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def plot_quality_measure_correlation_panel_and_ratio(
    dataset_dict,
    plot_settings,
    panel_type,
    quality_measure_method="new_acceptance_probability_style",
    increasing_only=False,
    max_file_i=1e9,
    sleep_time=0,
    indicate_bins=True,
    return_fig=False,
):
    """
    Main figure to construct a multi-panel plot that shows the correlation of map quality with other quanties.

    NOTE: this routine cannot be used to provide a subpanel with the ratio yet
    """

    use_averaged_w2 = False
    bins = np.arange(0, 1.1, 0.1)

    #
    plot_config = handle_plot_argument_logic(plot_settings=plot_settings)

    ##################
    # Extract the data
    for key in dataset_dict.keys():
        print("extracting data for {}".format(key))
        dataset_dict[key]["extracted_data"] = handle_data_extraction_for_given_dataset(
            output_dir=dataset_dict[key]["output_dir"],
            sleep_time=sleep_time,
            max_file_i=max_file_i,
            use_averaged_w2=use_averaged_w2,
            quality_measure_method=quality_measure_method,
            increasing_only=increasing_only,
        )

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(15, 15))
    gs = fig.add_gridspec(nrows=5, ncols=1)

    #
    axis_correlation_panel = fig.add_subplot(gs[:4, 0])
    axis_ratio_panel = fig.add_subplot(gs[4, 0])

    ################################
    # Plot the panels

    ###########
    # Bind the function call (DRY)
    bound_add_correlation_panel = functools.partial(
        add_correlation_panel,
        plot_settings=plot_settings,
        fig=fig,
        bins=bins,
        indicate_bins=indicate_bins,
        **plot_config,
    )

    #########
    #

    def tenpower(val_arr):
        return np.power(10, val_arr)

        pre_ratio_function = None

    if panel_type == "sample_quality":
        x_arr_key = "all_quality_measures"
        y_arr_key = "all_log10diffs_w2_dist"
        pre_ratio_function = tenpower
    elif panel_type == "training_time":
        x_arr_key = "all_quality_measures"
        y_arr_key = "all_log10total_training_time"
        pre_ratio_function = tenpower
    elif panel_type == "training_steps":
        x_arr_key = "all_quality_measures"
        y_arr_key = "all_log10num_training_steps"
        pre_ratio_function = tenpower
    elif panel_type == "quality_change":
        x_arr_key = "all_current_quality_measures"
        y_arr_key = "all_change_quality_measures"
    else:
        raise ValueError("Unsupported panel type")

    ###########
    #
    bound_add_correlation_panel(
        dataset_dict=dataset_dict,
        axis=axis_correlation_panel,
        x_arr_key=x_arr_key,
        y_arr_key=y_arr_key,
        add_labels=True,
        ratio_axis=axis_ratio_panel,
        pre_ratio_function=pre_ratio_function,
        panel_type=panel_type,
    )

    #########
    # Makeup
    if panel_type == "sample_quality":
        ylabel = r"$\rm{log}_{10}|W_{2,\rm{sampler}}-W_{2,\rm{True}}|$"
    elif panel_type == "training_time":
        ylabel = "log10(Time-spent / s)"
        if sleep_time > 0:
            ylabel = r"log10(Time-spent / $\tau_{s}$)"
    elif panel_type == "training_steps":
        ylabel = "log10(Map training-steps"
    elif panel_type == "quality_change":
        ylabel = (
            "Difference current and previous\nquality measure"
            + r"$Q_{\rm{cur}}$-$Q_{\rm{prev}}$"
        )
    #     if "custom_xlims" in plot_settings:
    #         axes_dict["sample_quality_panel"].set_xlim(plot_settings["custom_xlims"])
    #     if "custom_ylims" in plot_settings:
    #         axes_dict["sample_quality_panel"].set_ylim(plot_settings["custom_ylims"])

    axis_correlation_panel.set_ylabel(ylabel)
    axis_ratio_panel.set_xlabel("Quality measure Q")
    axis_correlation_panel.set_xticklabels([])

    # Add legend
    axis_correlation_panel.legend(loc="best")

    align_axes(
        fig, axes_list=[axis_correlation_panel, axis_ratio_panel], which_axis="x"
    )

    if return_fig:
        return fig

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


#########################
# Vanilla plot handlers


def handle_paper_plot_vanilla_general_without_change(result_root, max_file_i=1e90):
    """
    Routine that handles plotting the vanilla results
    """

    ############
    # plot dimensional scaling
    plot_grand_quality_measure_correlation(
        dataset_dict={
            "2dsimplemvn": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                # "colour": "blue",
                "label": "2-d MVN",
            },
        },
        plot_settings={
            "output_name": os.path.join(plot_output_dir, "vanilla.pdf"),
            "scatter_show": True,
            "scatter_alpha": 0.25,
            "scatter_colour": "black",
            "bin_limit_show": True,
        },
        max_file_i=max_file_i,
        sleep_time=0.0025,
        add_training_steps_panel=True,
        add_quality_change_panel=False,
    )


def handle_paper_plot_vanilla_general_change_only(result_root, max_file_i=1e90):
    """
    Routine that handles plotting the vanilla results
    """

    ############
    # plot dimensional scaling
    plot_grand_quality_measure_correlation(
        dataset_dict={
            "2dsimplemvn": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                # "colour": "blue",
                "label": "2-d MVN",
            },
        },
        plot_settings={
            "output_name": os.path.join(plot_output_dir, "vanilla_change_rate.pdf"),
            "scatter_show": True,
            "scatter_alpha": 0.25,
            "scatter_colour": "black",
            "bin_limit_show": True,
        },
        max_file_i=max_file_i,
        sleep_time=0.0025,
        add_sample_quality_panel=False,
        add_training_time_panel=False,
        add_training_steps_panel=False,
        add_quality_change_panel=True,
    )


#########################
# Variation plot handlers


# Dimension scaling
def handle_paper_plot_dimension_scaling_general(
    result_root, increasing_only, max_file_i=1e90
):
    """
    Function that handles plotting the dimension scaling results
    """

    ############
    # plot dimensional scaling
    plot_grand_quality_measure_correlation(
        dataset_dict={
            "2dsimplemvn": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "blue",
                "label": "2-d MVN",
            },
            "3dsimplemvn": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_3dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "red",
                "label": "3-d MVN",
            },
            "4dsimplemvn": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_4dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "black",
                "label": "4-d MVN",
            },
        },
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir,
                "dimension_variation_comparison_general_increasing_only_{}.pdf".format(
                    increasing_only
                ),
            ),
            "scatter_show": False,
            "bin_limit_show": False,
        },
        max_file_i=max_file_i,
        sleep_time=0.0025,
        increasing_only=increasing_only,
    )


def handle_paper_plot_dimension_scaling_training_time(
    result_root, increasing_only, max_file_i=1e90
):
    """
    Function to handle the paper plot for the dimension scaling variations training time
    """

    ############
    # plot dimensional scaling
    plot_quality_measure_correlation_panel_and_ratio(
        dataset_dict={
            "2dsimplemvn": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "blue",
                "label": "2-d MVN",
            },
            "3dsimplemvn": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_3dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "red",
                "label": "3-d MVN",
            },
            "4dsimplemvn": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_4dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "black",
                "label": "4-d MVN",
            },
        },
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir,
                "dimension_variation_comparison_training_time_increasing_only_{}.pdf".format(
                    increasing_only
                ),
            ),
            "scatter_show": False,
            "bin_limit_show": False,
            "add_expected_dimensional_scaling": True,
        },
        panel_type="training_time",
        max_file_i=max_file_i,
        sleep_time=0.0025,
        increasing_only=increasing_only,
    )


# Sample-size
def handle_paper_plot_sample_size_variation_general(
    result_root, increasing_only, max_file_i=1e90
):
    """
    Function that handles plotting the dimension scaling results
    """

    ############
    # plot dimensional scaling
    plot_grand_quality_measure_correlation(
        dataset_dict={
            "1000 size": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "blue",
                "label": "1000 size",
            },
            "500 size": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_sample_size_500_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "red",
                "label": "500 size",
            },
            "2000 size": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_sample_size_2000_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "black",
                "label": "2000 size",
            },
            "4000 size": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_sample_size_4000_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "purple",
                "label": "4000 size",
            },
        },
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir,
                "sample_size_variation_comparison_general_increasing_only_{}.pdf".format(
                    increasing_only
                ),
            ),
            "scatter_show": False,
            "bin_limit_show": False,
        },
        max_file_i=max_file_i,
        sleep_time=0.0025,
        increasing_only=increasing_only,
    )


def handle_paper_plot_sample_size_variation_sample_quality(
    result_root, increasing_only, max_file_i=1e90
):
    """
    Function to handle the paper plot for the dimension scaling variations training time
    """

    ############
    # plot dimensional scaling
    plot_quality_measure_correlation_panel_and_ratio(
        dataset_dict={
            "1000 size": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "blue",
                "label": "1000 size",
            },
            "500 size": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_sample_size_500_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "red",
                "label": "500 size",
            },
            "2000 size": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_sample_size_2000_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "black",
                "label": "2000 size",
            },
            "4000 size": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_sample_size_4000_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "purple",
                "label": "4000 size",
            },
        },
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir,
                "sample_size_variation_sample_quality_increasing_only_{}.pdf".format(
                    increasing_only
                ),
            ),
            "scatter_show": False,
            "bin_limit_show": False,
        },
        panel_type="sample_quality",
        max_file_i=max_file_i,
        sleep_time=0.0025,
        increasing_only=increasing_only,
    )


# num quality checks
def handle_paper_plot_num_checks_variation_general(
    result_root, increasing_only, max_file_i=1e90
):
    """
    Function that handles plotting the dimension scaling results
    """

    ############
    # plot dimensional scaling
    plot_grand_quality_measure_correlation(
        dataset_dict={
            "100 checks": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "blue",
                "label": "100 checks",
            },
            "20 checks": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_20_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "red",
                "label": "20 checks",
            },
            "500 checks": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_500_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "black",
                "label": "500 checks",
            },
        },
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir,
                "num_checks_variation_comparison_general_increasing_only_{}.pdf".format(
                    increasing_only
                ),
            ),
            "scatter_show": False,
            "bin_limit_show": False,
        },
        max_file_i=max_file_i,
        sleep_time=0.0025,
        increasing_only=increasing_only,
    )


def handle_paper_plot_num_checks_variation_sample_quality(
    result_root, increasing_only, max_file_i=1e90
):
    """ """

    ############
    # plot dimensional scaling
    plot_quality_measure_correlation_panel_and_ratio(
        dataset_dict={
            "100 checks": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "blue",
                "label": "100 checks",
            },
            "20 checks": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_20_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "red",
                "label": "20 checks",
            },
            "500 checks": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_500_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "black",
                "label": "500 checks",
            },
        },
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir,
                "num_checks_variation_sample_quality_increasing_only_{}.pdf".format(
                    increasing_only
                ),
            ),
            "scatter_show": False,
            "bin_limit_show": False,
        },
        panel_type="sample_quality",
        max_file_i=max_file_i,
        sleep_time=0.0025,
        increasing_only=increasing_only,
    )


# num flows
def handle_paper_plot_num_flows_variation_general(
    result_root, increasing_only, max_file_i=1e90
):
    """
    Function that handles plotting the dimension scaling results
    """

    ############
    # plot dimensional scaling
    plot_grand_quality_measure_correlation(
        dataset_dict={
            "1 flow": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "blue",
                "label": "100 checks",
            },
            "2 flows": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_2_num_flows_0.0025sleep/numdiff",
                ),
                "colour": "red",
                "label": "2 flows",
            },
            "3 flows": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_3_num_flows_0.0025sleep/numdiff",
                ),
                "colour": "black",
                "label": "3 flows",
            },
        },
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir,
                "num_flows_variation_comparison_general_increasing_only_{}.pdf".format(
                    increasing_only
                ),
            ),
            "scatter_show": False,
            "bin_limit_show": False,
        },
        max_file_i=max_file_i,
        sleep_time=0.0025,
        increasing_only=increasing_only,
    )


def handle_paper_plot_num_flows_variation_quality_and_training_time(
    result_root, increasing_only, max_file_i=1e90
):
    """
    Function that handles plotting the dimension scaling results
    """

    ############
    # plot dimensional scaling
    plot_grand_quality_measure_correlation(
        dataset_dict={
            "1 flow": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_0.0025sleep/numdiff",
                ),
                "colour": "blue",
                "label": "100 checks",
            },
            "2 flows": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_2_num_flows_0.0025sleep/numdiff",
                ),
                "colour": "red",
                "label": "2 flows",
            },
            "3 flows": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_3_num_flows_0.0025sleep/numdiff",
                ),
                "colour": "black",
                "label": "3 flows",
            },
        },
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir,
                "num_flows_variation_comparison_quality_and_training_time_increasing_only_{}.pdf".format(
                    increasing_only
                ),
            ),
            "scatter_show": False,
            "bin_limit_show": False,
        },
        max_file_i=max_file_i,
        sleep_time=0.0025,
        increasing_only=increasing_only,
        add_sample_quality_panel=True,
        add_training_time_panel=True,
        add_training_steps_panel=False,
        add_quality_change_panel=False,
    )


# learning rate steps (default flows =3)
def handle_paper_plot_learning_rate_variation_general(
    result_root, increasing_only, max_file_i=1e90
):
    """
    Function that handles plotting the learning rate variation plots. skips the number of steps for now because it didnt steore that properly
    """

    ############
    # plot dimensional scaling
    plot_grand_quality_measure_correlation(
        dataset_dict={
            "lr=5e-3": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_3_num_flows_0.0025sleep/numdiff",
                ),
                "colour": "blue",
                "label": "lr=5e-3 (3 flows)",
            },
            "lr=5e-2": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_learning_rate_5e-2_three_flows_0.0025sleep/numdiff",
                ),
                "colour": "red",
                "label": "lr=5e-2 (3 flows)",
            },
            "lr=5e-4": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_learning_rate_5e-4_three_flows_0.0025sleep/numdiff",
                ),
                "colour": "black",
                "label": "lr=5e-4 (3 flows)",
            },
        },
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir,
                "learning_rate_variation_comparison_general_increasing_only_{}.pdf".format(
                    increasing_only
                ),
            ),
            "scatter_show": False,
            "bin_limit_show": False,
        },
        max_file_i=max_file_i,
        sleep_time=0.0025,
        add_training_steps_panel=False,
        increasing_only=increasing_only,
    )


# learning rate schedulers  (default flows =3)
def handle_paper_plot_learning_rate_scheduler_variation_general(
    result_root, increasing_only=False, max_file_i=1e90
):
    """
    Function that handles plotting the learning rate variation plots. skips the number of steps for now because it didnt steore that properly
    """

    ############
    # plot dimensional scaling
    plot_grand_quality_measure_correlation(
        dataset_dict={
            "fixed": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_3_num_flows_0.0025sleep/numdiff",
                ),
                "colour": "blue",
                "label": "fixed (3 flows)",
            },
            "plateau": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_plateau_learning_rate_three_flows_0.0025sleep/numdiff",
                ),
                "colour": "red",
                "label": "plateau (3 flows)",
            },
            "linear": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_linear_learning_rate_three_flows_0.0025sleep/numdiff",
                ),
                "colour": "black",
                "label": "linear (3 flows)",
            },
            "exponential": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_exponential_learning_rate_three_flows_0.0025sleep/numdiff",
                ),
                "colour": "purple",
                "label": "exponential (3 flows)",
            },
        },
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir,
                "learning_rate_scheduler_variation_comparison_general_increasing_only_{}.pdf".format(
                    increasing_only
                ),
            ),
            "scatter_show": False,
            "bin_limit_show": False,
        },
        max_file_i=max_file_i,
        sleep_time=0.0025,
        add_training_steps_panel=False,
        increasing_only=increasing_only,
    )


# learning rate schedulers  (default flows =3 lr=5e-3)
def handle_paper_plot_learning_rate_scheduler_variation_lower_initial_rate_general(
    result_root, max_file_i=1e90
):
    """
    Function that handles plotting the learning rate variation plots. skips the number of steps for now because it didnt steore that properly
    """

    ############
    # plot dimensional scaling
    plot_grand_quality_measure_correlation(
        dataset_dict={
            "fixed": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_3_num_flows_0.0025sleep/numdiff",
                ),
                "colour": "blue",
                "label": "fixed (3 flows, init=5e-3)",
            },
            "plateau": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_plateau_learning_rate_three_flows_learning_rate_5e-3_0.0025sleep/numdiff",
                ),
                "colour": "red",
                "label": "plateau (3 flows, init=5e-3)",
            },
            "linear": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_linear_learning_rate_three_flows_learning_rate_5e-3_0.0025sleep/numdiff",
                ),
                "colour": "black",
                "label": "linear (3 flows, init=5e-3)",
            },
            "exponential": {
                "output_dir": os.path.join(
                    result_root,
                    "stage_2_results/map_quality_tests/paper/simple_2dMVN_100_checks_randomized_test_exponential_learning_rate_three_flows_learning_rate_5e-3_0.0025sleep/numdiff",
                ),
                "colour": "purple",
                "label": "exponential (3 flows, init=5e-3)",
            },
        },
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir,
                "learning_rate_scheduler_learning_rate_5e-3_variation_comparison_general.pdf",
            ),
            "scatter_show": False,
            "bin_limit_show": False,
        },
        max_file_i=max_file_i,
        sleep_time=0.0025,
        add_training_steps_panel=False,
    )


if __name__ == "__main__":
    result_root = "/home/david/data_projects/AAHMC_data/"
    plot_output_dir = os.path.join(this_file_dir, "plots")

    ############################
    # 2-d only

    # # All panels but change rate
    # handle_paper_plot_vanilla_general_without_change(
    #     result_root=result_root,
    #     # max_file_i=2
    # )

    # # change rate only
    # handle_paper_plot_vanilla_general_change_only(
    #     result_root=result_root,
    #     # max_file_i=2
    # )

    ############################
    # Variations

    ###################
    # Dimension scaling

    # # General plot
    # handle_paper_plot_dimension_scaling_general(
    #     result_root=result_root,
    #     increasing_only=False,
    #     # max_file_i=2
    # )
    # handle_paper_plot_dimension_scaling_general(
    #     result_root=result_root,
    #     increasing_only=True,
    #     # max_file_i=2
    # )

    # training time panel
    handle_paper_plot_dimension_scaling_training_time(
        result_root=result_root,
        increasing_only=False,
        # max_file_i=2
    )
    handle_paper_plot_dimension_scaling_training_time(
        result_root=result_root,
        increasing_only=True,
        # max_file_i=2
    )

    ###################
    # sample size

    # General plot
    handle_paper_plot_sample_size_variation_general(
        result_root=result_root,
        increasing_only=False,
        # max_file_i=2
    )
    handle_paper_plot_sample_size_variation_general(
        result_root=result_root,
        increasing_only=True,
        # max_file_i=2
    )

    # sample-quality
    handle_paper_plot_sample_size_variation_sample_quality(
        result_root=result_root,
        increasing_only=False,
        # max_file_i=2
    )
    handle_paper_plot_sample_size_variation_sample_quality(
        result_root=result_root,
        increasing_only=True,
        # max_file_i=2
    )

    ###################
    # Num quality checks

    # General plot
    handle_paper_plot_num_checks_variation_general(
        result_root=result_root,
        increasing_only=False,
        # max_file_i=2
    )
    handle_paper_plot_num_checks_variation_general(
        result_root=result_root,
        increasing_only=True,
        # max_file_i=2
    )

    # sample-quality
    handle_paper_plot_num_checks_variation_sample_quality(
        result_root=result_root,
        increasing_only=False,
        # max_file_i=2
    )
    handle_paper_plot_num_checks_variation_sample_quality(
        result_root=result_root,
        increasing_only=True,
        # max_file_i=2
    )

    ###################
    # num flows

    # general
    handle_paper_plot_num_flows_variation_general(
        result_root=result_root,
        increasing_only=False,
        # max_file_i=2
    )
    handle_paper_plot_num_flows_variation_general(
        result_root=result_root,
        increasing_only=True,
        # max_file_i=2
    )

    # quality and training time
    handle_paper_plot_num_flows_variation_quality_and_training_time(
        result_root=result_root,
        increasing_only=False,
        # max_file_i=2
    )
    handle_paper_plot_num_flows_variation_quality_and_training_time(
        result_root=result_root,
        increasing_only=True,
        # max_file_i=2
    )

    ###################
    # evaluation time

    # ##########
    # # TODO: this needs to be fixed
    # handle_paper_plot_num_quality_checks_general(result_root=result_root)

    ##################
    # learning rate

    # general
    handle_paper_plot_learning_rate_variation_general(
        result_root=result_root,
        increasing_only=False,
        # max_file_i=9
    )
    handle_paper_plot_learning_rate_variation_general(
        result_root=result_root,
        increasing_only=True,
        # max_file_i=9
    )

    ##################
    # learning rate schedulers

    # general
    handle_paper_plot_learning_rate_scheduler_variation_general(
        result_root=result_root,
        increasing_only=False,
        # max_file_i=3
    )
    handle_paper_plot_learning_rate_scheduler_variation_general(
        result_root=result_root,
        increasing_only=True,
        # max_file_i=3
    )

    # # ###################
    # # # learning rate schedulers lr=5e-3 initial rate

    # # general
    # handle_paper_plot_learning_rate_scheduler_variation_lower_initial_rate_general(
    #     result_root=result_root,
    #     # max_file_i=9
    # )
