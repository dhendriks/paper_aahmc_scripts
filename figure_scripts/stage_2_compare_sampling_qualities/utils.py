"""
Utils for the correlation with map quality plots
"""

import os
import pickle

import matplotlib.pyplot as plt
import matplotlib.transforms as transforms
import numpy as np
from hmc_project_code.functions.map_quality_heuristic.plot_map_quality_vs_w2_distance import (
    extract_data_from_quality_measures,
)
from hmc_project_code.functions.plotting.utility import align_axes


def expand(x, y, gap=1e-4):
    add = np.tile([0, gap, np.nan], len(x))
    x1 = np.repeat(x, 3) + add
    y1 = np.repeat(y, 3) + add
    return x1, y1


def add_correlation_panel(
    dataset_dict,
    fig,
    axis,
    x_arr_key,
    y_arr_key,
    bins,
    indicate_bins,
    add_labels,
    panel_type,
    plot_settings,
    #
    scatter_show,
    scatter_plot_properties,
    #
    bin_limit_show,
    bin_limit_plot_properties,
    #
    bin_statistics_show,
    bin_statistics_plot_properties,
    ratio_axis=None,
    pre_ratio_function=None,
):
    """
    Function to add data to the correlation panel.

    Allows multiple datasets
    """

    #
    num_datasets = len(dataset_dict.keys())

    #########
    # Handle plotting for datasets
    for dataset_i, dataset_key in enumerate(dataset_dict.keys()):
        print("Plotting data for {}".format(dataset_key))

        mean_values = np.zeros(len(bins) - 1)
        x_values = np.zeros(len(bins) - 1)
        std_values = np.zeros(len(bins) - 1)

        mean_values_to_connect = []
        x_values_to_connect = []

        #
        quality_measure_array = np.array(
            dataset_dict[dataset_key]["extracted_data"][x_arr_key]
        )
        value_array = np.array(dataset_dict[dataset_key]["extracted_data"][y_arr_key])

        ########
        # override colour
        if "colour" in dataset_dict[dataset_key].keys():
            scatter_plot_properties["colour"] = dataset_dict[dataset_key]["colour"]
            bin_limit_plot_properties["colour"] = dataset_dict[dataset_key]["colour"]
            bin_statistics_plot_properties["colour"] = dataset_dict[dataset_key][
                "colour"
            ]

        ########
        # Add scatter points
        if scatter_show:
            # #
            # ax_sample_quality.scatter(
            #     quality_measure_array,
            #     value_array,
            #     alpha=scatter_alpha,
            #     color=scatter_colour,
            #     edgecolors="none",
            # )

            axis.plot(
                *expand(quality_measure_array, value_array),
                lw=scatter_plot_properties["size"],
                color=scatter_plot_properties["colour"],
                alpha=scatter_plot_properties["alpha"],
            )

        ###########
        # Indicate the bins
        if indicate_bins:
            # Make transformation for only the y axis
            trans = transforms.blended_transform_factory(axis.transData, axis.transAxes)

            height = 0.05

            # # Draw lines to indicate
            # axis.vlines(
            #     bins,
            #     color="orange",
            #     lw=2,
            #     linestyle="--",
            #     alpha=1,
            #     ymax=1,  # TODO: perhaps we should throw in an ax transform and set this to a value
            #     ymin=0,
            #     transform=trans,
            # )

            # # Draw lines to indicate
            # axis.vlines(
            #     bins,
            #     color="orange",
            #     lw=2,
            #     linestyle="--",
            #     alpha=1,
            #     ymax=height,  # TODO: perhaps we should throw in an ax transform and set this to a value
            #     ymin=0,
            #     transform=trans,
            # )

            # Draw lines to indicate
            axis.vlines(
                bins,
                color="orange",
                lw=2,
                linestyle="--",
                alpha=1,
                ymax=1,  # TODO: perhaps we should throw in an ax transform and set this to a value
                ymin=1 - height,
                transform=trans,
            )

        ###########
        # handle bin statistics
        # TODO: consider cleaning this up

        #
        for bin_i in range(len(bins) - 1):

            #######
            # Get bin properties
            left_edge = bins[bin_i]
            right_edge = bins[bin_i + 1]
            center = (right_edge + left_edge) / 2

            # extra things to handle multiple datasets
            binwidth = np.abs(right_edge - left_edge)
            width_per_dataset = binwidth / num_datasets
            center_current_dataset = left_edge + (dataset_i + 0.5) * width_per_dataset
            left_edge_current_dataset = left_edge + dataset_i * width_per_dataset

            if not bin_statistics_plot_properties["shift_means"]:
                center_current_dataset = center

            #
            indices = np.where(
                (quality_measure_array >= left_edge)
                & (quality_measure_array < right_edge)
            )

            # Select items in bin
            quality_measures_in_bin = quality_measure_array[indices]
            values_in_bin = value_array[indices]

            if len(quality_measures_in_bin) == 0:
                continue

            ###############
            # draw bounds
            upper_values_in_bin = np.max(values_in_bin)
            lower_values_in_bin = np.min(values_in_bin)

            #
            if bin_limit_show:
                upper_line = (
                    upper_values_in_bin + bin_limit_plot_properties["offset_value"]
                )
                axis.plot(
                    (
                        left_edge_current_dataset,
                        left_edge_current_dataset + width_per_dataset,
                    ),
                    (upper_line, upper_line),
                    alpha=bin_limit_plot_properties["alpha"],
                    color=bin_limit_plot_properties["colour"],
                )

                lower_line = (
                    lower_values_in_bin - bin_limit_plot_properties["offset_value"]
                )
                axis.plot(
                    (
                        left_edge_current_dataset,
                        left_edge_current_dataset + width_per_dataset,
                    ),
                    (lower_line, lower_line),
                    alpha=bin_limit_plot_properties["alpha"],
                    color=bin_limit_plot_properties["colour"],
                )

            ##################
            # Draw statistics (mean, error)
            # TODO: this may need to change to median
            mean_values_in_bin = np.mean(values_in_bin)
            std_values_in_bin = np.std(values_in_bin)

            if bin_statistics_show:
                axis.errorbar(
                    center_current_dataset,
                    mean_values_in_bin,
                    yerr=std_values_in_bin,
                    alpha=bin_statistics_plot_properties["alpha"],
                    color=bin_statistics_plot_properties["colour"],
                    label=(
                        dataset_dict[dataset_key]["label"]
                        if add_labels and bin_i == 0
                        else None
                    ),
                )
                axis.scatter(
                    center_current_dataset,
                    mean_values_in_bin,
                    alpha=bin_statistics_plot_properties["alpha"],
                    color=bin_statistics_plot_properties["colour"],
                    s=200,
                )

                #
                x_values[bin_i] = center_current_dataset
                mean_values[bin_i] = mean_values_in_bin
                std_values[bin_i] = std_values_in_bin

                #
                mean_values_to_connect.append(mean_values_in_bin)
                x_values_to_connect.append(center_current_dataset)

        ########
        # plot lines between the means
        if bin_statistics_plot_properties["connect_means"]:
            axis.plot(
                x_values_to_connect,
                mean_values_to_connect,
                alpha=bin_statistics_plot_properties["alpha"],
                color=bin_statistics_plot_properties["colour"],
            )

        ########
        # Store values
        dataset_dict[dataset_key]["x_values"] = np.array(x_values)
        dataset_dict[dataset_key]["mean_values"] = np.array(mean_values)
        dataset_dict[dataset_key]["std_values"] = np.array(std_values)

    #########
    # Handle ratio to first
    if ratio_axis is not None:
        for dataset_key in list(dataset_dict.keys())[1:]:
            first_dataset = dataset_dict[list(dataset_dict.keys())[0]]
            current_dataset = dataset_dict[dataset_key]

            ########
            # override colour
            if "colour" in dataset_dict[dataset_key].keys():
                bin_statistics_plot_properties["colour"] = current_dataset["colour"]

            #
            centers = (bins[1:] + bins[:-1]) / 2
            first = first_dataset["mean_values"]
            current = current_dataset["mean_values"]

            #
            indices = np.where((first != 0) & (current != 0))

            #
            nonzero_first = first[indices]
            nonzero_current = current[indices]
            nonzero_centers = centers[indices]

            if pre_ratio_function is not None:
                nonzero_first = pre_ratio_function(nonzero_first)
                nonzero_current = pre_ratio_function(nonzero_current)

            nonzero_ratios = nonzero_current / nonzero_first

            # #
            # ratio_to_first = np.divide(
            #     current_dataset["mean_values"],
            #     first_dataset["mean_values"],
            #     out=np.zeros_like(first_dataset["mean_values"]),
            #     where=first_dataset["mean_values"] != 0,
            # )  # only divide nonzeros else 1

            # print("label:", current_dataset['label'])
            # print("Centers:", centers)
            # print("Mean values first:", first_dataset["mean_values"])
            # print("Mean values current:", current_dataset["mean_values"])
            # print("Ratio to first:", ratio_to_first)

            # #
            # nonzero_centers = centers[ratio_to_first!=0]
            # nonzero_ratios = ratio_to_first[ratio_to_first!=0]

            ###############
            #
            ratio_axis.scatter(
                nonzero_centers,  # TODO: we can also just do this in the center center
                nonzero_ratios,
                alpha=bin_statistics_plot_properties["alpha"],
                color=bin_statistics_plot_properties["colour"],
                s=200,
            )

            # #
            # axis.plot(
            #     x_values,
            #     mean_values,
            #     alpha=bin_statistics_plot_properties["alpha"],
            #     color=bin_statistics_plot_properties["colour"],
            # )

            # axis.errorbar(
            #     center_current_dataset,
            #     mean_values_in_bin,
            #     yerr=std_values_in_bin,
            #     alpha=bin_statistics_plot_properties["alpha"],
            #     color=bin_statistics_plot_properties["colour"],
            #     label=(
            #         dataset_dict[dataset_key]["label"]
            #         if add_labels and bin_i == 0
            #         else None
            #     ),
            # )

        ###########
        #
        if panel_type == "training_time":
            ylims = axis.get_ylim()
            axis.set_ylim((0, ylims[-1]))

        ###########
        # add expected scaling lines
        if plot_settings.get("add_expected_dimensional_scaling", False):

            expected_scaling_list = []
            for i in range(3, 6):
                expected_scaling = (2 * i + 1) / (2 * 2 + 1)
                expected_scaling_list.append(expected_scaling)
                ratio_axis.axhline(expected_scaling)

        # #
        # ratio_axis_twin_y = ratio_axis.twinx()
        # ratio_axis_twin_y.set_yticks(expected_scaling_list)
        # ratio_axis_twin_y.set_yticklabels(range(3, 6))

        # #
        # align_axes(fig, [ratio_axis, ratio_axis_twin_y], which_axis="y")
        ratio_axis.set_yscale("log")
        # ratio_axis_twin_y.set_yscale('log')


def handle_data_extraction_for_given_dataset(
    output_dir,
    sleep_time,
    max_file_i,
    use_averaged_w2,
    quality_measure_method,
    increasing_only,
):
    """
    Function to handle the extraction and preparation of the data for a particular dataset (defined by an output dir)

    returns a dictionary with the arrays in it

    TODO: check for double calculations
    """

    #
    all_diffs_w2_dist = []
    all_quality_measures = []

    #
    all_num_training_steps = []
    all_total_training_time = []
    all_total_testing_time = []
    all_total_combined_time = []

    #
    all_current_quality_measures = []
    all_previous_quality_measures = []
    all_change_quality_measures = []

    #
    all_current_training_steps = []
    all_previous_training_steps = []
    all_change_training_steps = []

    #
    all_change_quality_measure_vs_training_steps = []

    ##########
    # extract the data
    file_i = 0
    for _, file in enumerate(os.listdir(output_dir)):
        if file.endswith("pickle"):

            #########
            # Load data from file
            filename = os.path.join(output_dir, file)
            print("reading out filename: {}".format(filename))
            result_list = pickle.load(open(filename, "rb"))

            #########
            # Extract summarized data
            result_dict = extract_data_from_quality_measures(
                grand_result_list=result_list
            )

            ########
            # New approach
            for (
                result_at_given_training_step_i,
                result_at_given_training_step,
            ) in enumerate(result_dict["new_structure"]):
                ###################
                #
                quality_measure = handle_quality_measure_extraction(
                    quality_measure_method=quality_measure_method,
                    result_dict=result_at_given_training_step,
                )

                if increasing_only:
                    if result_at_given_training_step_i > 0:

                        # check if we are at higher quality than before. if so, store it and follow the rest of this function. Otherwise, go to the next iteration
                        if quality_measure > best_quality_measure:
                            best_quality_measure = quality_measure
                        else:
                            continue

                        # ##################
                        # # Determine how to handle the quality measure
                        # result_at_previous_training_step = result_dict["new_structure"][
                        #     result_at_given_training_step_i - 1
                        # ]

                        # #
                        # previous_quality_measure = handle_quality_measure_extraction(
                        #     quality_measure_method=quality_measure_method,
                        #     result_dict=result_at_previous_training_step,
                        # )

                        # if not previous_quality_measure > quality_measure:
                        #     continue
                    else:
                        best = result_at_given_training_step
                        best_quality_measure = quality_measure

                ###################
                #

                #
                num_training_steps = result_at_given_training_step["num_training_steps"]
                total_training_time = result_at_given_training_step[
                    "total_training_time"
                ]
                total_testing_time = result_at_given_training_step["total_testing_time"]

                #
                if use_averaged_w2:
                    diff_w2_dist = np.abs(
                        np.mean(
                            np.array(
                                result_at_given_training_step["w2dist_true_v_true_arr"]
                            )
                        )
                        - np.mean(
                            np.array(
                                result_at_given_training_step["w2dist_true_v_aahmc_arr"]
                            )
                        )
                    )
                else:
                    diff_w2_dist = np.abs(
                        np.array(
                            result_at_given_training_step["w2dist_true_v_true_arr"]
                        )
                        - np.array(
                            result_at_given_training_step["w2dist_true_v_aahmc_arr"]
                        )
                    )
                    quality_measure = np.ones(len(diff_w2_dist)) * quality_measure
                    num_training_steps = np.ones(len(diff_w2_dist)) * num_training_steps
                    total_training_time = (
                        np.ones(len(diff_w2_dist)) * total_training_time
                    )
                    total_testing_time = np.ones(len(diff_w2_dist)) * total_testing_time

                # Append everything
                all_diffs_w2_dist += list(diff_w2_dist)
                all_quality_measures += list(quality_measure)
                all_num_training_steps += list(num_training_steps)
                all_total_training_time += list(total_training_time)
                all_total_testing_time += list(total_testing_time)
                all_total_combined_time += list(
                    total_testing_time + total_training_time
                )

                ############
                # To track the changes
                if result_at_given_training_step_i > 0:

                    ##################
                    # Determine how to handle the quality measure
                    result_at_previous_training_step = result_dict["new_structure"][
                        result_at_given_training_step_i - 1
                    ]

                    #
                    previous_quality_measure = handle_quality_measure_extraction(
                        quality_measure_method=quality_measure_method,
                        result_dict=result_at_previous_training_step,
                    )

                    ###############
                    #
                    num_training_steps = result_at_given_training_step[
                        "num_training_steps"
                    ]
                    previous_num_training_steps = result_at_previous_training_step[
                        "num_training_steps"
                    ]

                    #
                    all_current_quality_measures.append(quality_measure)
                    all_previous_quality_measures.append(previous_quality_measure)
                    all_change_quality_measures.append(
                        quality_measure - previous_quality_measure
                    )

                    #
                    all_current_training_steps.append(num_training_steps)
                    all_previous_training_steps.append(previous_num_training_steps)
                    all_change_training_steps.append(
                        num_training_steps - previous_num_training_steps
                    )

                    #
                    all_change_quality_measure_vs_training_steps.append(
                        all_change_quality_measures[-1] / all_change_training_steps[-1]
                    )

            ######
            # cap max file number
            file_i += 1
            if file_i - 1 >= max_file_i:
                break

    #############################
    # Cast in arrays
    all_log10diffs_w2_dist = np.log10(np.array(all_diffs_w2_dist))
    all_log10num_training_steps = np.log10(np.array(all_num_training_steps))

    #
    all_total_training_time = np.array(all_total_training_time)
    all_total_testing_time = np.array(all_total_testing_time)
    all_total_combined_time = np.array(all_total_combined_time)

    # divide by sleep time if necessary
    if sleep_time > 0:
        print("dividing by sleep time")
        all_total_training_time = all_total_training_time / sleep_time
        all_total_testing_time = all_total_testing_time / sleep_time
        all_total_combined_time = all_total_combined_time / sleep_time

    all_log10total_training_time = np.log10(all_total_training_time)
    all_log10total_testing_time = np.log10(all_total_testing_time)
    all_log10total_combined_time = np.log10(all_total_combined_time)

    # Put everything into a dictionary and return the data

    return {
        "all_diffs_w2_dist": all_diffs_w2_dist,
        "all_quality_measures": all_quality_measures,
        "all_num_training_steps": all_num_training_steps,
        "all_total_training_time": all_total_training_time,
        "all_total_testing_time": all_total_testing_time,
        "all_total_combined_time": all_total_combined_time,
        "all_current_quality_measures": all_current_quality_measures,
        "all_previous_quality_measures": all_previous_quality_measures,
        "all_change_quality_measures": all_change_quality_measures,
        "all_current_training_steps": all_current_training_steps,
        "all_previous_training_steps": all_previous_training_steps,
        "all_change_training_steps": all_change_training_steps,
        "all_change_quality_measure_vs_training_steps": all_change_quality_measure_vs_training_steps,
        "all_log10diffs_w2_dist": all_log10diffs_w2_dist,
        "all_log10num_training_steps": all_log10num_training_steps,
        "all_log10total_training_time": all_log10total_training_time,
        "all_log10total_testing_time": all_log10total_testing_time,
        "all_log10total_combined_time": all_log10total_combined_time,
    }


def handle_quality_measure_extraction(quality_measure_method, result_dict):
    """
    Function to handle the extraction of the quality measure
    """

    if quality_measure_method == "acceptance_probability_style":
        quality_measure = 10 ** np.array(
            result_dict["avg_log10_original_acceptance_prob_style"]
        )
    elif quality_measure_method == "new_acceptance_probability_style":
        quality_measure = 10 ** np.array(result_dict["avg_log10_acceptance_prob_style"])
    elif quality_measure_method == "conservation_fraction_style":
        quality_measure = np.exp(
            np.array(result_dict["avg_conservation_fraction_style"]) - 1
        )

    return quality_measure


def handle_plot_argument_logic(plot_settings):
    """
    Function to handle the plot argument logic and return a dictionary that contains the correct settings
    """

    plot_config = {}

    ######
    # Plot argument processing
    plot_config["scatter_show"] = plot_settings.get("scatter_show", True)
    plot_config["scatter_plot_properties"] = {
        "alpha": plot_settings.get("scatter_alpha", 0.5),
        "colour": plot_settings.get("scatter_colour", "black"),
        "size": plot_settings.get("scatter_size", 10),
    }
    plot_config["bin_limit_show"] = plot_settings.get("bin_limit_show", True)
    plot_config["bin_limit_plot_properties"] = {
        "colour": plot_settings.get("bin_limit_colour", "red"),
        "alpha": plot_settings.get("bin_limit_alpha", 1),
        "offset_value": plot_settings.get("bin_limit_offset_value", 0.05),
        "offset_scale": plot_settings.get("bin_limit_offset_scale", 1.1),
    }
    plot_config["bin_statistics_show"] = plot_settings.get("bin_statistics_show", True)
    plot_config["bin_statistics_plot_properties"] = {
        "colour": plot_settings.get("bin_statistics_colour", "blue"),
        "alpha": plot_settings.get("bin_statistics_alpha", 1),
        "connect_means": plot_settings.get("bin_statistics_connect_means", True),
        "shift_means": plot_settings.get("bin_statistics_shift_means", True),
    }

    return plot_config
