import numpy as np

#
first = np.array(
    [
        2.36347909,
        3.20671898,
        2.98840926,
        3.84604484,
        4.05535175,
        4.29376347,
        4.56649633,
        4.68883962,
        4.75957548,
        0.0,
    ]
)
current = np.array(
    [
        2.65492379,
        3.70821006,
        3.88865008,
        4.3899957,
        4.67833503,
        4.80320697,
        5.20452095,
        5.20285017,
        0.0,
        0.0,
    ]
)
# Ratio to first: [1.12331174 1.1563876  1.30124415 1.14143123 1.15362003 1.11864731
#  1.13971863 1.10962425 0.         0.        ]

#
indices = np.where((first != 0) & (current != 0))

nonzero_first = first[indices]
nonzero_current = current[indices]


print(nonzero_first)
print(nonzero_current)
print(nonzero_current / nonzero_first)


def tenpower(val_arr):
    return np.power(10, val_arr)


# exp_nonzero_first = 10**(nonzero_first)
# exp_nonzero_current = 10**(nonzero_current)

exp_nonzero_first = tenpower(nonzero_first)
exp_nonzero_current = tenpower(nonzero_current)

print(exp_nonzero_first)
print(exp_nonzero_current)

print(exp_nonzero_current / exp_nonzero_first)
