"""
Schematic plot to show, for a given speed-up between samplers and some standard number of map training times,
- continue building this until its a fully generalised in terms of evaluation times.

TODO: add rho estimate as to when thingy is rho times faster than normal sampling
TODO: fix the formula. needs something to store the part where Ns > Nt for NUTS
"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot
from tabulate import tabulate

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def heavyside_func(x, val, incl=False):
    """
    F
    """

    if incl:
        if x >= val:
            return 1
        else:
            return 0
    else:
        if x > val:
            return 1
        else:
            return 0


def inverse_heavyside_func(x, val, incl=False):

    return np.abs(-(heavyside_func(x=x, val=val, incl=incl) - 1))


def cumulative_time_prediction_total(
    sample_size, speed_up, average_time_per_sample, sample_size_map_training
):
    """ """

    NUTS_part_one = (
        sample_size
        * speed_up
        * average_time_per_sample
        * inverse_heavyside_func(x=sample_size, val=sample_size_map_training)
    )
    NUTS_part_two = (
        sample_size_map_training
        * speed_up
        * average_time_per_sample
        * heavyside_func(x=sample_size, val=sample_size_map_training)
    )
    AAHMC_part = (
        average_time_per_sample
        * (sample_size - sample_size_map_training)
        * heavyside_func(x=sample_size, val=sample_size_map_training, incl=True)
    )

    print("sample-size, ", sample_size)
    print("NUTS_part_one-size, ", NUTS_part_one)
    print("NUTS_part_two-size, ", NUTS_part_two)
    print("AAHMC_part-size, ", AAHMC_part)

    return NUTS_part_one + NUTS_part_two + AAHMC_part


def heavyside_arr(num, arr):
    """
    Heavyside stepfunction
    """

    indices = np.indices(arr.shape)

    below = indices[indices < num]
    below_arr = np.ones(arr.shape)
    below_arr[below] = 0

    return np.abs(below_arr)


def inverse_heavyside_arr(num, arr):
    """ """

    return np.abs(-(heavyside_arr(num, arr) - 1))


def plot_cumulative_sampling_time_schematic(
    speed_up,
    sample_size,
    transition_sample_size,
    average_time_per_sample,
    plot_settings,
):
    """
    Function to plot the cumulative sampling time schematic

    TODO: all times should be expressed in eval times
    """

    ones = np.ones(sample_size)

    #
    AAHMC_times = ones * average_time_per_sample
    NUTS_times = AAHMC_times * speed_up

    arr_heavyside = heavyside_arr(transition_sample_size, ones)
    arr_inverse_heavyside = inverse_heavyside_arr(transition_sample_size, ones)

    filtered_NUTS_times = NUTS_times * arr_inverse_heavyside
    filtered_AAHMC_times = AAHMC_times * arr_heavyside

    total_times = filtered_NUTS_times + filtered_AAHMC_times

    #
    cumulative_AAHMC_times = np.cumsum(AAHMC_times)
    cumulative_NUTS_times = np.cumsum(NUTS_times)
    cumulative_hybrid_times = np.cumsum(total_times)

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(16, 16))
    gs = fig.add_gridspec(nrows=1, ncols=1)
    ax = fig.add_subplot(gs[0, :])
    scale = "log"

    number_of_samples_array = np.array(list(range(sample_size))) + 1

    cumulative_time_prediction_total_array = [
        cumulative_time_prediction_total(
            sample_size=sample_size,
            speed_up=speed_up,
            average_time_per_sample=average_time_per_sample,
            sample_size_map_training=transition_sample_size,
        )
        for sample_size in number_of_samples_array
    ]

    if scale == "log":

        ##################
        # Plot cumulative times
        ax.plot(
            number_of_samples_array,
            cumulative_NUTS_times,
            "r",
            linewidth=2,
            label="NUTS cumulative \nsampling time",
        )
        ax.plot(
            number_of_samples_array,
            cumulative_AAHMC_times,
            "b",
            linewidth=2,
            label="AAHMC cumulative \nsampling time\n(excl. map-building)",
        )

        ax.plot(
            number_of_samples_array,
            cumulative_hybrid_times,
            "green",
            # linestyle='--',
            linewidth=3,
            label="Hybrid cumulative \nsampling time",
        )

        # ax.plot(
        #     number_of_samples_array,
        #     cumulative_time_prediction_total_array,
        #     "purple",
        #     # linestyle='--',
        #     linewidth=3,
        #     label="Total cumulative \nsampling time",
        # )

        # Draw intersection point
        if sample_size > transition_sample_size:
            time_at_transition = cumulative_hybrid_times[transition_sample_size]

            ax.scatter(
                [transition_sample_size],
                [time_at_transition],
                linewidth=3,
                s=1000,
                facecolors="none",
                edgecolors="green",
                alpha=0.5,
            )

            ax.plot(
                [transition_sample_size, transition_sample_size],
                [0, time_at_transition],
                c="green",
                linestyle="--",
                alpha=1,
                linewidth=3,
            )

    #         ##############
    #         # Draw where total AAHMC sampling time (incl map) is only fraction rho of the NUTS sampling time/
    #         minimum_sample_size_for_fraction = calculate_minimum_sample_size(
    #             map_training_time=map_training_time,
    #             average_time_per_sample=average_time_per_sample,
    #             speed_up=speed_up,
    #             rho=plot_settings.get("rho", 0.25),
    #             max_sample_size=sample_size,
    #         )

    #         ax.plot(
    #             [minimum_sample_size_for_fraction, minimum_sample_size_for_fraction],
    #             [
    #                 0,
    #                 map_training_time
    #                 + minimum_sample_size_for_fraction * average_time_per_sample,
    #             ],
    #             c="blue",
    #             linestyle="-.",
    #             alpha=0.5,
    #         )

    #         # Draw intersection point
    #         ax.scatter(
    #             [minimum_sample_size_for_fraction],
    #             [
    #                 map_training_time
    #                 + minimum_sample_size_for_fraction * average_time_per_sample
    #             ],
    #             linewidth=3,
    #             s=1000,
    #             facecolors="none",
    #             edgecolors="blue",
    #             alpha=0.5,
    #         )

    #         ########
    #         #
    #         ax.annotate(
    #             "",
    #             (
    #                 minimum_sample_size_for_fraction,
    #                 cumulative_NUTS_times[int(minimum_sample_size_for_fraction) - 1]
    #                 * 0.9,
    #             ),
    #             (
    #                 minimum_sample_size_for_fraction,
    #                 (
    #                     map_training_time
    #                     + minimum_sample_size_for_fraction * average_time_per_sample
    #                 )
    #                 * 1.1,
    #             ),
    #             arrowprops={"arrowstyle": "->, head_width=0.4", "lw": 4},
    #             zorder=100,
    #         )

    #         ax.text(
    #             minimum_sample_size_for_fraction * 1.1,
    #             cumulative_NUTS_times[int(minimum_sample_size_for_fraction) - 1]
    #             * plot_settings.get("factor_y_speedup_text", 0.4),
    #             r"$\it{\rho}$",
    #             fontsize=plot_settings.get("speedup_text_fontsize", 32),
    #             zorder=100,
    #         )

    ########
    # Draw speedup
    if plot_settings.get("show_speedup_text", True):
        # speedup_sample_size = int(sample_size * plot_settings.get("speedup_sample_size_fraction", 1/3))

        # Where to draw the speedup line
        speedup_sample_size = 400
        bottom_x_value = cumulative_hybrid_times[speedup_sample_size - 1]
        bottom_x_scale = 1.1
        top_x_value = cumulative_NUTS_times[speedup_sample_size - 1]
        top_x_scale = 0.9

        ax.annotate(
            "",
            (speedup_sample_size, top_x_value * top_x_scale),
            (
                speedup_sample_size,
                bottom_x_value * bottom_x_scale,
            ),
            arrowprops={"arrowstyle": "->, head_width=0.4", "lw": 4},
            zorder=100,
        )

        ########
        #
        ax.text(
            speedup_sample_size * 1.1,
            top_x_value
            * plot_settings.get("factor_y_speedup_text", 0.4),
            r"n$_{speedup}$",
            fontsize=plot_settings.get("speedup_text_fontsize", 32),
            zorder=100,
        )

    ###########
    # Make up
    ax.set_yscale("log")
    ax.set_xscale("log")
    if plot_settings.get("show_title", True):
        ax.set_title(r"With a ${}\times$speed up".format(speed_up))

    ax.set_ylabel(
        r"Time $(\tau_{e})$", fontsize=plot_settings.get("label_fontsize", 32)
    )
    ax.set_xlabel(
        r"Sample size $n_{s}$", fontsize=plot_settings.get("label_fontsize", 32)
    )

    ax.legend(
        loc=2,
        fontsize=plot_settings.get("legend_fontsize", 32),
        framealpha=0.5,
        frameon=True,
    )

    # if not plot_settings.get("show_ticklabels", True):
    # ax.set_xticklabels([])
    # ax.set_yticklabels([])

    # ax.grid(True)
    # for tick in ax.xaxis.get_major_ticks():
    #     # tick.tick1line.set_visible(False)
    #     # tick.tick2line.set_visible(False)
    #     tick.label1.set_visible(False)
    #     tick.label2.set_visible(False)

    # for tick in ax.yaxis.get_major_ticks():
    #     # tick.tick1line.set_visible(False)
    #     # tick.tick2line.set_visible(False)
    #     tick.label1.set_visible(False)
    #     tick.label2.set_visible(False)

    if plot_settings.get("show_schematic_ticklabels", True):
        # ax.set_yticks([average_time_per_sample, map_training_times[0]])
        ax.set_yticklabels(
            [r"t$_{e}$", r"t$_{m}$"], fontsize=plot_settings.get("label_fontsize", 32)
        )

        #
        ax.set_xticks([transition_sample_size, transition_sample_size])
        ax.set_xticklabels(
            [r"n$_{\rm{min}, \rho=1}$", r"n$_{\rm{min}, \rho}$"],
            fontsize=plot_settings.get("label_fontsize", 32),
        )

    # ax.grid(True)

    ax.set_xlim(1, sample_size)

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    plot_output_dir = os.path.join(this_file_dir, "plots/")

    # For in the paper
    plot_cumulative_sampling_time_schematic(
        speed_up=15,
        sample_size=10000,
        transition_sample_size=25,
        average_time_per_sample=3,
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir, "schematic_speedup_hybrid.pdf"
            ),
            "show_ticklabels": False,
            "show_schematic_ticklabels": True,
            "add_aahmc_total_time_label": True,
            "show_title": False,
            "legend_fontsize": 32,
            "label_fontsize": 38,
            "factor_y_speedup_text": 0.5,
            "speedup_text_fontsize": 38,
            "speedup_sample_size_fraction": 1 / 3,
            "rho": 0.25,
            # "show_plot": True
        },
    )
