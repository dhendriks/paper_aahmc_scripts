"""
Schematic plot to show, for a given speed-up between samplers and some standard number of map training times,
- continue building this until its a fully generalised in terms of evaluation times.

"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot
from tabulate import tabulate

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def calculate_halfway_point(average_time_per_sample, map_training_time):
    """
    Function to calculate the number of samples where the map training constitutes half of the time of the entire training+sampling
    """

    return map_training_time / average_time_per_sample


def calculate_minimum_sample_size(
    average_time_per_sample,
    map_training_time,
    speed_up,
    max_sample_size,
    rho=1,
):
    """
    Function to calculate the minimum sample size necessary for the AAHMC sampling, including the map training, to be faster than NUTS
    """

    if speed_up / rho <= 1:
        raise ValueError(
            "Found negative sample size solution for average_time_per_sample: {} map_training_time: {} speed_up: {} rho: {}".format(
                average_time_per_sample, map_training_time, speed_up, rho
            )
        )

    #
    minimum_sample_size = map_training_time / (
        average_time_per_sample * (rho * speed_up - 1)
    )

    if minimum_sample_size > max_sample_size:
        print("Suggested minimum sample size exceeds max sample size")

    return minimum_sample_size


def plot_cumulative_sampling_time_schematic(
    speed_up, map_training_times, sample_size, average_time_per_sample, plot_settings
):
    """
    Function to plot the cumulative sampling time schematic

    TODO: all times should be expressed in eval times
    """

    # This will be in units of evaluation times
    # map_training_times = [
    #     1e0, 1e1, 1e3, 1e4, 1e5
    # ]
    # sample_size = 2000
    # average_time_per_sample = 3

    # speedup_factor = 15
    # #
    # n_factor = 3
    # dimension = 4
    # dimension_factor = 2*dimension+1

    # NUTS_times = np.ones(sample_size) * eval_time * n_factor * dimension_factor
    # AAHMC_times= np.ones(sample_size) * eval_time

    #
    AAHMC_times = np.ones(sample_size) * average_time_per_sample
    NUTS_times = AAHMC_times * speed_up

    cumulative_AAHMC_times = np.cumsum(AAHMC_times)
    cumulative_NUTS_times = np.cumsum(NUTS_times)

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(16, 16))
    gs = fig.add_gridspec(nrows=1, ncols=1)
    ax = fig.add_subplot(gs[0, :])
    scale = "log"

    # #
    # ax_linear = fig.add_subplot(gs[0, :])
    # ax_log = fig.add_subplot(gs[1, :])

    number_of_samples_array = np.array(list(range(sample_size))) + 1
    # print(number_of_samples_array)
    # quit()

    if scale == "log":

        ##################
        # Plot cumulative times
        ax.plot(
            number_of_samples_array,
            cumulative_NUTS_times,
            "r",
            label="NUTS cumulative \nsampling time",
        )
        ax.plot(
            number_of_samples_array,
            cumulative_AAHMC_times,
            "b",
            label="AAHMC cumulative \nsampling time",
        )

        #############
        # Loop over the training times
        for map_training_time in map_training_times:
            max_nuts_time = np.max(cumulative_NUTS_times)
            min_nuts_time = np.min(cumulative_NUTS_times)

            ################
            # Only plot when its within the bounds of the NUTS sampling time
            if map_training_time > max_nuts_time:
                print(
                    "map_training_time ({}) exceeds max nuts time ({})".format(
                        map_training_time, max_nuts_time
                    )
                )
                continue
            if map_training_time < min_nuts_time:
                print(
                    "map_training_time ({}) exceeds min nuts time ({})".format(
                        map_training_time, min_nuts_time
                    )
                )
                continue

            #############
            # Calaculate cumulative time AAHMC including map training time
            cumulative_AAHMC_times_plus_map_training_time = (
                cumulative_AAHMC_times + map_training_time
            )

            # Plot cumulative line
            ax.plot(
                number_of_samples_array,
                cumulative_AAHMC_times_plus_map_training_time,
                "b-.",
                alpha=0.5,
                label=(
                    "AAHMC cumulative\nsampling time\nincl. map building"
                    if plot_settings.get("add_aahmc_total_time_label", False)
                    else None
                ),
            )

            ##################
            # Draw where the cumulative NUTS sampling time and the total AAHMC (incl map) is equal.

            # draw estimate of formula
            minimum_sample_size_for_advantage = calculate_minimum_sample_size(
                map_training_time=map_training_time,
                average_time_per_sample=average_time_per_sample,
                speed_up=speed_up,
                rho=1,
                max_sample_size=sample_size,
            )

            # Draw intersection point
            ax.scatter(
                [minimum_sample_size_for_advantage],
                [
                    map_training_time
                    + minimum_sample_size_for_advantage * average_time_per_sample
                ],
                linewidth=3,
                s=1000,
                facecolors="none",
                edgecolors="blue",
                alpha=0.5,
            )

            ax.plot(
                [minimum_sample_size_for_advantage, minimum_sample_size_for_advantage],
                [
                    0,
                    map_training_time
                    + minimum_sample_size_for_advantage * average_time_per_sample,
                ],
                c="blue",
                linestyle="-.",
                alpha=0.5,
            )

            ##############
            # Draw where total AAHMC sampling time (incl map) is only fraction rho of the NUTS sampling time/
            minimum_sample_size_for_fraction = calculate_minimum_sample_size(
                map_training_time=map_training_time,
                average_time_per_sample=average_time_per_sample,
                speed_up=speed_up,
                rho=plot_settings.get("rho", 0.25),
                max_sample_size=sample_size,
            )

            ax.plot(
                [minimum_sample_size_for_fraction, minimum_sample_size_for_fraction],
                [
                    0,
                    map_training_time
                    + minimum_sample_size_for_fraction * average_time_per_sample,
                ],
                c="blue",
                linestyle="-.",
                alpha=0.5,
            )

            # Draw intersection point
            ax.scatter(
                [minimum_sample_size_for_fraction],
                [
                    map_training_time
                    + minimum_sample_size_for_fraction * average_time_per_sample
                ],
                linewidth=3,
                s=1000,
                facecolors="none",
                edgecolors="blue",
                alpha=0.5,
            )

            ########
            #
            ax.annotate(
                "",
                (
                    minimum_sample_size_for_fraction,
                    cumulative_NUTS_times[int(minimum_sample_size_for_fraction) - 1]
                    * 0.9,
                ),
                (
                    minimum_sample_size_for_fraction,
                    (
                        map_training_time
                        + minimum_sample_size_for_fraction * average_time_per_sample
                    )
                    * 1.1,
                ),
                arrowprops={"arrowstyle": "->, head_width=0.4", "lw": 4},
                zorder=100,
            )

            ax.text(
                minimum_sample_size_for_fraction * 1.1,
                cumulative_NUTS_times[int(minimum_sample_size_for_fraction) - 1]
                * plot_settings.get("factor_y_speedup_text", 0.4),
                r"$\it{\rho}$",
                fontsize=plot_settings.get("speedup_text_fontsize", 32),
                zorder=100,
            )

    ########
    # Draw speedup
    if plot_settings.get("show_speedup_text", True):
        # speedup_sample_size = int(sample_size * plot_settings.get("speedup_sample_size_fraction", 1/3))

        speedup_sample_size = 5

        ax.annotate(
            "",
            (speedup_sample_size, cumulative_NUTS_times[speedup_sample_size - 1] * 0.9),
            (
                speedup_sample_size,
                cumulative_AAHMC_times[speedup_sample_size - 1] * 1.1,
            ),
            arrowprops={"arrowstyle": "->, head_width=0.4", "lw": 4},
            zorder=100,
        )

        ########
        #
        ax.text(
            speedup_sample_size * 1.1,
            cumulative_NUTS_times[speedup_sample_size - 1]
            * plot_settings.get("factor_y_speedup_text", 0.4),
            r"n$_{speedup}$",
            fontsize=plot_settings.get("speedup_text_fontsize", 32),
            zorder=100,
        )

    ###########
    # Make up
    ax.set_yscale("log")
    ax.set_xscale("log")
    if plot_settings.get("show_title", True):
        ax.set_title(r"With a ${}\times$speed up".format(speed_up))

    ax.set_ylabel(
        r"Time $(\tau_{e})$", fontsize=plot_settings.get("label_fontsize", 32)
    )
    ax.set_xlabel(
        r"Sample size $n_{s}$", fontsize=plot_settings.get("label_fontsize", 32)
    )

    ax.legend(
        loc=2,
        fontsize=plot_settings.get("legend_fontsize", 32),
        framealpha=0.5,
        frameon=True,
    )

    # if not plot_settings.get("show_ticklabels", True):
    # ax.set_xticklabels([])
    # ax.set_yticklabels([])

    # ax.grid(True)
    # for tick in ax.xaxis.get_major_ticks():
    #     # tick.tick1line.set_visible(False)
    #     # tick.tick2line.set_visible(False)
    #     tick.label1.set_visible(False)
    #     tick.label2.set_visible(False)

    # for tick in ax.yaxis.get_major_ticks():
    #     # tick.tick1line.set_visible(False)
    #     # tick.tick2line.set_visible(False)
    #     tick.label1.set_visible(False)
    #     tick.label2.set_visible(False)

    if plot_settings.get("show_schematic_ticklabels", True):
        ax.set_yticks([average_time_per_sample, map_training_times[0]])
        ax.set_yticklabels(
            [r"t$_{e}$", r"t$_{m}$"], fontsize=plot_settings.get("label_fontsize", 32)
        )

        #
        ax.set_xticks(
            [minimum_sample_size_for_advantage, minimum_sample_size_for_fraction]
        )
        ax.set_xticklabels(
            [r"n$_{\rm{min}, \rho=1}$", r"n$_{\rm{min}, \rho}$"],
            fontsize=plot_settings.get("label_fontsize", 32),
        )

    # ax.grid(True)

    ax.set_xlim(1, sample_size)

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    plot_output_dir = os.path.join(this_file_dir, "plots/")

    # plot_cumulative_sampling_time_schematic(
    #     speed_up=15,
    #     map_training_times=[1e2, 1e3, 1e4],
    #     sample_size=20000,
    #     average_time_per_sample=3,
    #     show_ticklabels=False,
    #     plot_settings={
    #         "output_name": os.path.join(plot_output_dir, "schematic_speedup.pdf"),
    #     },
    # )

    # For in the paper
    plot_cumulative_sampling_time_schematic(
        speed_up=10,
        map_training_times=[1e3],
        sample_size=20000,
        average_time_per_sample=3,
        plot_settings={
            "output_name": os.path.join(plot_output_dir, "schematic_speedup.pdf"),
            "show_ticklabels": False,
            "show_schematic_ticklabels": True,
            "add_aahmc_total_time_label": True,
            "show_title": False,
            "legend_fontsize": 32,
            "label_fontsize": 38,
            "factor_y_speedup_text": 0.5,
            "speedup_text_fontsize": 38,
            "speedup_sample_size_fraction": 1 / 3,
            "rho": 0.25,
            # "show_plot": True
        },
    )

    # plot_cumulative_sampling_time_schematic(
    #     speed_up=15
    # )

    # plot_cumulative_sampling_time_schematic(
    #     speed_up=15
    # )
