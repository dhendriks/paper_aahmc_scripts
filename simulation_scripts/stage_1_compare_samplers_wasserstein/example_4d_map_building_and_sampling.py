"""
Script to run an example sampling of 4-d posterior that includes building the map AND sampling in 1 go.

Steps:
- train map to a certain degree
- do NOT test map during sampling (other than doing quality checks)
- sample NUTS
- sample AAHMC
- repeat N times

For this we do need to store the information of the map training.
- in particular because this contains the map training time

The purpose is to display the difference in sampling speed.
- we show sampling-only, sampling including map building
- additionally we show some indication of the equivalent sample size for NUTS

"""

import copy
import json
import logging
import os
import time
import uuid

from david_phd_functions.backup_functions.functions import backup_if_exists
from hmc_project_code.functions.map_quality_heuristic.handle_quality_check_with_AAHMC import (
    handle_quality_checks_with_AAHMC,
)
from hmc_project_code.functions.models.model_defs import (
    return_AutoDiffNewNormalPriorScaledTorchNdMVNFactorModel as return_autodiff_mvn_model,
)
from hmc_project_code.functions.models.model_defs import (
    return_NumDiffNewNormalPriorScaledTorchNdMVNFactorModel as return_numdiff_mvn_model,
)
from hmc_project_code.functions.sampling.generate_samples import generate_samples
from hmc_project_code.functions.sampling.training import (  # custom_sample_handler,
    build_neutra_model,
)

#
from paper_aahmc_scripts.project_settings import (
    project_config,
    simple_2dMVN_distribution_specifications,
    simple_4dMVN_distribution_specifications,
)


def sampler_comparison(
    result_root, config, mvn_distribution_specifications, run_autodiff, run_numdiff
):
    """
    Function to run the sampler comparison for a given, known, MVN target
    distribution we generate samples with a selection of samplers. We use
    autodiff-based models as well as numdiff based models.
    """

    ###########
    # storage management
    # os.makedirs(result_root, exist_ok=True)
    backup_if_exists(
        result_root,
        remove_old_directory_after_backup=True,
        create_empty_dir_after_backup=True,
    )

    config["logger"].setLevel(logging.INFO)

    ############
    # small extra global configuration

    ###########
    # Configuration for generate samples function
    generate_samples_args = {
        "dimensions": len(mvn_distribution_specifications["mean"]),
        "mvn_distribution_specifications": mvn_distribution_specifications,
        "config": config,
        "generate_real_samples": True,
        "run_nuts_on_target": True,
        "run_aahmc_on_target": False,
        "generate_map_regardless": False,
        "run_map_samples": False,
        "run_nuts_on_base_and_push": False,
        "run_aahmc_on_base_and_push": True,
        "verbose": 1,
    }

    # handle doing the sample loopiing here so we build the map each time
    n_samples = config["n_samples"]
    config["n_samples"] = 1

    for sample_i in range(n_samples):
        hex_ = uuid.uuid4().hex

        ######################################
        # Autodiff model and map configuration

        if run_autodiff:
            config["logger"].warning("Starting autodiff sampler comparison")

            # Set up model
            autodiff_mvn_model = return_autodiff_mvn_model(
                mvn_configuration=mvn_distribution_specifications,
                sleep_time=config["custom_sleep_time"],
                verbose=False,
            )

            # train
            autodiff_map_training_start = time.time()
            (
                autodiff_guide,
                autodiff_neutra,
                autodiff_neutra_model,
                autodiff_training_info_dict,
                autodiff_guide_samples,
            ) = build_neutra_model(model=autodiff_mvn_model, config=config)
            autodiff_map_training_stop = time.time()

            # store
            pretrained_autodiff_map_dict = {
                "guide": autodiff_guide,
                "neutra": autodiff_neutra,
                "neutra_model": autodiff_neutra_model,
                "loss_progress_bnaf": autodiff_training_info_dict["loss_progress"],
                "training_info_dict": autodiff_training_info_dict,
                "guide_samples": autodiff_guide_samples,
                "total_time": autodiff_map_training_stop - autodiff_map_training_start,
            }

            with open(
                os.path.join(result_root, "autodiff_map_building_{}.json".format(hex_)),
                "w",
            ) as f:
                data = {"total_time": pretrained_autodiff_map_dict["total_time"]}

                f.write(json.dumps(data))

            #
            generate_samples(
                **generate_samples_args,
                model=autodiff_mvn_model,
                output_filename=os.path.join(
                    result_root, "autodiff_samplers_{}.json".format(hex_)
                ),
                pretrained_map_dict=pretrained_autodiff_map_dict,
            )

        ######################################
        # numdiff model and map configuration
        if run_numdiff:
            config["logger"].warning("Starting numdiff sampler comparison")
            config["is_numdiff_model"] = True

            # disable NUTS on base. Doesnt work that well with numdiff
            generate_samples_args["run_nuts_on_base_and_push"] = False

            # Set up model
            numdiff_mvn_model = return_numdiff_mvn_model(
                mvn_configuration=mvn_distribution_specifications,
                numdiff_config=config["numdiff_config"],
                sleep_time=config["custom_sleep_time"],
            )

            # train
            numdiff_map_training_start = time.time()
            (
                numdiff_guide,
                numdiff_neutra,
                numdiff_neutra_model,
                numdiff_training_info_dict,
                numdiff_guide_samples,
            ) = build_neutra_model(model=numdiff_mvn_model, config=config)
            numdiff_map_training_stop = time.time()

            # store
            pretrained_numdiff_map_dict = {
                "guide": numdiff_guide,
                "neutra": numdiff_neutra,
                "neutra_model": numdiff_neutra_model,
                "training_info_dict": numdiff_training_info_dict,
                "loss_progress_bnaf": numdiff_training_info_dict["loss_progress"],
                "guide_samples": numdiff_guide_samples,
                "total_time": numdiff_map_training_stop - numdiff_map_training_start,
            }

            with open(
                os.path.join(result_root, "numdiff_map_building_{}.json".format(hex_)),
                "w",
            ) as f:
                data = {"total_time": pretrained_numdiff_map_dict["total_time"]}

                f.write(json.dumps(data))

            #
            generate_samples(
                **generate_samples_args,
                model=numdiff_mvn_model,
                output_filename=os.path.join(
                    result_root, "numdiff_samplers_{}.json".format(hex_)
                ),
                pretrained_map_dict=pretrained_numdiff_map_dict,
            )


def handle_paper_simulation_stage_1_sampler_comparison_general(
    mvn_distribution_specifications,
    mvn_name,
    sleep_time=0,
    run_autodiff=True,
    run_numdiff=True,
    extra_config=None,
):
    """
    General function to run a 2-d MVN sampling and compare between samplers
    """

    if extra_config is None:
        extra_config = {}

    #
    result_root = os.path.join(
        os.getenv("AAHMC_DATA_ROOT"),
        "stage_1_results",
        "sampler_comparison_{}_{}_sleep".format(mvn_name, sleep_time),
    )

    # Configure
    local_config = copy.copy(project_config)

    local_config["sample_size"] = 10000
    local_config["n_samples"] = 25
    local_config["guide_training_termination_hook"] = handle_quality_checks_with_AAHMC
    local_config["num_steps_map"] = (
        100000  # NOTE: this has to be larger than whatever largest number we check at in the array below
    )

    local_config["test_map"] = False
    local_config["generate_samples_when_calculating_quality"] = False

    # Config for the map quality check
    local_config["handle_quality_check_with_AAHMC_mean_target_quality_threshold"] = 0.3
    local_config["handle_quality_check_with_AAHMC_mean_base_quality_threshold"] = 0.99
    local_config["handle_quality_check_with_AAHMC_frequency"] = 100

    # set sleep time
    local_config["custom_sleep_time"] = sleep_time

    # update with extra config
    local_config = {**local_config, **extra_config}

    ##############
    # Run sampler comparison
    sampler_comparison(
        result_root=result_root,
        config=local_config,
        mvn_distribution_specifications=mvn_distribution_specifications,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
    )


def handle_paper_simulation_stage_1_sampler_comparison_simple_4dMVN(
    sleep_time=0, run_autodiff=True, run_numdiff=True, extra_config={}
):
    """
    Function to run a 4-d MVN sampling and compare between samplers
    """

    ###########
    # Set up standard model
    mvn_distribution_specifications = simple_4dMVN_distribution_specifications

    # Call handler
    handle_paper_simulation_stage_1_sampler_comparison_general(
        mvn_distribution_specifications=mvn_distribution_specifications,
        mvn_name="simple_4dMVN_full_example",
        sleep_time=sleep_time,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
        extra_config=extra_config,
    )


def handle_paper_simulation_stage_1_sampler_comparison_simple_2dMVN(
    sleep_time=0, run_autodiff=True, run_numdiff=True, extra_config={}
):
    """
    Function to run a 4-d MVN sampling and compare between samplers
    """

    ###########
    # Set up standard model
    mvn_distribution_specifications = simple_2dMVN_distribution_specifications

    # Call handler
    handle_paper_simulation_stage_1_sampler_comparison_general(
        mvn_distribution_specifications=mvn_distribution_specifications,
        mvn_name="simple_2dMVN_full_example",
        sleep_time=sleep_time,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
        extra_config=extra_config,
    )


if __name__ == "__main__":
    # ##########
    # # 4-d model with 0.1 sleep time
    # handle_paper_simulation_stage_1_sampler_comparison_simple_4dMVN(
    #     sleep_time=0.00025,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     extra_config={
    #         "n_samples": 10,
    #         "learning_rate_map": 5e-4,
    #         "learningrate_type": "plateau",
    #         "steps_per_epoch": 1000,
    #         "handle_quality_check_with_AAHMC_mean_target_quality_threshold": 0.7,
    #     },
    # )

    ##########
    # 4-d model with 0.1 sleep time
    handle_paper_simulation_stage_1_sampler_comparison_simple_2dMVN(
        sleep_time=0.00025,
        run_autodiff=False,
        run_numdiff=True,
        extra_config={
            "n_samples": 10,
            "learning_rate_map": 5e-3,
            "learningrate_type": "exponential",
            "steps_per_epoch": 1000,
            "handle_quality_check_with_AAHMC_mean_target_quality_threshold": 0.7,
        },
    )
