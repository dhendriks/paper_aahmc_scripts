"""
Main script to run the sampler comparison.

- the MVN is a scaled, transformed and translated MVN
- we use 50k training steps for the map
- we make two versions: numdiff-samplers, autodiff samplers
- we currently use a numpy based general MVN numdiff sampler for the numdiff

Steps:
- set up models
- train the map
- call generate samples

Next steps:
- turn to PYRO-MVN model or analyse change in evaluation time

TODO: restructure this file s.t. we dont repeat all the config each time
"""

import copy
import logging
import os
import time

import numpy as np
from david_phd_functions.backup_functions.functions import backup_if_exists
from hmc_project_code.functions.map_quality_heuristic.handle_quality_check_with_AAHMC import (
    handle_quality_checks_with_AAHMC,
)
from hmc_project_code.functions.models.model_defs import (
    return_AutoDiffNewNormalPriorScaledTorchNdMVNFactorModel as return_autodiff_mvn_model,
)
from hmc_project_code.functions.models.model_defs import (
    return_NumDiffNewNormalPriorScaledTorchNdMVNFactorModel as return_numdiff_mvn_model,
)
from hmc_project_code.functions.sampling.generate_samples import generate_samples
from hmc_project_code.functions.sampling.utility_functions import (  # custom_sample_handler,
    build_neutra_model,
)

#
from paper_aahmc_scripts.project_settings import (
    project_config,
    simple_2dMVN_distribution_specifications,
    simple_3dMVN_distribution_specifications,
    simple_4dMVN_distribution_specifications,
    simple_5dMVN_distribution_specifications,
)


def sampler_comparison(
    result_root, config, mvn_distribution_specifications, run_autodiff, run_numdiff
):
    """
    Function to run the sampler comparison for a given, known, MVN target
    distribution we generate samples with a selection of samplers. We use
    autodiff-based models as well as numdiff based models.
    """

    ###########
    # storage management
    # os.makedirs(result_root, exist_ok=True)
    backup_if_exists(result_root, remove_old_directory_after_backup=True)

    config["logger"].setLevel(logging.INFO)

    ############
    # small extra global configuration

    ###########
    # Configuration for generate samples function
    generate_samples_args = {
        "dimensions": len(mvn_distribution_specifications["mean"]),
        "mvn_distribution_specifications": mvn_distribution_specifications,
        "config": config,
        "generate_real_samples": True,
        "run_nuts_on_target": True,
        "run_aahmc_on_target": False,
        "generate_map_regardless": False,
        "run_map_samples": False,
        "run_nuts_on_base_and_push": False,
        "run_aahmc_on_base_and_push": True,
        "verbose": 1,
    }

    ######################################
    # Autodiff model and map configuration
    if run_autodiff:
        config["logger"].warning("Starting autodiff sampler comparison")

        # Set up model
        autodiff_mvn_model = return_autodiff_mvn_model(
            mvn_configuration=mvn_distribution_specifications,
            sleep_time=config["custom_sleep_time"],
            verbose=False,
        )

        # train
        autodiff_map_training_start = time.time()
        (
            autodiff_guide,
            autodiff_neutra,
            autodiff_neutra_model,
            autodiff_loss_progress_bnaf,
            autodiff_guide_samples,
        ) = build_neutra_model(model=autodiff_mvn_model, config=config)
        autodiff_map_training_stop = time.time()

        # store
        pretrained_autodiff_map_dict = {
            "guide": autodiff_guide,
            "neutra": autodiff_neutra,
            "neutra_model": autodiff_neutra_model,
            "loss_progress_bnaf": autodiff_loss_progress_bnaf,
            "guide_samples": autodiff_guide_samples,
            "total_time": autodiff_map_training_stop - autodiff_map_training_start,
        }

        #
        generate_samples(
            **generate_samples_args,
            model=autodiff_mvn_model,
            output_filename=os.path.join(result_root, "autodiff_samplers.json"),
            pretrained_map_dict=pretrained_autodiff_map_dict,
        )

    ######################################
    # numdiff model and map configuration
    if run_numdiff:
        config["logger"].warning("Starting numdiff sampler comparison")
        config["is_numdiff_model"] = True

        # disable NUTS on base. Doesnt work that well with numdiff
        generate_samples_args["run_nuts_on_base_and_push"] = False

        # Set up model
        numdiff_mvn_model = return_numdiff_mvn_model(
            mvn_configuration=mvn_distribution_specifications,
            numdiff_config=config["numdiff_config"],
            sleep_time=config["custom_sleep_time"],
        )

        # train
        numdiff_map_training_start = time.time()
        (
            numdiff_guide,
            numdiff_neutra,
            numdiff_neutra_model,
            numdiff_loss_progress_bnaf,
            numdiff_guide_samples,
        ) = build_neutra_model(model=numdiff_mvn_model, config=config)
        numdiff_map_training_stop = time.time()

        # store
        pretrained_numdiff_map_dict = {
            "guide": numdiff_guide,
            "neutra": numdiff_neutra,
            "neutra_model": numdiff_neutra_model,
            "loss_progress_bnaf": numdiff_loss_progress_bnaf,
            "guide_samples": numdiff_guide_samples,
            "total_time": numdiff_map_training_stop - numdiff_map_training_start,
        }

        #
        generate_samples(
            **generate_samples_args,
            model=numdiff_mvn_model,
            output_filename=os.path.join(result_root, "numdiff_samplers.json"),
            pretrained_map_dict=pretrained_numdiff_map_dict,
        )


def handle_paper_simulation_stage_1_sampler_comparison_general(
    mvn_distribution_specifications,
    mvn_name,
    sleep_time=0,
    run_autodiff=True,
    run_numdiff=True,
    extra_config=None,
):
    """
    General function to run a 2-d MVN sampling and compare between samplers
    """

    if extra_config is None:
        extra_config = {}

    #
    result_root = os.path.join(
        os.getenv("AAHMC_DATA_ROOT"),
        "stage_1_results",
        "sampler_comparison_{}_{}_sleep".format(mvn_name, sleep_time),
    )

    # Configure
    local_config = copy.copy(project_config)

    local_config["sample_size"] = 2000
    local_config["n_samples"] = 5
    local_config["guide_training_termination_hook"] = handle_quality_checks_with_AAHMC
    local_config["num_steps_map"] = (
        70001  # NOTE: this has to be larger than whatever largest number we check at in the array below
    )

    # Config for the map quality check
    local_config["handle_quality_check_with_AAHMC_training_step_check_array"] = [
        1,
        3,
        7,
        10,
        30,
        70,
        100,
        300,
        700,
        1000,
        3000,
        5000,
        7500,
        10000,
        12500,
        15000,
        17500,
        20000,
        22500,
        25000,
        27500,
        30000,
        32500,
        35000,
        37500,
        40000,
        42500,
        45000,
        47500,
        50000,
        52500,
        55000,
        57500,
        60000,
        62500,
        65000,
        67500,
        70000,
    ]
    local_config["handle_quality_check_with_AAHMC_frequency"] = 100
    local_config["handle_quality_check_with_AAHMC_n_quality_checks"] = 250
    local_config["handle_quality_check_with_AAHMC_mean_target_quality_threshold"] = 0.7
    local_config["handle_quality_check_with_AAHMC_mean_base_quality_threshold"] = 0.99

    # set sleep time
    local_config["custom_sleep_time"] = sleep_time

    # update with extra config
    local_config = {**local_config, **extra_config}

    ##############
    # Run sampler comparison
    sampler_comparison(
        result_root=result_root,
        config=local_config,
        mvn_distribution_specifications=mvn_distribution_specifications,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
    )


def handle_paper_simulation_stage_1_sampler_comparison_simple_2dMVN(
    sleep_time=0, run_autodiff=True, run_numdiff=True
):
    """
    Function to run a 2-d MVN sampling and compare between samplers
    """

    ############
    # Configure the distribution
    mvn_distribution_specifications = simple_2dMVN_distribution_specifications

    #
    extra_config = {}

    # Call handler
    handle_paper_simulation_stage_1_sampler_comparison_general(
        mvn_distribution_specifications=mvn_distribution_specifications,
        mvn_name="simple_2dMVN",
        sleep_time=sleep_time,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
        extra_config=extra_config,
    )


def handle_paper_simulation_stage_1_sampler_comparison_simple_3dMVN(
    sleep_time=0, run_autodiff=True, run_numdiff=True
):
    """
    Function to run a 3-d MVN sampling and compare between samplers
    """

    ###########
    # Set up standard model
    mvn_distribution_specifications = simple_3dMVN_distribution_specifications

    #
    extra_config = {}

    # Call handler
    handle_paper_simulation_stage_1_sampler_comparison_general(
        mvn_distribution_specifications=mvn_distribution_specifications,
        mvn_name="simple_3dMVN",
        sleep_time=sleep_time,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
        extra_config=extra_config,
    )


def handle_paper_simulation_stage_1_sampler_comparison_simple_4dMVN(
    sleep_time=0, run_autodiff=True, run_numdiff=True
):
    """
    Function to run a 4-d MVN sampling and compare between samplers
    """

    ###########
    # Set up standard model
    mvn_distribution_specifications = simple_4dMVN_distribution_specifications

    #
    extra_config = {}

    # Call handler
    handle_paper_simulation_stage_1_sampler_comparison_general(
        mvn_distribution_specifications=mvn_distribution_specifications,
        mvn_name="simple_4dMVN",
        sleep_time=sleep_time,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
        extra_config=extra_config,
    )


def handle_paper_simulation_stage_1_sampler_comparison_simple_5dMVN(
    sleep_time=0, run_autodiff=True, run_numdiff=True
):
    """
    Function to run a 5-d MVN sampling and compare between samplers
    """

    ###########
    # Set up standard model
    mvn_distribution_specifications = simple_5dMVN_distribution_specifications

    #
    result_root = os.path.join(
        os.getenv("AAHMC_DATA_ROOT"),
        "stage_1_results",
        "sampler_comparison_simple_5dMVN_{}_sleep".format(sleep_time),
    )

    # Configure
    local_config = copy.copy(project_config)

    local_config["sample_size"] = 2000
    local_config["n_samples"] = 10
    local_config["guide_training_termination_hook"] = handle_quality_checks_with_AAHMC
    local_config["num_steps_map"] = (
        70001  # NOTE: this has to be larger than whatever largest number we check at in the array below
    )

    # Config for the map quality check
    local_config["handle_quality_check_with_AAHMC_training_step_check_array"] = [
        1,
        3,
        7,
        10,
        30,
        70,
        100,
        300,
        700,
        1000,
        3000,
        5000,
        10000,
        12500,
        15000,
        17500,
        20000,
        22500,
        25000,
        27500,
        30000,
        32500,
        35000,
        37500,
        40000,
        42500,
        45000,
        47500,
        50000,
        52500,
        55000,
        57500,
        60000,
        62500,
        65000,
        67500,
        70000,
    ]
    local_config["handle_quality_check_with_AAHMC_frequency"] = 100
    local_config["handle_quality_check_with_AAHMC_n_quality_checks"] = 100
    local_config["handle_quality_check_with_AAHMC_mean_target_quality_threshold"] = 0.7
    local_config["handle_quality_check_with_AAHMC_mean_base_quality_threshold"] = 0.99

    # set sleep time
    local_config["custom_sleep_time"] = sleep_time

    ##############
    # Run sampler comparison
    sampler_comparison(
        result_root=result_root,
        config=local_config,
        mvn_distribution_specifications=mvn_distribution_specifications,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
    )


if __name__ == "__main__":

    ###############
    # # 2-d model without sleep time
    # handle_paper_simulation_stage_1_sampler_comparison_simple_2dMVN()

    # # 2-d model with 0.1s sleep time
    # handle_paper_simulation_stage_1_sampler_comparison_simple_2dMVN(sleep_time=0.01)

    ###############
    # # 3-d model without sleep time
    # handle_paper_simulation_stage_1_sampler_comparison_simple_3dMVN()

    # # 3-d model with 0.1 sleep time
    # handle_paper_simulation_stage_1_sampler_comparison_simple_3dMVN(
    #     sleep_time=0.01, run_autodiff=False
    # )

    ###############
    # 4-d model without sleep time
    # handle_paper_simulation_stage_1_sampler_comparison_simple_4dMVN()

    # 4-d model with 0.1 sleep time
    handle_paper_simulation_stage_1_sampler_comparison_simple_4dMVN(
        sleep_time=0.01, run_autodiff=False
    )

    ###############
    # # 5-d model without sleep time
    # handle_paper_simulation_stage_1_sampler_comparison_simple_5dMVN()

    # # 5-d model with 0.1 sleep time
    # handle_paper_simulation_stage_1_sampler_comparison_simple_5dMVN(sleep_time=0.01)
