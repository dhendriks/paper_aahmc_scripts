import pyro
import pyro.optim as pyroopt
import torch
import torch.nn as nn
import torch.optim as optim


# Define a simple model
class SimpleModel(nn.Module):
    def __init__(self):
        super(SimpleModel, self).__init__()
        self.fc = nn.Linear(10, 1)

    def forward(self, x):
        return self.fc(x)


# Initialize model, criterion, optimizer
model = SimpleModel()
criterion = nn.MSELoss()

# Define the optimizer
base_optimizer = optim.SGD
optim_args = {"lr": 0.01}
pyro_optimizer = pyroopt.PyroOptim(base_optimizer, optim_args)

# Define the scheduler with patience
scheduler = pyroopt.ReduceLROnPlateau(
    {
        "optimizer": base_optimizer,
        "optim_args": optim_args,
        "mode": "min",
        "factor": 0.1,
        "patience": 5,
        "threshold": 0.01,
        "min_lr": 1e-6,
        "cooldown": 0,
    }
)


# Function to simulate validation loss
def simulate_validation_loss(epoch):
    # Simulate a validation loss that decreases initially, then plateaus
    if epoch < 5:
        return 1.0 / (epoch + 1)
    else:
        return 0.2 + (epoch % 3) * 0.01  # Some oscillation around a small value


# Training loop
for epoch in range(20):
    # Simulate a training step
    dummy_input = torch.randn(10)
    target = torch.randn(1)

    # Access the underlying PyTorch optimizer

    print(pyro_optimizer.optim_objs)
    optimizer = pyro_optimizer.optim_objs[0]

    optimizer.zero_grad()
    output = model(dummy_input)
    loss = criterion(output, target)
    loss.backward()
    optimizer.step()

    # Simulate validation loss
    val_loss = simulate_validation_loss(epoch)

    # Step the scheduler with the validation loss
    scheduler.step(val_loss)

    # Print current learning rate
    current_lr = optimizer.param_groups[0]["lr"]
    print(f"Epoch {epoch+1}: val_loss={val_loss:.4f}, lr={current_lr:.6f}")

    # Print the best observed validation loss and patience-related info
    best_val_loss = scheduler.optim_objs[0].scheduler.best
    num_bad_epochs = scheduler.optim_objs[0].scheduler.num_bad_epochs
    print(f"Best observed val_loss up to now: {best_val_loss:.4f}")
    print(f"Number of bad epochs: {num_bad_epochs}")

    # Check if learning rate should be reduced
    if num_bad_epochs >= scheduler.optim_objs[0].scheduler.patience:
        print("Learning rate should be reduced now.")
