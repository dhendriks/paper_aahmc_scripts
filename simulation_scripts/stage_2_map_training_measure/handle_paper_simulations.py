"""
Functions to run simulations to test the map training

We choose a known controlled target distribution MVN
- the MVN is a scaled, transformed and translated MVN
- we use 50k training steps for the map
- we make two versions: numdiff-samplers, autodiff samplers
- we currently use a numpy based general MVN numdiff sampler for the numdiff

TODO: make the call to the sim handler more general
TODO: define the name entirely through argument except for some basename (now its half half)
TODO: make the output pickle files names random instead of incrementing number.
"""

import copy
import os
import pickle
import uuid
from functools import partial

from david_phd_functions.backup_functions.functions import backup_if_exists
from hmc_project_code.functions.map_quality_heuristic.test_map_quality_correlation import (
    test_map_quality,
)
from hmc_project_code.functions.models.model_defs import (
    return_AutoDiffNewNormalPriorScaledTorchNdMVNFactorModel as return_autodiff_mvn_model,
)
from hmc_project_code.functions.models.model_defs import (
    return_NumDiffNewNormalPriorScaledTorchNdMVNFactorModel as return_numdiff_mvn_model,
)

#
from paper_aahmc_scripts.project_settings import (
    project_config,
    simple_2dMVN_distribution_specifications,
    simple_3dMVN_distribution_specifications,
    simple_4dMVN_distribution_specifications,
    simple_5dMVN_distribution_specifications,
)
from paper_aahmc_scripts.simulation_scripts.stage_2_map_training_measure.randomize_target_training_times import (
    generate_random_traing_steps,
)


def set_random_training_steps(
    config,
    lower_num_training_steps=1,
    upper_num_training_steps=25000,
    num_target_steps=20,
):
    """
    Function to set random training steps
    """

    #
    config["testing_array"] = generate_random_traing_steps(
        lower_num_training_steps=lower_num_training_steps,
        upper_num_training_steps=upper_num_training_steps,
        num_target_steps=num_target_steps,
    )
    config["num_steps_map"] = config["testing_array"][-1] + 5

    return config


def map_quality_test(
    result_root,
    mvn_distribution_specifications,
    num_tests,
    num_checks,
    num_samples,
    num_flows_map,
    run_autodiff,
    run_numdiff,
    sample_size=1000,
    sleep_time=0,
    extra_config=None,
    use_random_training_steps=False,
    learningrate_type="fixed",
):
    """
    Function that handles the quality testing.
    """

    if extra_config is None:
        extra_config = {}

    ###########
    # storage management
    os.makedirs(result_root, exist_ok=True)

    #
    local_config = copy.deepcopy(project_config)

    # Configure
    local_config["n_warmup"] = 0
    local_config["num_flows_map"] = num_flows_map
    local_config["sample_size"] = sample_size
    local_config["n_samples"] = num_samples

    local_config["testing_array"] = [
        1,
        # 3,
        5,
        10,
        # 30,
        50,
        100,
        # 300,
        500,
        1000,
        1500,
        3000,
        4000,
        5000,
        # 6250,
        7500,
        # 8750,
        10000,
        # 11250,
        12500,
        # 13750,
        15000,
        # 16250,
        17500,
        # 18750,
        20000,
        # 21250,
        22500,
        # 23750,
        25000,
    ]
    local_config["num_length_tail_loss"] = 200
    local_config["steps_per_epoch"] = 1000
    local_config["num_quality_measures"] = num_checks
    local_config["learningrate_type"] = learningrate_type

    # update with some overriding things.
    local_config = {**local_config, **extra_config}
    local_config["num_steps_map"] = local_config["testing_array"][-1] + 5

    ###########
    # Configuration for generate samples function
    generate_samples_args = {
        "dimensions": len(mvn_distribution_specifications["mean"]),
        "mvn_distribution_specifications": mvn_distribution_specifications,
        "config": local_config,
        "generate_real_samples": True,
        "run_nuts_on_target": False,
        "run_aahmc_on_target": False,
        "generate_map_regardless": False,
        "run_map_samples": False,
        "run_nuts_on_base_and_push": False,
        "run_aahmc_on_base_and_push": True,
        "verbose": 1,
    }

    local_config["generate_samples_args"] = generate_samples_args

    ############
    # Autodiff model
    if run_autodiff:
        #
        result_dir = os.path.join(result_root, "autodiff")
        backup_if_exists(result_dir, remove_old_directory_after_backup=True)
        os.makedirs(result_dir, exist_ok=True)
        local_config["is_numdiff_model"] = False

        #
        print("Running map-quality tests with auto-differentiation model")
        for test_round_i in range(num_tests):

            ##########
            if use_random_training_steps:
                local_config = set_random_training_steps(config=local_config)

            print(
                "Checking the map training at {} traing steps".format(
                    local_config["testing_array"]
                )
            )

            ###########
            result_list = test_map_quality(
                config=local_config,
                mvn_distribution_specifications=mvn_distribution_specifications,
                model_generator_function=return_autodiff_mvn_model,
            )

            # Write result to output dir
            # output_file = os.path.join(result_dir, "{}.pickle".format(test_round_i))
            output_file = os.path.join(result_dir, "{}.pickle".format(uuid.uuid4().hex))
            pickle.dump(result_list, open(output_file, "wb"))

            #
            print("wrote results to {}".format(output_file))

    ############
    # Autodiff model
    if run_numdiff:
        #
        result_dir = os.path.join(result_root, "numdiff")
        backup_if_exists(result_dir, remove_old_directory_after_backup=True)
        os.makedirs(result_dir, exist_ok=True)
        local_config["is_numdiff_model"] = True

        bound_return_numdiff_mvn_model = partial(
            return_numdiff_mvn_model,
            numdiff_config=local_config["numdiff_config"],
            sleep_time=sleep_time,
        )

        #
        print("Running map-quality tests with numerical-differentiation model")
        for test_round_i in range(num_tests):

            ##########
            if use_random_training_steps:
                local_config = set_random_training_steps(config=local_config)

            print(
                "Checking the map training at {} traing steps".format(
                    local_config["testing_array"]
                )
            )

            ###########
            #
            result_list = test_map_quality(
                config=local_config,
                mvn_distribution_specifications=mvn_distribution_specifications,
                model_generator_function=bound_return_numdiff_mvn_model,
            )

            # Write result to output dir
            # output_file = os.path.join(result_dir, "{}.pickle".format(test_round_i))
            output_file = os.path.join(result_dir, "{}.pickle".format(uuid.uuid4().hex))
            pickle.dump(result_list, open(output_file, "wb"))

            #
            print("wrote results to {}".format(output_file))


# TODO: all these can just be generalized and bound
def handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    num_tests=3,
    num_checks=100,
    num_samples=5,
    num_flows_map=1,
    sample_size=1000,
    run_autodiff=True,
    run_numdiff=True,
    postfix="",
    sleep_time=0,
    use_random_training_steps=False,
    learningrate_type="fixed",
    extra_config=None,
):
    """
    Function to run the stage 2 map quality simulations for the paper
    """

    ############
    # Test specifications
    mvn_distribution_specifications = simple_2dMVN_distribution_specifications

    #
    result_root = os.path.join(
        os.getenv("AAHMC_DATA_ROOT"),
        "stage_2_results",
        "map_quality_tests",
        "simple_2dMVN_{}_checks{}_{}sleep".format(num_checks, postfix, sleep_time),
    )

    #
    map_quality_test(
        result_root=result_root,
        mvn_distribution_specifications=mvn_distribution_specifications,
        num_tests=num_tests,
        num_checks=num_checks,
        num_samples=num_samples,
        num_flows_map=num_flows_map,
        sleep_time=sleep_time,
        sample_size=sample_size,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
        use_random_training_steps=use_random_training_steps,
        learningrate_type=learningrate_type,
        extra_config=extra_config,
    )


# TODO: all these can just be generalized and bound
def handle_paper_simulation_stage_2_map_training_measure_simple_3dMVN(
    num_tests=3,
    num_checks=100,
    num_samples=5,
    run_autodiff=True,
    run_numdiff=True,
    postfix="",
    sleep_time=0,
    use_random_training_steps=False,
):
    """
    Function to run the stage 2 map quality simulations for the paper
    """

    ############
    # Test specifications
    mvn_distribution_specifications = simple_3dMVN_distribution_specifications

    #
    result_root = os.path.join(
        os.getenv("AAHMC_DATA_ROOT"),
        "stage_2_results",
        "map_quality_tests",
        "simple_3dMVN_{}_checks{}_{}sleep".format(num_checks, postfix, sleep_time),
    )

    #
    map_quality_test(
        result_root=result_root,
        mvn_distribution_specifications=mvn_distribution_specifications,
        num_tests=num_tests,
        num_checks=num_checks,
        num_samples=num_samples,
        sleep_time=sleep_time,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
        use_random_training_steps=use_random_training_steps,
    )


def handle_paper_simulation_stage_2_map_training_measure_simple_4dMVN(
    num_tests=3,
    num_checks=100,
    num_samples=5,
    run_autodiff=True,
    run_numdiff=True,
    postfix="",
    sleep_time=0,
    use_random_training_steps=False,
):
    """
    Function to run the stage 2 map quality simulations for the paper for a 4 dimensional Gaussian
    """

    ###########
    # Set up standard model
    mvn_distribution_specifications = simple_4dMVN_distribution_specifications

    #
    result_root = os.path.join(
        os.getenv("AAHMC_DATA_ROOT"),
        "stage_2_results",
        "map_quality_tests",
        "simple_4dMVN_{}_checks{}_{}sleep".format(num_checks, postfix, sleep_time),
    )

    #
    map_quality_test(
        result_root=result_root,
        mvn_distribution_specifications=mvn_distribution_specifications,
        num_tests=num_tests,
        num_checks=num_checks,
        sleep_time=sleep_time,
        num_samples=num_samples,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
        use_random_training_steps=use_random_training_steps,
    )


def handle_paper_simulation_stage_2_map_training_measure_simple_5dMVN(
    num_tests=3,
    num_checks=100,
    num_samples=5,
    run_autodiff=True,
    run_numdiff=True,
    postfix="",
    sleep_time=0,
):
    """
    Function to run the stage 2 map quality simulations for the paper for a 5 dimensional Gaussian
    """

    ###########
    # Set up standard model
    mvn_distribution_specifications = simple_5dMVN_distribution_specifications

    #
    result_root = os.path.join(
        os.getenv("AAHMC_DATA_ROOT"),
        "stage_2_results",
        "map_quality_tests",
        "simple_5dMVN_{}_checks{}_{}sleep".format(num_checks, postfix, sleep_time),
    )

    #
    map_quality_test(
        result_root=result_root,
        mvn_distribution_specifications=mvn_distribution_specifications,
        num_tests=num_tests,
        num_checks=num_checks,
        sleep_time=sleep_time,
        num_samples=num_samples,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
    )


if __name__ == "__main__":
    # Example
    # # 2-d MVN with 10 quality checks
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=50, num_checks=10, run_autodiff=True, run_numdiff=True
    # )

    ###############
    # Structured comparison study

    ###############
    # Dimension variations

    # # 2-d MVN with 100 quality checks
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    # )

    # # 3-d MVN with 100 quality checks
    # handle_paper_simulation_stage_2_map_training_measure_simple_3dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    # )

    # # DONE: 4-d MVN with 100 quality checks
    # handle_paper_simulation_stage_2_map_training_measure_simple_4dMVN(
    #     num_tests=5,
    #     num_checks=100,
    #     num_samples=1,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    # )

    ##############
    # Quality check variations

    # # DONE: 2-d MVN with 20 quality checks
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=20,
    #     num_samples=1,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    # )

    # # DONE: 2-d MVN with 500 quality checks
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=500,
    #     num_samples=1,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    # )

    ##############
    # Sample-size variations

    # # DONE: 2-d MVN with sample size of 500
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     sample_size=500,
    #     postfix="_sample_size_500_randomized_test",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    # )

    # # DONE: 2-d MVN with sample size of 2000
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=6,
    #     num_checks=100,
    #     num_samples=1,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     sample_size=2000,
    #     postfix="_sample_size_2000_randomized_test",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    # )

    # # DONE: 2-d MVN with sample size of 4000
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=10,
    #     num_checks=100,
    #     num_samples=1,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     sample_size=4000,
    #     postfix="_sample_size_4000_randomized_test",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    # )

    # # #############
    # # evalation time variations

    # # DONE: smaller evaluation time
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test",
    #     sleep_time=0.00025,
    #     use_random_training_steps=True,
    # )

    # # DONE: Larger evaluation time
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test",
    #     sleep_time=0.01,
    #     use_random_training_steps=True,
    # )

    ##############
    # Normalizing flow property variations

    # # DONE: two flows
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=2,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_2_num_flows",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    # )

    # # TODO: three flows
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=3,
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_3_num_flows",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    # )

    ##############
    # Map training variations: fixed learning rate variation

    # # IN PROGRESS: larger fixed learning rate
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=16,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=3,
    #     learningrate_type="fixed",
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_learning_rate_5e-2_three_flows",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    #     extra_config={"learning_rate_map": 5e-2},
    # )

    # # DONE: smaller fixed learning rate
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=3,
    #     learningrate_type="fixed",
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_learning_rate_5e-4_three_flows",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    #     extra_config={"learning_rate_map": 5e-4},
    # )

    ##############
    # Map training variations: scheduler variation

    # # DONE: plateau-based learning rate
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=3,
    #     learningrate_type="plateau",
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_plateau_learning_rate_three_flows",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    #     extra_config={"learning_rate_map": 5e-2},
    # )

    # # DONE: linear learning rate
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=22,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=3,
    #     learningrate_type="linear",
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_linear_learning_rate_three_flows",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    #     extra_config={"learning_rate_map": 5e-2},
    # )

    # # DONE: exponential learning rate
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=22,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=3,
    #     learningrate_type="exponential",
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_exponential_learning_rate_three_flows",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    #     extra_config={"learning_rate_map": 5e-2},
    # )

    # #############
    # # Map training variations: scheduler variation with initial learning rate = 5e-3

    # # DONE: plateau-based learning rate
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=3,
    #     learningrate_type="plateau",
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_plateau_learning_rate_three_flows_learning_rate_5e-3",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    #     extra_config={"learning_rate_map": 5e-3},
    # )

    # # Done: linear learning rate
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=8,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=3,
    #     learningrate_type="linear",
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_linear_learning_rate_three_flows_learning_rate_5e-3",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    #     extra_config={"learning_rate_map": 5e-3},
    # )

    # # DONE: exponential learning rate
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=3,
    #     learningrate_type="exponential",
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_exponential_learning_rate_three_flows_learning_rate_5e-3",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    #     extra_config={"learning_rate_map": 5e-3},
    # )

    # #############
    # # Map training variations: scheduler variation with initial learning rate = 5e-4

    # # DONE: plateau-based learning rate
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=3,
    #     learningrate_type="plateau",
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_plateau_learning_rate_three_flows_learning_rate_5e-4",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    #     extra_config={"learning_rate_map": 5e-4},
    # )

    # # DONE: linear learning rate
    # handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    #     num_tests=25,
    #     num_checks=100,
    #     num_samples=1,
    #     num_flows_map=3,
    #     learningrate_type="linear",
    #     run_autodiff=False,
    #     run_numdiff=True,
    #     postfix="_randomized_test_linear_learning_rate_three_flows_learning_rate_5e-4",
    #     sleep_time=0.0025,
    #     use_random_training_steps=True,
    #     extra_config={"learning_rate_map": 5e-4},
    # )

    # TODO: exponential learning rate
    handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
        num_tests=23,
        num_checks=100,
        num_samples=1,
        num_flows_map=3,
        learningrate_type="exponential",
        run_autodiff=False,
        run_numdiff=True,
        postfix="_randomized_test_exponential_learning_rate_three_flows_learning_rate_5e-4",
        sleep_time=0.0025,
        use_random_training_steps=True,
        extra_config={"learning_rate_map": 5e-4},
    )
