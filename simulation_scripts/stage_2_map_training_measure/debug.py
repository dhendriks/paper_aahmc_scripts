"""
Functions to run simulations to test the map training

We choose a known controlled target distribution MVN
- the MVN is a scaled, transformed and translated MVN
- we use 50k training steps for the map
- we make two versions: numdiff-samplers, autodiff samplers
- we currently use a numpy based general MVN numdiff sampler for the numdiff
"""

import copy
import logging
import os
import pickle
import time
from functools import partial

import numpy as np
from david_phd_functions.backup_functions.functions import backup_if_exists
from hmc_project_code.functions.action_angle.angle_step_methods import (
    quarter_phase_shift,
)
from hmc_project_code.functions.map_quality_heuristic.handle_quality_check_with_AAHMC import (
    handle_quality_checks_with_AAHMC,
)
from hmc_project_code.functions.map_quality_heuristic.test_map_quality_correlation import (
    test_map_quality,
)
from hmc_project_code.functions.models.model_defs import (
    return_AutoDiffNewNormalPriorScaledTorchNdMVNFactorModel as return_autodiff_mvn_model,
)
from hmc_project_code.functions.models.model_defs import (
    return_NumDiffNewNormalPriorScaledTorchNdMVNFactorModel as return_numdiff_mvn_model,
)
from hmc_project_code.functions.sampling.generate_samples import generate_samples
from hmc_project_code.functions.sampling.utility_functions import (
    build_neutra_model,
    train_and_return_neutra_model,
)
from pyro.distributions.transforms import block_autoregressive, iterated
from pyro.infer.autoguide import AutoNormalizingFlow

#
from paper_aahmc_scripts.project_settings import project_config


def map_quality_test(
    result_root,
    mvn_distribution_specifications,
    num_tests,
    run_autodiff,
    run_numdiff,
    extra_config=None,
):
    """
    Function that handles the quality testing.
    """

    if extra_config is None:
        extra_config = {}

    ###########
    # storage management
    os.makedirs(result_root, exist_ok=True)

    #
    local_config = copy.deepcopy(project_config)

    # Configure
    local_config["n_warmup"] = 0
    local_config["sample_size"] = 1000
    local_config["n_samples"] = 10
    # local_config["testing_frequency"] = 250
    local_config["testing_array"] = [
        1,
        3,
        7,
        10,
        30,
        70,
        100,
        300,
        700,
        1000,
        3000,
        5000,
        10000,
        15000,
        20000,
        25000,
    ]
    local_config["num_steps_map"] = local_config["testing_array"][-1] + 5
    local_config["num_length_tail_loss"] = 200
    local_config["num_quality_measures"] = 1000

    # update with some overriding things.
    local_config = {**local_config, **extra_config}

    ###########
    # Configuration for generate samples function
    generate_samples_args = {
        "dimensions": len(mvn_distribution_specifications["mean"]),
        "mvn_distribution_specifications": mvn_distribution_specifications,
        "config": local_config,
        "generate_real_samples": True,
        "run_nuts_on_target": False,
        "run_aahmc_on_target": False,
        "generate_map_regardless": False,
        "run_map_samples": False,
        "run_nuts_on_base_and_push": False,
        "run_aahmc_on_base_and_push": True,
        "verbose": 1,
    }

    local_config["generate_samples_args"] = generate_samples_args

    ############
    # Autodiff model
    if run_autodiff:
        #
        result_dir = os.path.join(result_root, "autodiff")
        backup_if_exists(result_dir, remove_old_directory_after_backup=True)
        os.makedirs(result_dir, exist_ok=True)
        local_config["is_numdiff_model"] = False

        #
        print("Running map-quality tests with auto-differentiation model")
        for test_round_i in range(num_tests):
            result_list = test_map_quality(
                config=local_config,
                mvn_distribution_specifications=mvn_distribution_specifications,
                model_generator_function=return_autodiff_mvn_model,
            )

            # Write result to output dir
            output_file = os.path.join(result_dir, "{}.pickle".format(test_round_i))
            pickle.dump(result_list, open(output_file, "wb"))

            #
            print("wrote results to {}".format(output_file))

    ############
    # Autodiff model
    if run_numdiff:
        #
        result_dir = os.path.join(result_root, "numdiff")
        backup_if_exists(result_dir, remove_old_directory_after_backup=True)
        os.makedirs(result_dir, exist_ok=True)
        local_config["is_numdiff_model"] = True

        bound_return_numdiff_mvn_model = partial(
            return_numdiff_mvn_model, numdiff_config=local_config["numdiff_config"]
        )

        #
        print("Running map-quality tests with numerical-differentiation model")
        for test_round_i in range(num_tests):
            result_list = test_map_quality(
                config=local_config,
                mvn_distribution_specifications=mvn_distribution_specifications,
                model_generator_function=bound_return_numdiff_mvn_model,
            )

            # Write result to output dir
            output_file = os.path.join(result_dir, "{}.pickle".format(test_round_i))
            pickle.dump(result_list, open(output_file, "wb"))

            #
            print("wrote results to {}".format(output_file))


def handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
    num_tests=3, run_autodiff=True, run_numdiff=True
):
    """
    Function to run the stage 2 map quality simulations for the paper
    """

    ############
    # Test specifications
    mvn_distribution_specifications = {
        "mean": np.array(
            [60.0, 20.0],
            # dtype=np.float64
        ),
        "covariance": np.array(
            [[5.0, 0.0], [0.0, 5.0]],
            # dtype=np.float64
        ),
    }

    #
    result_root = os.path.join(
        os.getenv("AAHMC_DATA_ROOT"),
        "stage_2_results",
        "map_quality_tests",
        "simple_2dMVN",
    )

    #
    map_quality_test(
        result_root=result_root,
        mvn_distribution_specifications=mvn_distribution_specifications,
        num_tests=num_tests,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
    )


def handle_paper_simulation_stage_2_map_training_measure_simple_5dMVN(
    num_tests=3, run_autodiff=True, run_numdiff=True
):
    """
    Function to run the stage 2 map quality simulations for the paper for a 5 dimensional Gaussian
    """

    ###########
    # Set up standard model
    mvn_distribution_specifications = {
        "mean": np.array(
            [
                10.0,
                5.0,
                80.0,
                1.0,
                -4.0,
            ]
        ),
        "covariance": np.array(
            [
                [5.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 2.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 9.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.5, 0.0],
                [0.0, 0.0, 0.0, 0.0, 2.0],
            ]
        ),
    }

    #
    result_root = os.path.join(
        os.getenv("AAHMC_DATA_ROOT"),
        "stage_2_results",
        "map_quality_tests",
        "simple_5dMVN",
    )

    #
    map_quality_test(
        result_root=result_root,
        mvn_distribution_specifications=mvn_distribution_specifications,
        num_tests=num_tests,
        run_autodiff=run_autodiff,
        run_numdiff=run_numdiff,
    )


# def handle_test_simulation_stage_2_sampler_comparison():
#     """
#     Function to debug
#     """


#     #
#     result_root = os.path.join(
#             os.getenv("AAHMC_DATA_ROOT"),
#             "stage_1_results_test", "sampler_comparison"
#         )

#     ############
#     # Test specifications
#     mvn_distribution_specifications = {
#         "mean": np.array([60.0, 20.0],
#             # dtype=np.float64
#         ),
#         "covariance": np.array([[5.0, 0.0], [0.0, 5.0]],
#             # dtype=np.float64
#         ),
#     }

#     #
#     local_config = copy.deepcopy(project_config)

#     # Configure
#     local_config["sample_size"] = 20
#     local_config["n_samples"] = 2
#     local_config["n_warmup"] = 0

#     local_config["num_flows_map"] = 1
#     local_config["num_steps_map"] = 20

#     #
#     local_config['handle_quality_check_with_AAHMC_frequency'] = 100
#     local_config['handle_quality_check_with_AAHMC_n_quality_checks'] = 50
#     local_config['guide_training_termination_hook'] = handle_quality_checks_with_AAHMC


#     ##############
#     # Run sampler comparison
#     sampler_comparison(
#         result_root=result_root,
#         config=local_config,
#         mvn_distribution_specifications=mvn_distribution_specifications,
#         run_autodiff=True,
#         run_numdiff=True
#     )


if __name__ == "__main__":

    # to test
    handle_paper_simulation_stage_2_map_training_measure_simple_2dMVN(
        num_tests=1, run_autodiff=True
    )

    #
    # handle_paper_simulation_stage_2_map_training_measure_simple_5dMVN()
