import os

from handle_paper_simulations import map_quality_test

#
from paper_aahmc_scripts.project_settings import (
    project_config,
    simple_2dMVN_distribution_specifications,
)

num_checks = 100
num_samples = 1
num_flows_map = 1
sleep_time = 0.00025
num_tests = 1
sample_size = 1000
run_autodiff = False
run_numdiff = True
use_plateau_learningrate_scheduler = True
use_random_training_steps = False
extra_config = {"testing_array": [10000]}

############
# Test specifications
mvn_distribution_specifications = simple_2dMVN_distribution_specifications

#
result_root = os.path.join(
    os.getenv("AAHMC_DATA_ROOT"),
    "stage_2_results",
    "map_quality_tests",
    "scheduler_test",
)

#
map_quality_test(
    result_root=result_root,
    mvn_distribution_specifications=mvn_distribution_specifications,
    num_tests=num_tests,
    num_checks=num_checks,
    num_samples=num_samples,
    num_flows_map=num_flows_map,
    use_plateau_learningrate_scheduler=use_plateau_learningrate_scheduler,
    sleep_time=sleep_time,
    sample_size=sample_size,
    run_autodiff=run_autodiff,
    run_numdiff=run_numdiff,
    use_random_training_steps=use_random_training_steps,
    extra_config=extra_config,
)
