import numpy as np


def generate_random_traing_steps(
    lower_num_training_steps, upper_num_training_steps, num_target_steps
):
    """
    Function to generate a bunch of random training steps
    """

    # transform to log10
    log10lower_num_training_steps = np.log10(lower_num_training_steps)
    log10upper_num_training_steps = np.log10(upper_num_training_steps)

    # get range
    log10_range = log10upper_num_training_steps - log10lower_num_training_steps

    # generate random uniform sample
    random_sample = np.random.uniform(size=num_target_steps)

    # scale and shift
    log10num_training_steps = log10lower_num_training_steps + (
        random_sample * log10_range
    )

    # exponentiate, make int, sort
    num_training_steps = np.sort(
        np.array(np.ceil(10**log10num_training_steps), dtype=int)
    )

    # check if there are no unique ones
    if len(np.unique(num_training_steps)) == len(num_training_steps):
        return num_training_steps

    # else, call self again
    return generate_random_traing_steps(
        lower_num_training_steps=lower_num_training_steps,
        upper_num_training_steps=upper_num_training_steps,
        num_target_steps=num_target_steps,
    )


#
if __name__ == "__main__":
    lower_num_training_steps = 1
    upper_num_training_steps = 25000
    num_target_steps = 20

    num_training_steps = generate_random_traing_steps(
        lower_num_training_steps, upper_num_training_steps, num_target_steps
    )

    # print(num_training_steps)
    # for num in num_training_steps:
    #     print("{}".format(num))
