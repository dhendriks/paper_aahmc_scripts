"""
Emcee function used in the bayesfit routines
"""

from multiprocessing import Pool

import emcee
import numpy as np

# #%% emcee
# def main(nsteps,nwalkers,param_opt,mp,backendfile):


def bayesfit_emcee_routine(initial_position_array, posterior_function, config):
    """
    General bayesfit routine emcee
    """

    #
    emcee_config = config["emcee_config"]

    #
    print("...using {} walkers...".format(emcee_config["n_walkers"]))

    # Unpack some stuff
    backendfile = emcee_config["backendfile"]
    nwalkers = emcee_config["n_walkers"]
    nsteps = emcee_config["n_steps"]
    restart = emcee_config["restart"]

    # Number of dimensions
    ndim = len(initial_position_array)

    # Set random number seed
    np.random.seed(1234)

    #
    param_opt = initial_position_array
    # pos = param_opt + 1e-4 * np.random.randn(nwalkers, ndim) # starting point(s) of the walkers

    # ############
    # # Set up multiprocessing pool run samplers
    # with Pool() as pool:
    #     print("...in parallel mode...")

    #     backend = emcee.backends.HDFBackend(backendfile)
    #     if restart:
    #         sampler = emcee.EnsembleSampler(
    #             nwalkers, ndim, posterior_function, pool=pool, backend=backend
    #         )
    #         pos = sampler.get_last_sample()
    #     else:
    #         pos = param_opt + 1e-2 * np.random.randn(
    #             nwalkers, ndim
    #         )  # starting point(s) of the walkers

    #         print('yo!')
    #         print(pos)

    #         backend.reset(nwalkers, ndim)
    #         sampler = emcee.EnsembleSampler(
    #             nwalkers, ndim, posterior_function, pool=pool, backend=backend
    #         )

    #         print('yo')

    pos = param_opt + 1e-2 * np.random.randn(
        nwalkers, ndim
    )  # starting point(s) of the walkers

    print("yo!")
    print(pos)

    backend = emcee.backends.HDFBackend(backendfile)
    backend.reset(nwalkers, ndim)
    sampler = emcee.EnsembleSampler(nwalkers, ndim, posterior_function, backend=backend)

    print("running")
    sampler.run_mcmc(pos, nsteps, progress=True, store=True)
    print("wtf")


# # Read in data files
# DATAFILE = "mock_simple_nstars_15000.txt"
# data = np.loadtxt(DATAFILE)
# NSTARS = len(data)
# print(NSTARS)
# VERBOSE = True
# AUTOCORR = False  # autocorrelation analysis?
# NSTEPS = 30000  # number steps in mcmc
# NPARAM = 7
# NWALKERS = 14
# MP = True
# CLUSTER = True
# RESTART = False
# BACKEND = True
# BACKENDFILE = "Backend_simple_nstars_" + str(NSTARS) + ".h5"


# initial_guess = np.array([2.0, 5.0, 7000.0, 1.1, 0.7, 1.5 * (10**7.0), 16.0])
# param_opt = np.copy(initial_guess)
# # =============================================================================
# # opt = minimize(_minus_likelihood,initial_guess,method='Nelder-Mead')
# # param_opt = opt.x
# # print("Parameters that minimize -logL: "+ str(param_opt))
# # =============================================================================


# # %%
# if __name__ == "__main__":
#     print("Starting emcee run...")
#     main(NSTEPS, NWALKERS, param_opt, MP, BACKENDFILE)
#     print("Done!")
