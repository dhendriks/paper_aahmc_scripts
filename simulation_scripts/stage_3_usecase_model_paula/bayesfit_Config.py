"""
File containing configurations for bayesfit models
"""

import agama
import numpy as np
import pyro
import pyro.distributions as dist
import torch

torch.set_default_tensor_type(torch.DoubleTensor)

##################################
# Older full-configuration


#####
# potential function
def potential_function_old_full(configuration):
    """
    Function to set up the potential. Based on older full-model of BayesFit
    """

    ###################
    # Configure the potentials

    # Halo potential
    pot_halo = agama.Potential(
        type=configuration["halo_type"],
        densityNorm=configuration["halo_densityNorm"],
        gamma=configuration["halo_gamma"],
        beta=configuration["halo_beta"],
        alpha=configuration["halo_alpha"],
        scaleRadius=configuration["halo_scaleRadius"],
        outercutoffradius=configuration["halo_outercutoffradius"],
        axisRatioZ=configuration["halo_axisRatioZ"],
    )

    # Disk potential
    pot_disk = agama.Potential(
        type=configuration["disk_type"],
        mass=configuration["disk_mass"],
        scaleRadius=configuration["disk_scaleRadius"],
        scaleHeight=configuration["disk_scaleHeight"],
        n=configuration["disk_n"],
    )

    # Bulge potential
    pot_bulge = agama.Potential(
        type=configuration["bulge_type"],
        mass=configuration["bulge_mass"],
        scaleRadius=configuration["bulge_scaleRadius"],
        sersicIndex=configuration["bulge_sersicIndex"],
        axisRatioZ=configuration["bulge_axisRatioZ"],
    )

    ####
    # Total potential
    pot = agama.Potential(pot_bulge, pot_disk, pot_halo)

    return pot


#####
# df function
def df_function_old_full(configuration):
    """
    Function to set up the df. Based on older full-model of BayesFit
    """

    ####
    # Distribution function for the potential
    df = agama.DistributionFunction(
        type=configuration["df_type"],
        norm=configuration["df_norm"],
        slopeIn=configuration["df_slopeIn"],
        slopeOut=configuration["df_slopeOut"],
        J0=configuration["df_J0"],
        coefJrIn=configuration["df_coefJrIn"],
        coefJzIn=configuration["df_coefJzIn"],
        coefJrOut=configuration["df_coefJrOut"],
        coefJzOut=configuration["df_coefJzOut"],
        rotFrac=configuration["df_rotFrac"],
        Jcutoff=configuration["df_Jcutoff"],
    )

    return df


#####
# logL function
def logL_function_old_full(df, pot, data):
    """
    Function to calculate the log likelihood
    """

    verbosity = False

    # Calculate the normalisation of the DF
    norm = df.totalMass()  # Normalisation of the DF
    # print("norm: ", norm)

    # Calculate actions
    try:
        af = agama.ActionFinder(pot)
    except RuntimeError:
        if verbosity:
            print("no actions")
        return -np.inf

    #
    act_f = af(data, angles=False)

    # Calculate likelihood
    logL = np.sum(
        np.log(df(act_f) / norm)
    )  # total likelihood (of all the stars in the catalog)
    # print("df(act_f): ", df(act_f))
    # print("logL: ", logL)

    # handle nan
    if np.isnan(logL):
        if verbosity:
            print("nan logL")
        logL = -np.inf

    return logL


#############
# mock configuration
bayesfit_mock_configuration_old_full = {
    # Halo configuration
    "halo_type": "Spheroid",
    "halo_densityNorm": 1.1 * (10**7.0),
    "halo_gamma": 1.0,
    "halo_beta": 3.0,
    "halo_alpha": 1.0,
    "halo_scaleRadius": 17.0,
    "halo_outercutoffradius": 1000000000.0,
    "halo_axisRatioZ": 0.8,
    # Disk configuration
    "disk_type": "Disk",
    "disk_mass": 5.6 * (10**10),
    "disk_scaleRadius": 2.57,
    "disk_scaleHeight": 0.4,
    "disk_n": 1.2,
    # bulge configuration
    "bulge_type": "Sersic",
    "bulge_mass": 3.1 * (10**10),
    "bulge_scaleRadius": 1.155,
    "bulge_sersicIndex": 2.7,
    "bulge_axisRatioZ": 0.72,
    # df configuration
    "df_type": "DoublePowerLaw",
    "df_norm": 1,
    "df_slopeIn": 2.5,
    "df_slopeOut": 5.5,
    "df_J0": 8000.0,
    "df_coefJrIn": 0.75,
    "df_coefJzIn": 1.7,
    "df_coefJrOut": 0.88,
    "df_coefJzOut": 1.1,
    "df_rotFrac": 0.5,
    "df_Jcutoff": 12000.0,
}

# #############
# # prior configuration
# prior_dictionary_old_full = {
#     'halo_densityNorm': {'pyro_dist': dist.Uniform, 'lower': 0, 'upper': 2 * 11000000.0},
#     'halo_scaleRadius': {'pyro_dist': dist.Uniform, 'lower': 0, 'upper': 2 * 17.0},
# }


##################################
# New simple-configuration
def potential_function_new_simple(configuration):
    """
    pot function for the new simple model.
    """

    # Halo potential
    pot_halo = agama.Potential(
        type=configuration["halo_type"],
        densityNorm=configuration["halo_densityNorm"],
        gamma=configuration["halo_gamma"],
        beta=configuration["halo_beta"],
        alpha=configuration["halo_alpha"],
        scaleRadius=configuration["halo_scaleRadius"],
        outercutoffradius=configuration["halo_outercutoffradius"],
        axisRatioZ=configuration["halo_axisRatioZ"],
    )

    return pot_halo


def df_function_new_simple(configuration):
    """
    df function for the new simple model.
    """

    ####
    # Distribution function for the potential
    df = agama.DistributionFunction(
        type=configuration["df_type"],
        norm=configuration["df_norm"],
        slopeIn=configuration["df_slopeIn"],
        slopeOut=configuration["df_slopeOut"],
        J0=configuration["df_J0"],
        coefJrIn=configuration["df_coefJrIn"],
        coefJzIn=configuration["df_coefJzIn"],
        coefJrOut=configuration["df_coefJrOut"],
        coefJzOut=configuration["df_coefJzOut"],
        rotFrac=configuration["df_rotFrac"],
        Jcutoff=configuration["df_Jcutoff"],
    )

    return df


def logL_function_new_simple(df, pot, data):
    """
    Log-likelihood function for the new simple model.
    """

    norm = df.totalMass()
    # Calculate actions
    print("norm", norm)

    try:
        print("pre af")
        af = agama.ActionFinder(pot)
        print("af")

    except RuntimeError:
        print("no actions")
        return -np.inf
    print("pre-act")
    act_f = af(data, angles=False)
    print("act_f", act_f)

    # Calculate likelihood
    logL = np.sum(
        np.log((df(act_f) / norm))
    )  # total likelihood (of all the stars in the catalog)
    print("logL", logL)

    if np.isnan(logL):
        print("nan logL")
        logL = -np.inf

    return logL


#############
# mock configuration
bayesfit_mock_configuration_new_simple = {
    # Halo configuration
    "halo_type": "Spheroid",
    "halo_densityNorm": 1.1 * (10**7.0),
    "halo_gamma": 1.0,
    "halo_beta": 3.0,
    "halo_alpha": 1.0,
    "halo_scaleRadius": 17.0,
    "halo_outercutoffradius": 1000000.0,
    "halo_axisRatioZ": 1.0,
    # df configuration
    "df_type": "DoublePowerLaw",
    "df_norm": 1,
    "df_slopeIn": 2.5,
    "df_slopeOut": 5.5,
    "df_J0": 8000.0,
    "df_coefJrIn": 1.2,
    "df_coefJzIn": 0.8,
    "df_coefJrOut": 1.2,
    "df_coefJzOut": 0.8,
    "df_rotFrac": 0.0,
    "df_Jcutoff": 20000,
}


# Define prior function
def prior_function_new_simple(configuration):
    """
    Prior function
    """

    # Set default prior val
    prior_val = 1

    # Prior
    if (
        (configuration["df_slopeIn"] < 0)
        | (configuration["df_slopeIn"] > 3.0)
        | (configuration["df_slopeOut"] < 0)
        | (configuration["df_slopeOut"] < 3.0)
    ):
        return -np.inf

    if configuration["df_J0"] <= 0.0:
        return -np.inf

    if configuration["df_coefJrIn"] < 0.0:
        return -np.inf

    if configuration["df_coefJzIn"] < 0.0:
        return -np.inf

    if 3 - configuration["df_coefJzIn"] - configuration["df_coefJrIn"] < 0.0:
        return -np.inf

    if configuration["halo_densityNorm"] < 0.0:
        return -np.inf

    if configuration["halo_scaleRadius"] < 0.0:
        return -np.inf

    return prior_val


# #############
# # prior configuration
# prior_dictionary_old_full = {
#     # Prior
#     if (slopein<0) | (slopein>3.) | (slopeout<0) | (slopeout<3.):
#         if VERBOSE ==True:
#             print ('**from prior -inf')
#         return -np.inf

#     if  (J_0<=0.):
#         if VERBOSE ==True:
#             print ('**from prior -inf')
#         return -np.inf

#     if  (coefJrIn<0.):
#         if VERBOSE ==True:
#             print ('**from prior -inf')
#         return -np.inf

#     if  (coefJzIn<0.):
#         if VERBOSE ==True:
#             print ('**from prior -inf')
#         return -np.inf

#     if (3-coefJzIn-coefJrIn<0.):
#         return -np.inf

#     if (rho0<0.):
#         return -np.inf
#     if (Rs<0.):
#         return -np.inf

# }


##################################
# New Full-configuration
def potential_function_new_full(configuration):
    """
    pot function for the new full model.
    """

    # Dark Matter potential
    pot_dm = agama.Potential(
        type=configuration["halo_type"],
        densityNorm=configuration["halo_densityNorm"],
        gamma=configuration["halo_gamma"],
        beta=configuration["halo_beta"],
        alpha=configuration["halo_alpha"],
        scaleRadius=configuration["halo_scaleRadius"],
        outercutoffradius=configuration["halo_outercutoffradius"],
        axisRatioZ=configuration["halo_axisRatioZ"],
    )

    # Disk potential
    pot_disk = agama.Potential(
        type=configuration["disk_type"],
        mass=configuration["disk_mass"],
        scaleRadius=configuration["disk_scaleRadius"],
        sersicIndex=configuration["disk_sersicIndex"],
    )

    # Bulge potential
    pot_bulge = agama.Potential(
        type=configuration["bulge_type"],
        mass=configuration["bulge_mass"],
        scaleRadius=configuration["bulge_scaleRadius"],
        sersicIndex=configuration["bulge_sersicIndex"],
    )

    # Total potential
    pot = agama.Potential(pot_bulge, pot_disk, pot_dm)  # total potential of the galaxy

    return pot


def df_function_new_full(configuration):
    """
    df function for the new full model.
    """

    ####
    # Distribution function for the potential
    df = agama.DistributionFunction(
        type=configuration["df_type"],
        norm=configuration["df_norm"],
        slopeIn=configuration["df_slopeIn"],
        slopeOut=configuration["df_slopeOut"],
        J0=configuration["df_J0"],
        coefJrIn=configuration["df_coefJrIn"],
        coefJzIn=configuration["df_coefJzIn"],
        coefJrOut=configuration["df_coefJrOut"],
        coefJzOut=configuration["df_coefJzOut"],
        rotFrac=configuration["df_rotFrac"],
        Jcutoff=configuration["df_Jcutoff"],
    )

    return df


def logL_function_new_full(df, pot, data):
    """
    Log-likelihood function for the new full model.
    """

    norm = df.totalMass()
    # Calculate actions

    try:
        af = agama.ActionFinder(pot)
    except RuntimeError:
        print("no actions")
        return -np.inf
    act_f = af(data, angles=False)

    # Calculate likelihood
    logL = np.sum(
        np.log((df(act_f) / norm))
    )  # total likelihood (of all the stars in the catalog)

    if np.isnan(logL):
        print("nan logL")
        logL = -np.inf

    return logL


#############
# mock configuration
bayesfit_mock_configuration_new_full = {
    # Halo configuration
    "halo_type": "Spheroid",
    "halo_densityNorm": 1.1 * (10**7.0),
    "halo_gamma": 1.0,
    "halo_beta": 3.0,
    "halo_alpha": 1.0,
    "halo_scaleRadius": 17.0,
    "halo_outercutoffradius": 1000000.0,
    "halo_axisRatioZ": 1.0,
    # Disk configuration
    "disk_type": "Sersic",
    "disk_mass": 6.648 * (10**10.0),
    "disk_scaleRadius": 2.57,
    "disk_sersicIndex": 1.2,
    # bulge configuration
    "bulge_type": "Sersic",
    "bulge_mass": 3.1 * (10**10),
    "bulge_scaleRadius": 1.155,
    "bulge_sersicIndex": 2.7,
    # df configuration
    "df_type": "DoublePowerLaw",
    "df_norm": 1,
    "df_slopeIn": 2.5,
    "df_slopeOut": 5.5,
    "df_J0": 8000.0,
    "df_coefJrIn": 0.75,
    "df_coefJzIn": 1.7,
    "df_coefJrOut": 0.88,
    "df_coefJzOut": 1.1,
    "df_rotFrac": 0.5,
    "df_Jcutoff": 20000,
}

# #############
# # prior configuration
# prior_dictionary_new_full = {
#     # Prior
# # Prior
# if (slopein<0) | (slopein>3.) | (slopeout<0) | (slopeout<3.):
#     if VERBOSE ==True:
#         print ('**from prior -inf')
#     return -np.inf

# if  (J_0<=0.):
#     if VERBOSE ==True:
#         print ('**from prior -inf')
#     return -np.inf

# if  (coefJrIn<0.):
#     if VERBOSE ==True:
#         print ('**from prior -inf')
#     return -np.inf

# if  (coefJzIn<0.):
#     if VERBOSE ==True:
#         print ('**from prior -inf')
#     return -np.inf

# if (3-coefJzIn-coefJrIn<0.):
#     return -np.inf

# if  (coefJrOut<0.):
#     if VERBOSE ==True:
#         print ('**from prior -inf')
#     return -np.inf

# if  (coefJzOut<0.):
#     if VERBOSE ==True:
#         print ('**from prior -inf')
#     return -np.inf
# if (3-coefJzOut-coefJrOut<0.):
#     return -np.inf

# if (rotFrac<-1) | (rotFrac>1):
#     return -np.inf

# if (rho0<0.):
#     return -np.inf
# if (Rs<0.):
#     return -np.inf
# if (q<=0.) | (q>1.):
#     return -np.inf
# if (mdisk<=0.):
#     return -np.inf
# if (mbulge<=0.):
#     return -np.inf

# }

# parameters to vary
# params_to_vary = ['rho0_true', 'Rs_true', 'q_true', 'rdisk_true', 'mdisk_true', 'rbulge_true', 'mbulge_true', 'nb_true', 'r_cut']
#     slopein,slopeout,J_0, coefJrIn, coefJzIn,coefJrOut,coefJzOut, rotFrac, rho0, Rs, q, mbulge,mdisk = param
