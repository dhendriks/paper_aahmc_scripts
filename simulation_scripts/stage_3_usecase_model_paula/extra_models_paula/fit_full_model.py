#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Imports
import numpy as np
import matplotlib.pyplot as plt
import emcee
import agama
from multiprocessing import Pool


# Set units for AGAMA
agama.setUnits(mass=1,length=1,velocity=1)

# Global parameters


# Read in data files
DATAFILE = 'mock_full_nstars_15000.txt'
data = np.loadtxt(DATAFILE)

NSTARS = len(data)
print(NSTARS)

VERBOSE     = True 
AUTOCORR    = False # autocorrelation analysis?
NSTEPS      = 30000 # number steps in mcmc
NPARAM      = 13
NWALKERS    = 26
MP          = True
CLUSTER     = True
RESTART     = False
BACKEND     = True
BACKENDFILE = "Backend_full_nstars_"+str(NSTARS)+".h5"

#%% Define prior, likelihood, and posterior functions

def _posterior(param):
    """
    

    Parameters
    ----------
    param : TYPE [numpy.array]
        DESCRIPTION: Contains the parameters of the model. slopein, slopeout, J_0, coefJrIn (hr) -  characterize
        the distribution function (df) of the stellar halo. 
        NFW dark matter halo potential parameters: rho_0 (scale density of the NFW profile), Rs (scale radius), q (flattening, z-axis ratio).
        of the dark matter halo.

    Returns: logL
    -------
    TYPE [scalar]
        DESCRIPTION: Returns the value of the posterior for the given model.

    """

    #slopein,slopeout,J_0 = param
    slopein,slopeout,J_0, coefJrIn, coefJzIn,coefJrOut,coefJzOut, rotFrac, rho0, Rs, q, mbulge,mdisk = param
    
    # Prior
    if (slopein<0) | (slopein>3.) | (slopeout<0) | (slopeout<3.):
        if VERBOSE ==True:
            print ('**from prior -inf')
        return -np.inf

    if  (J_0<=0.):
        if VERBOSE ==True:
            print ('**from prior -inf')
        return -np.inf
    
    if  (coefJrIn<0.):
        if VERBOSE ==True:
            print ('**from prior -inf')
        return -np.inf
    
    if  (coefJzIn<0.):
        if VERBOSE ==True:
            print ('**from prior -inf')
        return -np.inf
    
    if (3-coefJzIn-coefJrIn<0.):
        return -np.inf
    
    if  (coefJrOut<0.):
        if VERBOSE ==True:
            print ('**from prior -inf')
        return -np.inf
    
    if  (coefJzOut<0.):
        if VERBOSE ==True:
            print ('**from prior -inf')
        return -np.inf
    if (3-coefJzOut-coefJrOut<0.):
        return -np.inf
    
    if (rotFrac<-1) | (rotFrac>1):
        return -np.inf
    
    if (rho0<0.):
        return -np.inf
    if (Rs<0.):
        return -np.inf
    if (q<=0.) | (q>1.):
        return -np.inf
    if (mdisk<=0.):
        return -np.inf
    if (mbulge<=0.):
        return -np.inf
# =============================================================================
#     if (beta<=2.):
#         return -np.inf
#     if(gamma>=3):
#         return -np.inf
# =============================================================================

    

    # Potential
    rdisk_true = 4.318  # disk scale radius
    rbulge_true = 0.877  # bulge effective radius
    nb_true = 1.026  # bulge Sersic index
    r_cut = 1000000.
    
    # Dark Matter potential
    dmHaloNFW_param_true = dict(type='Spheroid',
                    densityNorm=rho0,
                    gamma=1.,
                    beta=3.,
                    alpha=1.,
                    scaleRadius = Rs,
                    outercutoffradius=r_cut,
                    axisRatioZ = q)
    
    pot_dm = agama.Potential(dmHaloNFW_param_true)
    
    # Disk potential
    pot_disk = agama.Potential(type='Sersic',
                               mass=mdisk,
                               scaleRadius=rdisk_true,
                               sersicIndex=1.)
    
    
    # Bulge potential
    pot_bulge = agama.Potential(type='Sersic',
                            mass=mbulge,
                            scaleRadius=rbulge_true,
                            sersicIndex=nb_true)
    
    # Total potential
    pot      = agama.Potential(pot_bulge,pot_disk,pot_dm) # total potential of the galaxy
    
    
    
    # Distribution function for the stellar halo
    
    df = agama.DistributionFunction(type='DoublePowerLaw',
                                    norm=1,
                                    slopeIn=slopein,
                                    slopeOut=slopeout,
                                    J0=J_0,
                                    coefJrIn=coefJrIn,
                                    coefJzIn=coefJzIn,
                                    coefJrOut=coefJrOut,
                                    coefJzOut=coefJzOut,
                                    rotFrac=rotFrac,
                                    Jcutoff=20000)
    
    norm = df.totalMass()
    # Calculate actions
    
    try:
        af = agama.ActionFinder(pot)
    except (RuntimeError):
        print('no actions')
        return -np.inf
    act_f = af(data,angles=False)
    
    # Calculate likelihood
    logL = np.sum(np.log((df(act_f)/norm))) # total likelihood (of all the stars in the catalog)
    
    if np.isnan(logL):
        print('nan logL')
        logL = -np.inf
    return (logL)  
#%% Apply gradient optimization

initial_guess = np.array([1.8,5.,7000., 0.8,1.,0.7,0.85,0.4,1.5*10**7.,16.,0.7,0.8*(10**10),6.2*(10**10.)])
param_opt = np.copy(initial_guess)
# =============================================================================
# opt = minimize(_minus_likelihood,initial_guess,method='Nelder-Mead')
# param_opt = opt.x
# print("Parameters that minimize -logL: "+ str(param_opt))
# =============================================================================
#%% emcee 
def main(nsteps,nwalkers,param_opt,mp,backendfile):
    
    # Set random number seed
    
    # Number of dimensions
    ndim = len(param_opt)
    
    #pos = np.random.randn(nwalkers,ndim) # starting point(s) of the walkers
    np.random.seed(1234)

    #pos = param_opt + 1e-4 * np.random.randn(nwalkers, ndim) # starting point(s) of the walkers
    print("...using "+str(nwalkers)+" walkers...")
    
    # Define backend 
    with Pool() as pool:
        print('...in parallel mode...')
        
        if (RESTART):
            backend = emcee.backends.HDFBackend(backendfile) 
            sampler = emcee.EnsembleSampler(nwalkers,ndim, _posterior,pool=pool,backend=backend)
            pos = sampler.get_last_sample()
                     
        else:
            pos = param_opt + 1e-2 * np.random.randn(nwalkers, ndim) # starting point(s) of the walkers
            backend = emcee.backends.HDFBackend(backendfile)
            backend.reset(nwalkers,ndim)
            sampler = emcee.EnsembleSampler(nwalkers,ndim, _posterior,pool=pool,backend=backend)
        
        sampler.run_mcmc(pos, nsteps,progress=True,store=True)
#%%
if __name__ == "__main__":
    print('Starting emcee run...')
    main(NSTEPS,NWALKERS,param_opt,MP,BACKENDFILE)
    print('Done!')
