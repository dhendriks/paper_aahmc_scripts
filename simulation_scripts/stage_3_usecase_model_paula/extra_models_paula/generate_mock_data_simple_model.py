#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Imports
import numpy as np
import matplotlib.pyplot as plt
import agama
from scipy.special import gamma as gamma_fct

# Set units for AGAMA
agama.setUnits(mass=1,length=1,velocity=1)

N = 15000 # nr of stars to sample



#%% Define mock galaxy

# Potential
rho0_true = 1.1*(10**7.)
Rs_true = 17.
r_cut = 1000000.


# things to vary
# ['densityNorm', 'Rs_true', 'r_cut']





# Dark Matter potential
dmHaloNFW_param_true = dict(type='Spheroid',
                densityNorm=1.1*(10**7.),
                gamma=1.,
                beta=3.,
                alpha=1.,
                scaleRadius = 17.,
                outercutoffradius=1000000.,
                axisRatioZ = 1.)

pot_dm= agama.Potential(dmHaloNFW_param_true)

df = agama.DistributionFunction(type='DoublePowerLaw',
                                norm=1,
                                slopeIn=2.5,
                                slopeOut=5.5,
                                J0=8000.,
                                coefJrIn=1.2,
                                coefJzIn=0.8,
                                coefJrOut=1.2,
                                coefJzOut=0.8,
                                rotFrac=0.,
                                Jcutoff=20000)


# Distribution function for a spheroid-like component



gm  = agama.GalaxyModel(pot_dm,df)

#%%

data,_ = gm.sample(N)

np.savetxt('mock_simple_nstars_'+str(N)+'.txt',data)





