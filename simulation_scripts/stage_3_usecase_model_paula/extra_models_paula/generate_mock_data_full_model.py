#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Imports
import numpy as np
import matplotlib.pyplot as plt
import agama
from scipy.special import gamma as gamma_fct

# Set units for AGAMA
agama.setUnits(mass=1, length=1, velocity=1)

N = 15000  # nr of stars to sample



# Dark Matter potential
dmHaloNFW_param_true = dict(type='Spheroid',
                            densityNorm=1.1*(10**7.),
                            gamma=1.,
                            beta=3.,
                            alpha=1.,
                            scaleRadius=17.,
                            outercutoffradius=1000000.,
                            axisRatioZ=0.8)

pot_dm_true = agama.Potential(dmHaloNFW_param_true)

# Disk potential
pot_disk_true = agama.Potential(type='Sersic',
                                mass=6.648*(10**10.),
                                scaleRadius=4.318,
                                sersicIndex=1.)


# Bulge potential
pot_bulge_true = agama.Potential(type='Sersic',
                                 mass=0.930*(10**10.),
                                 scaleRadius=0.877,
                                 sersicIndex=1.026)

# Total potential
# total potential of the galaxy
pot_true = agama.Potential(pot_bulge_true, pot_disk_true, pot_dm_true)


# Distribution function for the stellar halo

df = agama.DistributionFunction(type='DoublePowerLaw',
                                norm=1,
                                slopeIn=2.5,
                                slopeOut=5.5,
                                J0=8000.,
                                coefJrIn=0.75,
                                coefJzIn=1.7,
                                coefJrOut=0.88,
                                coefJzOut=1.1,
                                rotFrac=0.5,
                                Jcutoff=20000)

gm = agama.GalaxyModel(pot_true, df)

# %%

data, _ = gm.sample(N)

np.savetxt('mock_full_nstars_'+str(N)+'.txt', data)
