#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from multiprocessing import Pool

import agama
import emcee
import matplotlib.pyplot as plt

# Imports
import numpy as np

# Set units for AGAMA
agama.setUnits(mass=1, length=1, velocity=1)


# Read in data files
DATAFILE = "mock_simple_nstars_15000.txt"
data = np.loadtxt(DATAFILE)

NSTARS = len(data)
print(NSTARS)

VERBOSE = True
AUTOCORR = False  # autocorrelation analysis?
NSTEPS = 30000  # number steps in mcmc
NPARAM = 7
NWALKERS = 14
MP = True
CLUSTER = True
RESTART = False
BACKEND = True
BACKENDFILE = "Backend_simple_nstars_" + str(NSTARS) + ".h5"

# %% Define prior, likelihood, and posterior functions


def _posterior(param):
    """

    Parameters
    ----------
    param : TYPE [numpy.array]
        DESCRIPTION: Contains the parameters of the model. slopein, slopeout, J_0, coefJrIn (hr) -  characterize
        the distribution function (df) of the stellar halo.
        NFW dark matter halo potential parameters: rho_0 (scale density of the NFW profile), Rs (scale radius), q (flattening, z-axis ratio).
        of the dark matter halo.

    Returns: logL
    -------
    TYPE [scalar]
        DESCRIPTION: Returns the value of the posterior for the given model.

    """
    # slopein,slopeout,J_0 = param
    slopein, slopeout, J_0, coefJrIn, coefJzIn, rho0, Rs = param

    # Prior
    if (slopein < 0) | (slopein > 3.0) | (slopeout < 0) | (slopeout < 3.0):
        if VERBOSE == True:
            print("**from prior -inf")
        return -np.inf

    if J_0 <= 0.0:
        if VERBOSE == True:
            print("**from prior -inf")
        return -np.inf

    if coefJrIn < 0.0:
        if VERBOSE == True:
            print("**from prior -inf")
        return -np.inf

    if coefJzIn < 0.0:
        if VERBOSE == True:
            print("**from prior -inf")
        return -np.inf

    if 3 - coefJzIn - coefJrIn < 0.0:
        return -np.inf

    if rho0 < 0.0:
        return -np.inf
    if Rs < 0.0:
        return -np.inf

    # Potential
    r_cut = 1000000.0

    # Dark Matter potential
    dmHaloNFW_param_true = dict(
        type="Spheroid",
        densityNorm=rho0,
        gamma=1.0,
        beta=3.0,
        alpha=1.0,
        scaleRadius=Rs,
        outercutoffradius=r_cut,
        axisRatioZ=1.0,
    )

    pot_dm = agama.Potential(dmHaloNFW_param_true)

    # Distribution function for the stellar halo

    df = agama.DistributionFunction(
        type="DoublePowerLaw",
        norm=1,
        slopeIn=slopein,
        slopeOut=slopeout,
        J0=J_0,
        coefJrIn=coefJrIn,
        coefJzIn=coefJzIn,
        coefJrOut=coefJrIn,
        coefJzOut=coefJzIn,
        rotFrac=0.0,
        Jcutoff=20000,
    )

    norm = df.totalMass()
    # Calculate actions

    try:
        af = agama.ActionFinder(pot_dm)
    except RuntimeError:
        print("no actions")
        return -np.inf
    act_f = af(data, angles=False)

    # Calculate likelihood
    logL = np.sum(
        np.log((df(act_f) / norm))
    )  # total likelihood (of all the stars in the catalog)

    if np.isnan(logL):
        print("nan logL")
        logL = -np.inf
    return logL


# %% Apply gradient optimization

initial_guess = np.array([2.0, 5.0, 7000.0, 1.1, 0.7, 1.5 * (10**7.0), 16.0])
param_opt = np.copy(initial_guess)


# =============================================================================
# opt = minimize(_minus_likelihood,initial_guess,method='Nelder-Mead')
# param_opt = opt.x
# print("Parameters that minimize -logL: "+ str(param_opt))
# =============================================================================
# %% emcee
def main(nsteps, nwalkers, param_opt, mp, backendfile):

    # Set random number seed

    # Number of dimensions
    ndim = len(param_opt)

    # pos = np.random.randn(nwalkers,ndim) # starting point(s) of the walkers
    np.random.seed(1234)

    # pos = param_opt + 1e-4 * np.random.randn(nwalkers, ndim) # starting point(s) of the walkers

    print("...using " + str(nwalkers) + " walkers...")

    # Define backend

    with Pool() as pool:
        print("...in parallel mode...")

        if RESTART:
            backend = emcee.backends.HDFBackend(backendfile)
            sampler = emcee.EnsembleSampler(
                nwalkers, ndim, _posterior, pool=pool, backend=backend
            )
            pos = sampler.get_last_sample()

        else:
            pos = param_opt + 1e-2 * np.random.randn(
                nwalkers, ndim
            )  # starting point(s) of the walkers
            backend = emcee.backends.HDFBackend(backendfile)
            backend.reset(nwalkers, ndim)
            sampler = emcee.EnsembleSampler(
                nwalkers, ndim, _posterior, pool=pool, backend=backend
            )

        sampler.run_mcmc(pos, nsteps, progress=True, store=True)


# %%
if __name__ == "__main__":
    print("Starting emcee run...")
    main(NSTEPS, NWALKERS, param_opt, MP, BACKENDFILE)
    print("Done!")
