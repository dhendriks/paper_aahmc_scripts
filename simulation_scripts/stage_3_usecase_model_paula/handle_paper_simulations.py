"""
Functions to handle the simulations for stage 3: use-case of paula's model
TODO: set up 2d, 5d, 10d model configurations for each of the samplers.
"""

import copy
import os
import pickle
from functools import partial

import numpy as np
import pyro
import pyro.distributions as dist
import torch
from hmc_project_code.functions.analysis.model_analysis.inspection_functions.inspect_gradient_with_two_models import (
    calculate_potential_grad,
    return_potential_fn,
)
from hmc_project_code.functions.map_quality_heuristic.handle_quality_check_with_AAHMC import (
    handle_quality_checks_with_AAHMC,
)
from hmc_project_code.functions.sampling.generate_samples import generate_samples

from paper_aahmc_scripts.project_settings import project_config

#
from paper_aahmc_scripts.simulation_scripts.stage_3_usecase_model_paula import (
    bayesfit_Config,
)
from paper_aahmc_scripts.simulation_scripts.stage_3_usecase_model_paula.bayesfit_emcee_function import (
    bayesfit_emcee_routine,
)
from paper_aahmc_scripts.simulation_scripts.stage_3_usecase_model_paula.bayesfit_likelihood_function import (
    bayesfit_general_likelihood_function,
)
from paper_aahmc_scripts.simulation_scripts.stage_3_usecase_model_paula.bayesfit_mock_data_functions import (
    bayesfit_generate_mockdata,
)
from paper_aahmc_scripts.simulation_scripts.stage_3_usecase_model_paula.bayesfit_PYRO_model import (
    return_bayesFit_PYRO_model,
)


def check_parameters(parameters_to_vary, mock_config, pyro_param_dict):
    """
    Function to check whether the parameters that are suggested to be varied actually are present in the mockdata and prior dictionary
    """

    ########
    # check mock config
    for parameter in parameters_to_vary:
        if parameter not in mock_config:
            raise ValueError("Parameter is not defined in the mock config")

    ########
    # check prior dictionary
    for parameter in parameters_to_vary:
        if parameter not in pyro_param_dict:
            raise ValueError("Parameter is not defined in the prior dictionary")


def generate_and_load_mockdata(
    output_dir, potential_function, df_function, mock_config
):
    """
    Function to generate and load the mock data
    """

    #########
    # Make mockdata generator
    mockdata_generator = partial(
        bayesfit_generate_mockdata,
        potential_function=potential_function,
        df_function=df_function,
        mock_configuration=mock_config,
    )

    #####
    # Generate mockdata
    mockdata_filename = os.path.join(output_dir, "mockdata_and_config.pickle")
    mockdata_generator(output_file=mockdata_filename)

    #####
    # Load mockdata
    mockdata_dict = pickle.load(open(mockdata_filename, "rb"))
    mock_data = mockdata_dict["mock_data"]

    return mock_data


def generate_initial_position_array(mock_config, parameters_to_vary):
    """
    Function to generate the initial position.

    Takes the values from the mock configuration and adds some noise to it
    """

    # Set truth
    truth_array = np.array([mock_config[parameter] for parameter in parameters_to_vary])

    # generate random offset
    random_offset = (np.random.random(len(truth_array)) - 1) * truth_array

    # generate initial position array
    initial_position_array = truth_array + random_offset

    return initial_position_array


def handle_simulation(
    output_dir,
    mock_config,
    extra_config,
    parameters_to_vary,
    pyro_param_dict,
    # functions
    potential_function,
    df_function,
    logL_function,
    prior_function,
    # switches
    use_emcee,
    use_NUTS,
    use_NUTS_through_map,
    use_AAHMC,
    actually_run_samplers=True,
):
    """
    Main function that handles running the functions for Paula Gherghinescu's models
    """

    ######
    # check if varied parameters are actually in the configuration
    check_parameters(
        parameters_to_vary=parameters_to_vary,
        mock_config=mock_config,
        pyro_param_dict=pyro_param_dict,
    )

    #######
    # config
    local_config = copy.deepcopy(project_config)
    local_config = {**local_config, **extra_config}

    local_config["sample_size"] = 2000
    local_config["n_samples"] = 25
    local_config["guide_training_termination_hook"] = handle_quality_checks_with_AAHMC
    local_config["num_steps_map"] = (
        25001  # NOTE: this has to be larger than whatever largest number we check at in the array below
    )

    # Config for the map quality check
    local_config["handle_quality_check_with_AAHMC_training_step_check_array"] = [
        1,
        3,
        7,
        10,
        30,
        70,
        100,
        300,
        700,
        1000,
        3000,
        5000,
        10000,
        15000,
        20000,
        25000,
        50000,
    ]
    local_config["handle_quality_check_with_AAHMC_frequency"] = 100
    local_config["handle_quality_check_with_AAHMC_n_quality_checks"] = 100
    local_config["handle_quality_check_with_AAHMC_mean_target_quality_threshold"] = 0.6
    local_config["handle_quality_check_with_AAHMC_mean_base_quality_threshold"] = 0.99

    #
    local_config["emcee_config"] = {
        "n_walkers": 2,
        "backendfile": os.path.join(output_dir, "emcee_backend_file.h5"),
        "n_steps": 10,
        "restart": False,
    }

    ###########
    # Configuration for generate samples function
    generate_samples_args = {
        "dimensions": len(parameters_to_vary),
        "config": local_config,
        "generate_real_samples": False,
        "run_nuts_on_target": use_NUTS,
        "run_aahmc_on_target": False,
        "generate_map_regardless": False,
        "run_map_samples": False,
        "run_nuts_on_base_and_push": use_NUTS_through_map,
        "run_aahmc_on_base_and_push": use_AAHMC,
        "verbose": 1,
    }

    #######
    # Generate mockdata
    mock_data = generate_and_load_mockdata(
        output_dir=output_dir,
        potential_function=potential_function,
        df_function=df_function,
        mock_config=mock_config,
    )

    #########
    # Make likelihood function
    likelihood_function = partial(
        bayesfit_general_likelihood_function,
        parameters_to_vary=parameters_to_vary,
        potential_function=potential_function,
        df_function=df_function,
        logL_function=logL_function,
        prior_function=prior_function,
        mock_configuration=mock_config,
        mock_data=mock_data,
    )

    ###############
    # Set initial position array
    initial_position_array = generate_initial_position_array(
        mock_config=mock_config, parameters_to_vary=parameters_to_vary
    )

    #####
    # handle model with EMCEE
    if use_emcee:
        ##################
        # Set up emcee
        emcee_routine = partial(
            bayesfit_emcee_routine,
            posterior_function=likelihood_function,
            config=local_config,
        )

        ##################
        # run EMCEE
        if actually_run_samplers:
            emcee_routine(initial_position_array)

    ##########
    # handle model with NUTS and/org AAHMC
    if use_NUTS or use_NUTS_through_map or use_AAHMC:

        #################
        # Set up PYRO model
        bayesFit_PYRO_model = return_bayesFit_PYRO_model(
            likelihood_function=likelihood_function,
            numdiff_config=local_config["numdiff_config"],
            parameters_to_vary=parameters_to_vary,
            pyro_param_dict=pyro_param_dict,
        )

        # ##################
        # # Test steps
        # initial_position_tensor_array = torch.Tensor(initial_position_array)

        # # Set up param dict
        # param_dict = {
        #     param_name: initial_position_tensor_array[param_name_i]
        #     for param_name_i, param_name in enumerate(parameters_to_vary)
        # }

        # # Set up potential function
        # potential_fn, _ = return_potential_fn(bayesFit_PYRO_model)

        # # Calculate potential and gradient at that location
        # potential_grad, potential_val = calculate_potential_grad(potential_fn, param_dict)

        # print("param_dict": param_dict)
        # print("potential_grad: ", potential_grad)
        # print("potential_val: ", potential_val)

        ################
        # Run samplers
        if actually_run_samplers:
            generate_samples(
                **generate_samples_args,
                model=bayesFit_PYRO_model,
                output_filename=os.path.join(
                    result_root, "BayesFit_PYRO_samplers.json"
                ),
            )


def handle_BayesFit_simple_model_2d(
    output_root,
    use_emcee=True,
    use_NUTS=True,
    use_NUTS_through_map=True,
    use_AAHMC=True,
    actually_run_samplers=True,
):
    """
    Example function on how to set up a bayesFit sampling model comparison
    """

    ###########
    # Configure extra config
    extra_config = {}

    ###########
    # Define parameters to vary
    parameters_to_vary = ["halo_densityNorm", "halo_scaleRadius"]

    #####
    # Configure output dir
    output_dir = os.path.join(output_root, "bayesFit_2d")
    os.makedirs(output_dir, exist_ok=True)

    #####
    # Select the functions we want to use
    df_function = bayesfit_Config.df_function_new_simple
    potential_function = bayesfit_Config.potential_function_new_simple
    logL_function = bayesfit_Config.logL_function_new_simple

    # Select mock config
    mock_config = bayesfit_Config.bayesfit_mock_configuration_new_simple

    #################
    # Set up PYRO param dict
    pyro_param_dict = {
        "halo_densityNorm": dist.Normal(0, 1),
        "halo_scaleRadius": dist.Normal(0, 1),
    }

    #####
    # Define some some local functions
    prior_function = bayesfit_Config.prior_function_new_simple

    ########
    # call simulation handler
    handle_simulation(
        output_dir=output_dir,
        mock_config=mock_config,
        extra_config=extra_config,
        parameters_to_vary=parameters_to_vary,
        pyro_param_dict=pyro_param_dict,
        # functions
        potential_function=potential_function,
        df_function=df_function,
        logL_function=logL_function,
        prior_function=prior_function,
        # switches
        use_emcee=use_emcee,
        use_NUTS=use_NUTS,
        use_NUTS_through_map=use_NUTS_through_map,
        use_AAHMC=use_AAHMC,
        actually_run_samplers=actually_run_samplers,
    )


if __name__ == "__main__":

    #
    result_root = os.path.join(
        os.getenv("AAHMC_DATA_ROOT"), "stage_3_results", "BayesFit"
    )

    ##########
    # Some testing calls
    # handle_BayesFit_simple_model_2d(result_root, actually_run_samplers=False)
    handle_BayesFit_simple_model_2d(
        result_root, actually_run_samplers=True, use_emcee=True, use_NUTS=True
    )

    ##########
    # Some testing calls
