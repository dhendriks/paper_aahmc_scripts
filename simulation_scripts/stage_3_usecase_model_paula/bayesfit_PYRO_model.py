"""
File containing the bayesift PYRO model
"""

import inspect

import pyro
import torch
from hmc_project_code.functions.finite_diff_functionality.finite_diff_decorator import (
    numerical_diff_wrapper,
)


def return_bayesFit_PYRO_model(
    likelihood_function,
    numdiff_config,
    parameters_to_vary,
    pyro_param_dict,
):
    """
    General function to return the PYRO model
    """

    verbose = True

    #
    if numdiff_config is None:
        numdiff_config = {}

    @numerical_diff_wrapper(config=numdiff_config)
    def numdiff_likelihood_function(arr):
        """
        Closure for MVN to handle decorating and passing of arguments
        """

        if verbose:
            function_name = inspect.stack()[0][3]
            print("Evaluating function {} with args {}".format(function_name, arr))

        # if sleep_time > 0:
        #     time.sleep(sleep_time)

        return likelihood_function(param_arr=arr)

    def model():
        """
        model
        """

        # cast values in new shape
        param_array = torch.zeros(len(parameters_to_vary))

        # Return scaled samples
        with pyro.poutine.scale(scale=1e-100):

            # build up the parameters
            for param_i, param in enumerate(parameters_to_vary):

                print("Adding sampling distribution for {}".format(param))
                param_array[param_i] = pyro.sample(param, pyro_param_dict[param])

        # Add likelihood to function
        model_factor = pyro.factor(
            "custom_likelihood",
            numdiff_likelihood_function(param_array),
        )

    return model
