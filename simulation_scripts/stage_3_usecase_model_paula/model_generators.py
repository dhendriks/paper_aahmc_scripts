"""
Function to return a new BayesFit2D model
"""


import pyro
import pyro.distributions as dist
import torch

torch.set_default_tensor_type(torch.DoubleTensor)


from hmc_project_code.functions.finite_diff_functionality.finite_diff_decorator import (
    argument_structure_wrapper,
    handle_numerical_diff_wrapping,
)
from hmc_project_code.projects.usecase_projects.project_paula_gherghinescu.bayesfit_2d.likelihood_function import (
    bind_bayesfit2d_likelihood_function,
)



import os

import numpy as np
import torch

torch.set_default_tensor_type(torch.DoubleTensor)
import pickle
from functools import partial

import agama

#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)





















###########################
# 2-d model things
def return_BayesFit2dModel(
    mock_data_filename, make_num_diff=False, numdiff_config=None, verbosity=0
):
    """
    Function to return the BayesFit2D model
    """

    # bind the data and config to the likelihood function
    partial_bayesfit2d_likelihood_function = bind_bayesfit2d_likelihood_function(
        mock_data_filename=mock_data_filename, verbosity=verbosity
    )

    #
    if make_num_diff:
        # change input format from args/kwargs to tensor array
        partial_bayesfit2d_likelihood_function = argument_structure_wrapper(
            partial_bayesfit2d_likelihood_function
        )

        # Handle numerically
        partial_bayesfit2d_likelihood_function = handle_numerical_diff_wrapping(
            make_num_diff=make_num_diff,
            func=partial_bayesfit2d_likelihood_function,
            config=numdiff_config,
        )

    def BayesFit2dModel():
        """
        BayesFit2dModel
        """

        # with pyro.poutine.scale(scale=1e-10):
        halo_densityNorm = pyro.sample(
            "halo_densityNorm", dist.Uniform(0, 2 * 11000000.0)
        )
        halo_scaleRadius = pyro.sample("halo_scaleRadius", dist.Uniform(0, 2 * 17.0))

        if make_num_diff:
            # cast values in new shape
            param_array = torch.zeros(2)
            param_array[0] = halo_densityNorm
            param_array[1] = halo_scaleRadius

            likelihood = pyro.factor(
                "likelihood",
                partial_bayesfit2d_likelihood_function(param_array),
            )
        else:
            likelihood = pyro.factor(
                "likelihood",
                partial_bayesfit2d_likelihood_function(
                    halo_densityNorm=halo_densityNorm, halo_scaleRadius=halo_scaleRadius
                ),
            )

    return BayesFit2dModel

def bayesfit2d_likelihood_function(
    halo_densityNorm, halo_scaleRadius, data, config, verbosity
):
    """
    BayesFit2d likelihood function

    Returns: logL
    -------
    TYPE [scalar]
        DESCRIPTION: Returns the value of the log-posterior for the given model given the parameters and data.
    """

    ###################################
    # Priors
    if halo_densityNorm < 0.0:
        return -np.inf
    if halo_scaleRadius < 0.0:
        return -np.inf

    print("halo_densityNorm ", halo_densityNorm)
    print("halo_scaleRadius ", halo_scaleRadius)

    ###################################
    # Define the model

    ##########
    # DM halo potential
    halo_param = config["halo_param"]
    halo_param["densityNorm"] = halo_densityNorm
    halo_param["scaleRadius"] = halo_scaleRadius

    pot_halo = agama.Potential(**halo_param)

    ##########
    # Disk potential
    disk_param = config["disk_param"]

    pot_disk = agama.Potential(**disk_param)

    ##########
    # Bulge potential
    bulge_param = config["bulge_param"]

    pot_bulge = agama.Potential(**bulge_param)

    ##########
    # Total potential
    pot = agama.Potential(pot_bulge, pot_disk, pot_halo)

    ##########
    # Distribution function for the stellar halo
    df_param = config["df_param"]

    df = agama.DistributionFunction(**df_param)

    ###################
    # Calculate properties

    # Calculate the normalisation of the DF
    norm = df.totalMass()  # Normalisation of the DF
    print("norm: ", norm)

    # Calculate actions
    try:
        af = agama.ActionFinder(pot)
    except RuntimeError:
        print("YOYOYO")
        if verbosity:
            print("no actions")
        return -np.inf
    act_f = af(data, angles=False)

    # Calculate likelihood
    logL = np.sum(
        np.log(df(act_f) / norm)
    )  # total likelihood (of all the stars in the catalog)
    print("df(act_f): ", df(act_f))
    print("logL: ", logL)

    if np.isnan(logL):
        if verbosity:
            print("nan logL")
        logL = -np.inf

    return logL


def bind_bayesfit2d_likelihood_function(mock_data_filename, verbosity):
    """
    Function to partially bind the bayesfit2d likelihood function
    """

    ##########
    # Load data
    mockdata_dict = pickle.load(open(mock_data_filename, "rb"))

    #
    mock_data = mockdata_dict["data"]
    config = mockdata_dict["config"]

    # Fix the mock_data for the likelihood function, as well as the config and the verbosity.
    partial_bayesfit2d_likelihood_function = partial(
        bayesfit2d_likelihood_function,
        data=mock_data,
        config=config,
        verbosity=verbosity,
    )

    return partial_bayesfit2d_likelihood_function
