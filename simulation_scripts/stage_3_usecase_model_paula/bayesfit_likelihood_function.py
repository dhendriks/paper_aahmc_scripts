"""
General likelihood function that should be configured before use.
"""

import agama


def bayesfit_general_likelihood_function(
    param_arr,
    parameters_to_vary,
    # functions
    potential_function,
    df_function,
    logL_function,
    prior_function,
    #
    mock_configuration,
    mock_data,
):
    """
    Very general likelihood function. Will
    """

    # Set units for AGAMA
    agama.setUnits(mass=1, length=1, velocity=1)

    ###############
    # Take care of reading out the values in the correct order of the paramaeters
    param_dict = {
        param_name: param_arr[param_name_i]
        for param_name_i, param_name in enumerate(parameters_to_vary)
    }

    ###############
    # update config
    config = {**mock_configuration, **param_dict}

    ###############
    # Take care of the priors
    prior = 1
    if prior_function is not None:
        prior = prior_function(configuration=config)
    print("prior", prior)

    ###################
    # load the potential
    pot = potential_function(configuration=config)
    print(pot)

    ###################
    # load the df
    df = df_function(configuration=config)
    print(df)

    ###################
    # Likelihood calculation
    logL = logL_function(df=df, pot=pot, data=mock_data)
    print("LogL", logL)

    return logL * prior
