"""
utilities to handle generating the mock data

TODO: use a light-version.

"""

import datetime
import json
import os
import pickle

import agama
import numpy as np

#
this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def data_serialiser(obj):
    """
    Function to serialise the vader paramdict

    Args:
        obj: The object that might not be serialisable

    Returns:
        Either string representation of object if the object is a function, or the object itself
    """

    if isinstance(obj, (np.float32, np.float64)):
        return float(obj)
    elif isinstance(obj, (np.int32, np.int64)):
        return int(obj)
    elif isinstance(obj, np.ndarray):
        return list(obj)
    else:
        try:
            string_version = str(obj)
            return string_version
        except:
            json.JSONEncoder.default(self, obj)
            # raise TypeError(
            #     "Unserializable object {} of type {}. Attempted to convert to string but that failed.".format(obj, type(obj))
            # )


def bayesfit_generate_mockdata(
    output_file, potential_function, df_function, mock_configuration, n_stars=15000
):
    """
    Function to generate the mockdata for the galaxy models that we use in Bayesfit
    """

    # Set units for AGAMA
    agama.setUnits(mass=1, length=1, velocity=1)

    ###################
    # load the potential
    pot = potential_function(configuration=mock_configuration)

    ###################
    # load the df
    df = df_function(configuration=mock_configuration)

    #################
    # Set up actual galaxy model
    gm = agama.GalaxyModel(pot, df)  # the galaxy model object

    # Generate the mock data
    data, _ = gm.sample(n_stars)  # sample data

    # Construct output json
    output_json = {
        "mock_data": data,
        "mock_configuration": mock_configuration,
        "datetime_generated": datetime.datetime.now().isoformat(),
    }

    with open(output_file, "wb") as f:
        pickle.dump(output_json, f, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    output_file = "/tmp/test.pickle"
    generate_mockdata(output_file=output_file)
    # mockdata = pickle.loads(open(output_file, 'rb'))
    with open(output_file, "rb") as picklefile:
        data = pickle.load(picklefile)
    print(data)
