"""
Routine to plot the 5d sample comparison between the samplers
"""


import json
import os

import matplotlib.pyplot as plt
import numpy as np
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot

from hmc_project_code.functions.plotting.utility import align_axes
from hmc_project_code.projects.action_angle_scripts.functions.plot_routines.plot_sample_comparison_paper import (
    plot_sample_comparison_paper,
)

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


###
def readout_sequence_results(filename):
    """
    Function to read out sample-set of samplers
    """

    with open(filename, "r") as f:
        data = json.loads(f.read())

    sequence_results = []

    # loop over all the individual sample set iterations
    for sample_set_i in data["samplers"]["aahmc_through_map"]["sample_set"].keys():
        sample_new_shape = {}

        # Readout real sample
        sample_new_shape["real_sample"] = np.array(
            data["samplers"]["map"]["sample_set"][sample_set_i]["real_sample"]
        )
        sample_new_shape["name_real_sample"] = "True"

        # Readout second real sample
        sample_new_shape["second_real_sample"] = np.array(
            data["samplers"]["map"]["sample_set"][sample_set_i]["second_real_sample"]
        )
        sample_new_shape["name_second_real_sample"] = "True-2"

        # Nuts direct sampled:
        sample_new_shape["sample_1"] = np.array(
            data["samplers"]["nuts_on_target"]["sample_set"][sample_set_i]["samples"]
        )
        sample_new_shape["times_sample_1"] = data["samplers"]["nuts_on_target"][
            "sample_set"
        ][sample_set_i]["runtimes_array"]
        sample_new_shape["name_sample_1"] = "nuts_direct"
        sample_new_shape["display_name_sample_1"] = "NUTS"

        # Nuts through map sampled:
        sample_new_shape["sample_2"] = np.array(
            data["samplers"]["nuts_through_map"]["sample_set"][sample_set_i]["samples"]
        )
        sample_new_shape["times_sample_2"] = data["samplers"]["nuts_through_map"][
            "sample_set"
        ][sample_set_i]["runtimes_array"]
        sample_new_shape["name_sample_2"] = "nuts_through_map"
        sample_new_shape["display_name_sample_2"] = "NUTS + map"

        # Nuts through map sampled:
        sample_new_shape["base_sample_2"] = np.array(
            data["samplers"]["nuts_through_map"]["sample_set"][sample_set_i][
                "samples_base"
            ]
        )
        sample_new_shape["name_base_sample_2"] = "nuts_through_map_base"
        sample_new_shape["display_name_base_sample_2"] = "NUTS through map base"

        # AAHMC through map sampled:
        sample_new_shape["sample_3"] = np.array(
            data["samplers"]["aahmc_through_map"]["sample_set"][sample_set_i]["samples"]
        )
        sample_new_shape["times_sample_3"] = data["samplers"]["aahmc_through_map"][
            "sample_set"
        ][sample_set_i]["runtimes_array"]
        sample_new_shape["name_sample_3"] = "aahmc_through_map"
        sample_new_shape["display_name_sample_3"] = "AAHMC"

        # AAHMC through map sampled:
        sample_new_shape["base_sample_3"] = np.array(
            data["samplers"]["aahmc_through_map"]["sample_set"][sample_set_i][
                "samples_base"
            ]
        )
        sample_new_shape["name_base_sample_3"] = "aahmc_through_map base"
        sample_new_shape["display_name_base_sample_3"] = "AAHMC base"

        #
        sequence_results.append(sample_new_shape)

    return sequence_results


def plot_sample_panels(sequence_result_set, plot_settings={}):
    """
    function to plot panels containg base and target samples for a sequence result dict.
    """

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(8, 20))
    gs = fig.add_gridspec(nrows=5, ncols=2)

    #
    ax_nuts_direct = fig.add_subplot(gs[0, 1])

    ax_nuts_map_base = fig.add_subplot(gs[1, 0])
    ax_nuts_map_target = fig.add_subplot(gs[1, 1])

    ax_aahmc_map_base = fig.add_subplot(gs[2, 0])
    ax_aahmc_map_target = fig.add_subplot(gs[2, 1])

    ax_real = fig.add_subplot(gs[3, 1])
    ax_real_two = fig.add_subplot(gs[4, 1])

    # Plot data:
    ax_nuts_direct.scatter(
        sequence_result_set["sample_1"][:, 0], sequence_result_set["sample_1"][:, 1]
    )

    # NUTS sampled
    ax_nuts_map_base.scatter(
        sequence_result_set["base_sample_2"][:, 0],
        sequence_result_set["base_sample_2"][:, 1],
    )
    ax_nuts_map_target.scatter(
        sequence_result_set["sample_2"][:, 0], sequence_result_set["sample_2"][:, 1]
    )

    # AAHMC sampled
    ax_aahmc_map_base.scatter(
        sequence_result_set["base_sample_3"][:, 0],
        sequence_result_set["base_sample_3"][:, 1],
    )
    ax_aahmc_map_target.scatter(
        sequence_result_set["sample_3"][:, 0], sequence_result_set["sample_3"][:, 1]
    )

    #
    ax_real.scatter(
        sequence_result_set["real_sample"][:, 0],
        sequence_result_set["real_sample"][:, 1],
    )
    ax_real_two.scatter(
        sequence_result_set["second_real_sample"][:, 0],
        sequence_result_set["second_real_sample"][:, 1],
    )

    #
    real_axes_list = [
        ax_nuts_direct,
        ax_nuts_map_target,
        ax_aahmc_map_target,
        ax_real,
        ax_real_two,
    ]
    align_axes(fig, axes_list=real_axes_list, which_axis="x")
    align_axes(fig, axes_list=real_axes_list, which_axis="y")

    #
    base_axes_list = [ax_nuts_map_base, ax_aahmc_map_base]
    align_axes(fig, axes_list=base_axes_list, which_axis="x")
    align_axes(fig, axes_list=base_axes_list, which_axis="y")

    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    # Get filename
    hmc_data_root = os.path.join(os.getenv("PROJECT_DATA_ROOT"), "hmc_data")
    filename = os.path.join(
        hmc_data_root,
        "aahmc_5d_sampling_comparison",
        "aahmc_5d_sampling_comparison.json",
    )

    # Get outputname
    plot_dir = "plots/"
    basename = "sample_analysis_poster.pdf"

    #
    sequence_results = readout_sequence_results(filename)

    plot_sample_comparison_paper(
        sequence_results=sequence_results,
        add_mean_evolution=False,
        add_variance_evolution=False,
        add_skewness_evolution=False,
        add_kurtosis_evolution=False,
        add_wasserstein_evolution=True,
        add_ESS_evolution=False,
        add_time_evolution=True,
        add_confidence_interval_overlap=False,
        verbose=1,
        log_xscale=True,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(plot_dir, basename),
            "add_second_confidence_interval": False,
            "fontsize_axislabel": 80,
            "fontsize_title": 80,
            "fontsize_legend": 40,
            "size_ticklabels": 60,
            "include_subplot_titles": False,
        },
    )
