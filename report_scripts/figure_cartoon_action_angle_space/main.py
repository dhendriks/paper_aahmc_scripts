"""
Script to plot the cartoon
"""

import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from david_phd_functions.plotting.utils import show_and_save_plot


def plot_figure_cartoon_action_angle_space(plot_settings):
    """
    Function to plot the figure cartoon action angle space
    """

    # This figure will be in XKCD-style
    with plt.xkcd():
        mpl.rcParams["font.family"] = "DejaVu Sans"

        # Vertical
        fig = plt.figure(figsize=(12, 40))
        gs = fig.add_gridspec(nrows=7, ncols=1)

        # Create axes
        ax = fig.add_subplot(gs[0:2, :])

        #
        radii = [0.7, 1.2, 0.9]
        zero_angle = [0.2 * np.pi, 0.7 * np.pi, 0.5 * np.pi]
        angle_shift = [1 * np.pi, 1 * np.pi, 1 * np.pi]
        action_values = np.array([1, 2, 5])

        #
        ax.set_xlabel(
            "Angle ($\Phi$)", fontsize=plot_settings.get("fontsize_axislabel", 24)
        )
        ax.set_ylabel(
            "Action ($J$)", fontsize=plot_settings.get("fontsize_axislabel", 24)
        )

        ax.set_xticks([0, np.pi, 2 * np.pi])
        ax.set_xticklabels(
            [0, r"$\pi$", r"2 $\pi$"],
            fontsize=plot_settings.get("fontsize_ticklabel", 24),
        )

        ax.set_yticks(action_values)
        ax.set_yticklabels(
            [r"$J_{1}$", r"$J_{2}$", r"$J_{n}$"],
            fontsize=plot_settings.get("fontsize_ticklabel", 24),
        )

        # Plot dots
        ax.scatter([np.pi, np.pi, np.pi], [3, 3.5, 4], s=150, c="k", alpha=0.5)

        # Plot points
        for i in range(len(zero_angle)):
            xvals = [zero_angle[i], zero_angle[i] + angle_shift[i]]
            yvals = [action_values[i], action_values[i]]
            dx = (xvals[-1] - xvals[0]) / 2
            dy = (yvals[-1] - yvals[0]) / 2

            # Plot line
            ax.plot(xvals, yvals, "--", c="k", zorder=-1)

            # # Plot arrow
            # plt.arrow(
            #     x=xvals[0],
            #     y=yvals[0],
            #     dx=dx,
            #     dy=dy,
            #     width=0.02,
            #     linestyle=(5, (3, 6)),
            #     color="k",
            # )
            # arrowStart = (xvals[0], yvals[0])
            # arrowStop = (xvals[0] + dx, yvals[0] + dy)

            # ax.annotate(
            #     "",
            #     arrowStop,
            #     xytext=arrowStart,
            #     arrowprops=dict(
            #         linewidth=0,
            #         arrowstyle="-|>",
            #         shrinkA=0,
            #         shrinkB=0,
            #         edgecolor="none",
            #         facecolor="k",
            #         linestyle="solid",
            #     ),
            # )
            # Plot points
            ax.scatter(
                xvals,
                yvals,
                s=200,
                c="k",
            )

            # Plot text
            ax.text(
                xvals[0] + dx - 0.2,
                yvals[0] + dy - 0.5,
                r"d$\Phi_{%s}$" % (["1", "2", "n"][i]),
                fontsize=plot_settings.get("fontsize_ticklabel", 24),
            )

    ax.set_ylim([0, ax.get_xlim()[1]])

    #
    fig.subplots_adjust(top=0.98, bottom=0.05)
    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    plot_dir = "/home/david/Dropbox/Academic/PHD/papers/paper_hmc/paper_tex/figures/action_angle_space_cartoon"
    plot_dir = "plots/"
    base_name = "action_angle_space_cartoon.pdf"

    fontsize = 70
    fontsize_ticklabel = 50

    # Plot and save to paper dir
    plot_figure_cartoon_action_angle_space(
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                plot_dir,
                base_name,
            ),
            "fontsize_axislabel": fontsize,
            "fontsize_ticklabel": fontsize_ticklabel,
        },
    )
