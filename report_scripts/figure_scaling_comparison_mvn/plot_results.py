"""
Function to plot the results of the dimensional scaling
"""

import os

from hmc_project_code.action_angle_scripts.functions.plot_routines.plot_scaling_comparison import (
    plot_scaling_comparison_combined,
    plot_scaling_per_samplesize_per_dimension,
    plot_scaling_samples_per_time,
    plot_scaling_time_per_sample,
)


def plot_all_scaling_comparison_plots_mvn(result_dir, plot_output_dir):
    """
    Function to plot all the scaling plots
    """

    show_plot = False

    # time it took per sample size per dimension
    plot_scaling_per_samplesize_per_dimension(
        grid_result_dir=result_dir,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(
                plot_output_dir, "plot_scaling_per_samplesize_per_dimension.pdf"
            ),
        },
    )

    # Samples taken per time
    plot_scaling_samples_per_time(
        grid_result_dir=result_dir,
        log_xscale=True,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(
                plot_output_dir, "plot_scaling_samples_per_time.pdf"
            ),
        },
    )

    # Time it took per sample
    plot_scaling_time_per_sample(
        grid_result_dir=result_dir,
        log_xscale=True,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(
                plot_output_dir, "plot_scaling_time_per_sample.pdf"
            ),
        },
    )

    # Combined plot
    plot_scaling_comparison_combined(
        grid_result_dir=result_dir,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(
                plot_output_dir, "plot_scaling_comparison_combined.pdf"
            ),
        },
    )


#
if __name__ == "__main__":
    #
    result_dir = os.path.join(
        os.path.dirname(__file__), "results/ultra_res/quarter_phase_shift"
    )

    plot_all_scaling_comparison_plots_mvn(
        result_dir=result_dir,
        plot_output_dir=os.path.join(os.path.dirname(__file__), "plots"),
    )
