"""
Main function to generate the results for the sampler comparison when sampling the MVN
"""

import os

from hmc_project_code.action_angle_scripts.functions.aa_angle_functions import (
    quarter_phase_shift,
)
from hmc_project_code.action_angle_scripts.functions.generate_angle_algorithm_analysis import (
    generate_angle_algorithm_analysis,
)
from hmc_project_code.config import config

# Flags to generate results and plots
generate_results = True
generate_plots = False

# Flags to include samplers
include_hmc = False
include_aa = True

include_mcmc = False
include_nuts = True
include_pyro_hmc = False


############
# Resolution of grid

# ultra res
num_realisations = 5
num_dimensions = [2, 3, 5, 8, 10, 20, 35, 50, 80, 100, 200, 300, 500, 800, 1000]
num_sample_sizes = [1000]
simulation_set_name = "ultra_res"

# # high res
# num_realisations = 5
# num_dimensions = [2, 5, 10, 20, 50, 100]
# num_sample_sizes = [1000]
# simulation_set_name = 'high_res'

# # mid res
# num_realisations = 25
# num_dimensions = [2, 5, 10, 20]
# num_sample_sizes = [1000]
# simulation_set_name = 'mid_res'

# # low res
# num_realisations = 5
# num_dimensions = [2, 5, 10]
# num_sample_sizes = [1000]
# simulation_set_name = 'low_res'

# # Test res
# num_realisations = 1
# num_dimensions = [2, 5]
# num_sample_sizes = [1000]
# simulation_set_name = 'test_res'

# Set the angle methods
angle_method_dict = {
    "quarter_phase_shift": quarter_phase_shift,
}

for angle_shift_method_key, angle_shift_method_value in angle_method_dict.items():
    config = config.copy()

    # Configure the samplers
    config["aa_angle_shift_function"] = angle_shift_method_value
    config["grid_dimensions_array"] = num_dimensions
    config["grid_realisations"] = num_realisations
    config["grid_sample_size_array"] = num_sample_sizes

    config["grid_include_custom_hmc_sampler"] = include_hmc
    config["grid_include_custom_aamc_sampler"] = include_aa
    config["grid_include_pyro_nuts_sampler"] = include_nuts
    config["grid_include_pyro_hmc_sampler"] = include_pyro_hmc
    config["grid_include_pyro_mcmc_sampler"] = include_mcmc

    # Pyro sampling
    config["pyro_n_warmup"] = 500

    #
    main_result_dir = os.path.join(
        os.path.dirname(__file__),
        "results",
        simulation_set_name,
        angle_shift_method_key,
    )
    main_plot_dir = os.path.join(
        os.path.dirname(__file__), "plots", simulation_set_name, angle_shift_method_key
    )

    generate_angle_algorithm_analysis(
        config=config,
        name=angle_shift_method_key,
        result_dir=main_result_dir,
        plot_dir=main_plot_dir,
        generate_results=generate_results,
        generate_plots=generate_plots,
        verbosity=0,
    )
