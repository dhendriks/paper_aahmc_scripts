"""
Main script to analyse the energy balance for the AAHMC sampler.

The set up is as follows:
- First we run a sampling directly on the target, with the AAHMC sampler, where the target == standard MVN
- Then we train a simple map and run the AAHMC on the base + pushing. We also run the map samples here to see what the baseline accuracy is
"""

import json
import logging
import os

import numpy as np

from hmc_project_code.config import config
from hmc_project_code.functions.action_angle.angle_step_methods import (
    quarter_phase_shift,
)
from hmc_project_code.functions.sampling.generate_samples import generate_samples
from hmc_project_code.functions.sampling.utility_functions import mvn_model

with open("results/with_map.json", "r") as f:
    data = json.loads(f.read())

for step_i in data["samplers"]["aahmc_through_map"]["sample_set"]["0"]["orbit_data"][
    "0"
]:
    step_dict = data["samplers"]["aahmc_through_map"]["sample_set"]["0"]["orbit_data"][
        "0"
    ][step_i]

    initial_latent_position = step_dict["latent_data"]["initial_latent_positions"]
    final_latent_position = step_dict["latent_data"]["final_latent_positions"]

    print(initial_latent_position, final_latent_position)

    initial_real_position = step_dict["real_data"]["initial_real_positions"]
    final_real_position = step_dict["real_data"]["final_real_positions"]

    print(initial_real_position, final_real_position)

os.makedirs("results", exist_ok=True)

run_orbit_target_standard_mvn = True
run_orbit_with_map = True

###########################
# configure model
n_dimensions = 2

# Configure
config["sample_size"] = 1
config["n_samples"] = 1
config["n_warmup"] = 0

#
config["logger"].setLevel(logging.DEBUG)

#
config["aahmc_sampler_args"] = {
    "angle_shift_method": quarter_phase_shift,
    "print_energies": False,
    "resolve_orbit": True,
    "resolve_orbit_max_angle": (2.0 / 3.0) * np.pi,
}
config["return_orbit_data"] = True

###########
# Set up standard model
mvn_distribution_specifications = {
    "mean": np.array([0.0, 0.0]),
    "covariance": np.array([[1.0, 0.8], [0.8, 1.0]]),
}
model = mvn_model(mvn_distribution_specifications)

config["num_flows_map"] = 1
config["num_steps_map"] = 5
config["learning_rate_map"] = 1e-3

#
generate_samples(
    model=model,
    dimensions=len(mvn_distribution_specifications["mean"]),
    mvn_distribution_specifications=mvn_distribution_specifications,
    config=config,
    generate_real_samples=False,
    run_nuts_on_target=False,
    run_aahmc_on_target=False,
    generate_map_regardless=False,
    run_map_samples=False,
    run_nuts_on_base_and_push=False,
    run_aahmc_on_base_and_push=True,
    output_filename="results/with_map.json",
    verbose=0,
)
