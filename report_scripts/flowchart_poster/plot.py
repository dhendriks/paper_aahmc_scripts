"""
Plot routine for the a 2-d gaussian

TODO: use the same distributions in a routine that does the mapping and 'integrate the orbit to include points'
"""

import os

import numpy as np
from scipy.stats import multivariate_normal

from hmc_project_code.report_scripts.flowchart_poster.functions import (
    multivariate_gaussian,
    plot_mvn_2_orientations,
    readout_orbit_latent_real,
)

labelfontsize = 50

#
orbit_dict = readout_orbit_latent_real("results/with_map.json")

#############################
# Configuration
plot_output_dir = "plots"

# Our 2-dimensional distribution will be over variables X and Y
N = 200
X = np.linspace(-2, 2, N)
Y = np.linspace(-2, 2, N)
X, Y = np.meshgrid(X, Y)

########
# Standard normal

# Mean vector and covariance matrix
mu = np.array([0.0, 0.0])
Sigma = np.array([[1.0, 0.0], [0.0, 1.0]])

# Pack X and Y into a single 3-dimensional array
pos = np.empty(X.shape + (2,))
pos[:, :, 0] = X
pos[:, :, 1] = Y

# The distribution on the variables X, Y packed into pos.
Z_std = multivariate_gaussian(pos, mu, Sigma)

#
position_data = np.array(orbit_dict["latent"]["initial_positions"])
z_data = multivariate_normal.pdf(position_data, mean=mu, cov=Sigma)

# Create new position data
new_position_data = np.zeros((position_data.shape[0], position_data.shape[1] + 1))
new_position_data[:, :2] = position_data
new_position_data[:, 2] = z_data

#
plot_mvn_2_orientations(
    X,
    Y,
    Z_std,
    include_orbit=False,
    position_data=new_position_data,
    plot_settings={
        "show_plot": False,
        "output_name": os.path.join(plot_output_dir, "base_distribution.pdf"),
        "labelfontsize": labelfontsize,
    },
)

########
# MVN

# Mean vector and covariance matrix
mu = np.array([0.0, 0.0])
Sigma = np.array([[1.0, 0.8], [0.8, 1.0]])

# Pack X and Y into a single 3-dimensional array
pos = np.empty(X.shape + (2,))
pos[:, :, 0] = X
pos[:, :, 1] = Y

# The distribution on the variables X, Y packed into pos.
Z_cov = multivariate_gaussian(pos, mu, Sigma)
plot_mvn_2_orientations(
    X,
    Y,
    Z_cov,
    include_orbit=False,
    position_data=new_position_data,
    plot_settings={
        "show_plot": False,
        "output_name": os.path.join(plot_output_dir, "target_distribution.pdf"),
        "labelfontsize": labelfontsize,
    },
)
