"""
Extra functions for the poster flow chart
"""

import json

import matplotlib.pyplot as plt
import numpy as np
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc
from david_phd_functions.plotting.utils import show_and_save_plot
from matplotlib import cm

load_mpl_rc()


def readout_orbit_latent_real(filename):
    """
    Function to read out orbit in latent and real space
    """

    # Structure for the dict
    orbit_dict = {
        "latent": {
            "initial_positions": [],
            "final_positions": [],
        },
        "real": {
            "initial_positions": [],
            "final_positions": [],
        },
    }

    # read out file
    with open(filename, "r") as f:
        data = json.loads(f.read())

    # loop over orbit steps (for sample 0)
    for step_i in data["samplers"]["aahmc_through_map"]["sample_set"]["0"][
        "orbit_data"
    ]["0"]:
        step_dict = data["samplers"]["aahmc_through_map"]["sample_set"]["0"][
            "orbit_data"
        ]["0"][step_i]

        # read out latent positions
        initial_latent_position = step_dict["latent_data"]["initial_latent_positions"]
        final_latent_position = step_dict["latent_data"]["final_latent_positions"]

        orbit_dict["latent"]["initial_positions"].append(
            initial_latent_position["model_shared_latent"]
        )
        orbit_dict["latent"]["final_positions"].append(
            final_latent_position["model_shared_latent"]
        )

        # Read out real positions
        initial_real_position = step_dict["real_data"]["initial_real_positions"]
        final_real_position = step_dict["real_data"]["final_real_positions"]

        orbit_dict["real"]["initial_positions"].append(initial_real_position["model"])
        orbit_dict["real"]["final_positions"].append(final_real_position["model"])

    #
    return orbit_dict


def multivariate_gaussian(pos, mu, Sigma):
    """Return the multivariate Gaussian distribution on array pos."""

    n = mu.shape[0]
    Sigma_det = np.linalg.det(Sigma)
    Sigma_inv = np.linalg.inv(Sigma)
    N = np.sqrt((2 * np.pi) ** n * Sigma_det)
    # This einsum call calculates (x-mu)T.Sigma-1.(x-mu) in a vectorized
    # way across all the input variables.
    fac = np.einsum("...k,kl,...l->...", pos - mu, Sigma_inv, pos - mu)

    return np.exp(-fac / 2) / N


def plot_mvn_2_orientations(X, Y, Z, include_orbit, position_data, plot_settings):
    """
    Function to plot a MVN in 3-d
    """

    ################
    # Set up figure
    fig = plt.figure(figsize=(30, 30))
    fig.subplots_adjust(hspace=0)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=2, ncols=1)

    ax_side = fig.add_subplot(gs[0, :], projection="3d")
    ax_top = fig.add_subplot(gs[1, :], projection="3d")

    ####
    ax_side.plot_surface(
        X,
        Y,
        Z,
        rstride=3,
        cstride=3,
        linewidth=1,
        antialiased=True,
        cmap=cm.viridis,
    )
    ax_side.view_init(55, -70)
    ax_side.set_xticks([])
    ax_side.set_yticks([])
    ax_side.set_zticks([])
    ax_side.set_xlabel(r"$x_1$", fontsize=plot_settings.get("labelfontsize", 32))
    ax_side.set_ylabel(r"$x_2$", fontsize=plot_settings.get("labelfontsize", 32))

    ####
    ax_top.contourf(X, Y, Z, zdir="z", offset=0, cmap=cm.viridis)
    ax_top.view_init(90, 90)

    ax_top.grid(False)
    ax_top.set_xticks([])
    ax_top.set_yticks([])
    ax_top.set_zticks([])
    ax_top.set_xlabel(r"$x_1$", fontsize=plot_settings.get("labelfontsize", 32))
    ax_top.set_ylabel(r"$x_2$", fontsize=plot_settings.get("labelfontsize", 32))

    # # Handle plotting of the orbit
    # if include_orbit:
    #     z = np.ones(position_data[:, 1].shape) * 10
    #     ax_top.plot(
    #         position_data[:, 0],
    #         position_data[:, 1],
    #     )

    #
    show_and_save_plot(fig=fig, plot_settings=plot_settings)
