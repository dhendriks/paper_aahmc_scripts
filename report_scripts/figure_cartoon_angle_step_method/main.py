"""
Function to plot a cartoon/schematic for the angle step method

- [X] circles in p,q
- [X] align all
- [ ] radii determined by gaussian
"""

import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from david_phd_functions.plotting.custom_mpl_settings import load_mpl_rc
from david_phd_functions.plotting.utils import show_and_save_plot

from hmc_project_code.functions.plotting.utility import align_axes

load_mpl_rc()
mpl.rc("text", usetex=False)

mpl.rc(
    "font",
    weight="medium",
)

fontsize = 120


def plot_figure_cartoon_angle_step_method(orientation="horizontal", plot_settings={}):
    """
    Function to plot the angle step method
    """

    # This figure will be in XKCD-style
    with plt.xkcd():
        mpl.rcParams["font.family"] = "DejaVu Sans"
        #
        if orientation == "vertical":
            # Vertical
            fig = plt.figure(figsize=(12, 40))
            gs = fig.add_gridspec(nrows=7, ncols=1)

            # Create axes
            ax1 = fig.add_subplot(gs[0:2, :])
            ax2 = fig.add_subplot(gs[2:4, :])
            ax3 = fig.add_subplot(gs[4, :])
            ax4 = fig.add_subplot(gs[5:7, :])
        elif orientation == "horizontal":
            # Horizontal
            fig = plt.figure(figsize=(40, 12))
            gs = fig.add_gridspec(nrows=1, ncols=7)

            # Create axes
            ax1 = fig.add_subplot(gs[:, 0:2])
            ax2 = fig.add_subplot(gs[:, 2:4])
            ax3 = fig.add_subplot(gs[:, 4])
            ax4 = fig.add_subplot(gs[:, 5:7])
        else:
            raise ValueError("orientation {} not supported".format(orientation))

        ax1.set_xlabel(r"p$_{0}$", fontsize=plot_settings.get("fontsize_axislabel", 24))
        ax1.set_ylabel(r"q$_{0}$", fontsize=plot_settings.get("fontsize_axislabel", 24))

        ax2.set_xlabel(r"p$_{1}$", fontsize=plot_settings.get("fontsize_axislabel", 24))
        ax2.set_ylabel(r"q$_{1}$", fontsize=plot_settings.get("fontsize_axislabel", 24))

        ax4.set_xlabel(
            r"p$_{\mathrm{N}}$", fontsize=plot_settings.get("fontsize_axislabel", 24)
        )
        ax4.set_ylabel(
            r"q$_{\mathrm{N}}$", fontsize=plot_settings.get("fontsize_axislabel", 24)
        )

        #
        radii = [0.7, 1.2, 0.9]
        zero_angle = [0.2, 0.7, 0.5]

        angle_shift = [1, 1, 1]

        axes_list = [ax1, ax2, ax4]
        angle_array = np.arange(0, 2 * np.pi, 0.01)

        # Loop over the axis
        for axis_i, axis in enumerate(axes_list):
            axis.plot(
                radii[axis_i] * np.cos(angle_array),
                radii[axis_i] * np.sin(angle_array),
            )

            # Remove ticklabels
            axis.set_xticklabels([])
            axis.set_yticklabels([])

            # Draw dot on final position
            cur_angle_arr = np.arange(
                zero_angle[axis_i], zero_angle[axis_i] + angle_shift[axis_i], 0.01
            )
            axis.plot(
                radii[axis_i] * np.cos(cur_angle_arr),
                radii[axis_i] * np.sin(cur_angle_arr),
                c="green",
                linewidth=8,
            )

            # Draw dot on zero position
            axis.scatter(
                radii[axis_i] * np.cos(zero_angle[axis_i]),
                radii[axis_i] * np.sin(zero_angle[axis_i]),
                s=100,
                c="red",
                zorder=10,
            )

            # Draw dot on final position
            axis.scatter(
                radii[axis_i] * np.cos(zero_angle[axis_i] + angle_shift[axis_i]),
                radii[axis_i] * np.sin(zero_angle[axis_i] + angle_shift[axis_i]),
                s=100,
                c="red",
                zorder=10,
            )

            # Draw thin lines to center
            centerline = axis.plot(
                [0, radii[axis_i] * np.cos(zero_angle[axis_i])],
                [0, radii[axis_i] * np.sin(zero_angle[axis_i])],
                zorder=9,
                alpha=0.5,
                linestyle="--",
            )
            axis.plot(
                [0, radii[axis_i] * np.cos(zero_angle[axis_i] + angle_shift[axis_i])],
                [0, radii[axis_i] * np.sin(zero_angle[axis_i] + angle_shift[axis_i])],
                zorder=9,
                alpha=0.5,
                linestyle="--",
                color=centerline[0].get_color(),
            )

            # Get angle halfway of arc:
            halfway_angle = (
                zero_angle[axis_i] + zero_angle[axis_i] + angle_shift[axis_i]
            ) / 2
            quarter_radius = 0.25 * radii[axis_i]
            halfway_radius = 0.5 * radii[axis_i]

            #
            axis.scatter(
                quarter_radius * np.cos(halfway_angle),
                quarter_radius * np.sin(halfway_angle),
                s=100,
                c="red",
                zorder=10,
            )

            #
            axis.text(
                halfway_radius * np.cos(1.25 * np.pi),
                halfway_radius * np.sin(1.25 * np.pi),
                s=r"d$\Phi_{%s}$" % axis_i,
                fontsize=80,
            )

        # Align axes
        align_axes(fig, axes_list=axes_list, which_axis="x")
        align_axes(fig, axes_list=axes_list, which_axis="y")

        # Add plot with 3 dots axis
        if orientation == "vertical":
            ax3.scatter([0, 0, 0], [-1, 0, 1], s=200, c="k")
        if orientation == "horizontal":
            ax3.scatter([-1, 0, 1], [0, 0, 0], s=200, c="k")
        ax3.set_ylim([-2, 2])
        ax3.xaxis.set_visible(False)
        ax3.yaxis.set_visible(False)
        ax3.set_frame_on(False)

    #
    fig.subplots_adjust(top=0.98, bottom=0.05)
    fig.tight_layout()

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    plot_dir = "/home/david/Dropbox/Academic/PHD/papers/paper_hmc/paper_tex/figures/angle_step_cartoon"
    plot_dir = "plots/"
    base_name = "angle_step_cartoon.pdf"

    # Plot and save to paper dir
    plot_figure_cartoon_angle_step_method(
        orientation="vertical",
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(
                plot_dir,
                base_name,
            ),
            "fontsize_axislabel": fontsize,
        },
    )
