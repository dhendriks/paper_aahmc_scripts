"""
Main function to plot the results for the comparison of the angle-step methods. This will only run a comparison between custom HMC, custom AAMC and self-vs-self
"""

import os

from hmc_project_code.projects.action_angle_scripts.functions.functions import (
    generate_sequence_results,
    get_grid_results,
)
from hmc_project_code.projects.action_angle_scripts.functions.plot_routines.plot_sample_comparison_paper import (
    plot_sample_comparison_paper,
)


def plot_figure_sampler_comparison_mvn(result_dir, plot_output_dir):
    """
    Function to plot the sampler comparison with the best
    """

    # Read out the correct items and create a sequence of results
    grid_result_list = get_grid_results(result_dir)

    # Find all the two-d results
    two_dimensional_runs = [res for res in grid_result_list if res["dimensions"] == 2]
    if two_dimensional_runs:
        largest_n_samples = max([res["n_samples"] for res in two_dimensional_runs])
        largest_sample_size_two_dimensional_runs = [
            res for res in two_dimensional_runs if res["n_samples"] == largest_n_samples
        ]

        # Generate sequence results
        sequence_results = generate_sequence_results(
            result_dict_list=largest_sample_size_two_dimensional_runs,
            sample_name_dict={"sample_1": "aa", "sample_2": "pyro_nuts"},
            display_name_dict={"sample_1": "AAHMC", "sample_2": "PYRO NUTS"},
        )

        #
        plot_sample_comparison_paper(
            sequence_results=sequence_results,
            add_mean_evolution=True,
            add_variance_evolution=True,
            add_skewness_evolution=True,
            add_kurtosis_evolution=True,
            add_wasserstein_evolution=True,
            add_ESS_evolution=False,
            add_time_evolution=False,
            verbose=1,
            log_xscale=True,
            plot_settings={
                "show_plot": False,
                "output_name": os.path.join("comparison_AAMC_VS_NUTS.pdf"),
                "add_second_confidence_interval": False,
                "fontsize_axislabel": 50,
                "fontsize_title": 50,
                "fontsize_legend": 28,
                "size_ticklabels": 40,
            },
        )


if __name__ == "__main__":
    #
    plot_output_dir = os.path.join(os.path.dirname(__file__), "plots/")
    result_dir = "/home/david/projects/hmc_project/repo/hmc_project_code/report_scripts/figure_sampler_comparison_mvn/results/high_res/quarter_phase_shift"

    #
    plot_figure_sampler_comparison_mvn(
        result_dir=result_dir, plot_output_dir=plot_output_dir
    )
