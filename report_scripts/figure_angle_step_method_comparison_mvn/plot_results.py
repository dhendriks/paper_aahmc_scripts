"""
Main function to plot the results for the comparison of the angle-step methods.

This will only run a comparison between custom AAMC and self-vs-self
"""

import os

from hmc_project_code.action_angle_scripts.functions.functions import (
    generate_sequence_results,
    get_grid_results,
)
from hmc_project_code.action_angle_scripts.functions.plot_routines.plot_sample_comparison_paper import (
    plot_sample_comparison_paper,
)


def plot_all_angle_method_comparisons_on_MVN(result_dir, plot_output_dir):
    """
    Function to plot all the angle method comparison on MVN
    """

    #
    for subdir in os.listdir(main_result_dir):
        result_dir = os.path.join(main_result_dir, subdir)

        # Read out the correct items and create a sequence of results
        grid_result_list = get_grid_results(result_dir)

        # Find all the two-d results
        two_dimensional_runs = [
            res for res in grid_result_list if res["dimensions"] == 2
        ]
        if two_dimensional_runs:
            largest_n_samples = max([res["n_samples"] for res in two_dimensional_runs])
            largest_sample_size_two_dimensional_runs = [
                res
                for res in two_dimensional_runs
                if res["n_samples"] == largest_n_samples
            ]

            # Generate sequence results
            sequence_results = generate_sequence_results(
                result_dict_list=largest_sample_size_two_dimensional_runs,
                sample_name_dict={"sample_1": "aa"},
                display_name_dict={"sample_1": "AAHMC"},
            )

            #
            plot_sample_comparison_paper(
                sequence_results=sequence_results,
                add_mean_evolution=True,
                add_variance_evolution=True,
                add_skewness_evolution=True,
                add_kurtosis_evolution=True,
                add_wasserstein_evolution=True,
                add_ESS_evolution=False,
                add_time_evolution=False,
                add_confidence_interval_overlap=False,
                verbose=1,
                log_xscale=True,
                plot_settings={
                    "show_plot": False,
                    "output_name": os.path.join(
                        plot_output_dir, "{}_sample_analysis.pdf".format(subdir)
                    ),
                    "runname": "plot_sample_comparison: {} vs {}".format("hmc", "aa"),
                    "add_second_confidence_interval": False,
                    "fontsize_axislabel": 50,
                    "fontsize_title": 50,
                    "fontsize_legend": 28,
                    "size_ticklabels": 40,
                },
            )


if __name__ == "__main__":
    #
    main_result_dir = "/home/david/projects/hmc_project/server_results/generate_paper_results/results/sample_comparison/high_res/"
    plot_output_dir = os.path.join(os.path.dirname(__file__), "plots/")

    #
    plot_all_angle_method_comparisons_on_MVN(
        result_dir=main_result_dir, plot_output_dir=plot_output_dir
    )
