"""
Main function to run all the plotting routines and generate the plots
"""

import os

from hmc_project_code.report_scripts.figure_angle_step_method_comparison_mvn.plot_results import (
    plot_all_angle_method_comparisons_on_MVN,
)
from hmc_project_code.report_scripts.figure_cartoon_angle_step_method.main import (
    plot_figure_cartoon_angle_step_method,
)
from hmc_project_code.report_scripts.figure_sampler_comparison_mvn.plot_results import (
    plot_figure_sampler_comparison_mvn,
)
from hmc_project_code.report_scripts.figure_scaling_comparison_mvn.plot_results import (
    plot_all_scaling_comparison_plots_mvn,
)


def run_all_plotting_routines(
    output_root,
    include_cartoon_plots=True,
    include_angle_step_method_comparison_MVN=True,
    include_sampler_comparison_MVN=True,
    include_scaling_comparison_MVN=True,
):
    """
    Function to plot all the plots for the
    """

    ############
    # Plot cartoons
    if include_cartoon_plots:
        # Plot angle step cartoon
        base_name_angle_step_cartoon = "angle_step_cartoon.pdf"
        plot_figure_cartoon_angle_step_method(
            plot_settings={
                "show_plot": False,
                "output_name": os.path.join(output_root, base_name_angle_step_cartoon),
                "fontsize_axislabel": 60,
            }
        )

    ############
    # Plot angle step method on MVN plots
    if include_angle_step_method_comparison_MVN:
        main_result_dir = "/home/david/projects/hmc_project/server_results/generate_paper_results/results/sample_comparison/high_res/"
        plot_all_angle_method_comparisons_on_MVN(
            result_dir=main_result_dir,
            plot_output_dir=os.path.join(output_root, "angle_step_method_comparison"),
        )

    ############
    # Plot sampler comparison for sampling the MVN
    if include_sampler_comparison_MVN:
        result_dir = "/home/david/projects/hmc_project/repo/hmc_project_code/report_scripts/figure_sampler_comparison_mvn/results/high_res/quarter_phase_shift"
        plot_figure_sampler_comparison_mvn(
            result_dir=result_dir,
            plot_output_dir=os.path.join(output_root, "sampler_comparison"),
        )

    ############
    # Plot sampler comparison for sampling the MVN
    if include_scaling_comparison_MVN:
        result_dir = "/home/david/projects/hmc_project/repo/hmc_project_code/report_scripts/figure_scaling_comparison_mvn/results/ultra_res/quarter_phase_shift"
        plot_all_scaling_comparison_plots_mvn(
            result_dir=result_dir,
            plot_output_dir=os.path.join(output_root, "scaling_comparison"),
        )
