"""
Main project settings
"""

import logging

import numpy as np
from hmc_project_code.config import config
from hmc_project_code.functions.action_angle.angle_step_methods import (
    quarter_phase_shift,
)
from hmc_project_code.functions.sampling.utility_functions import custom_sample_handler

# Create project specific settings
project_config = {
    **config,
    "num_flows_map": 1,
    "learning_rate_map": 5e-3,
    "n_warmup": 500,
    #
    "aahmc_sampler_args": {
        "angle_shift_method": quarter_phase_shift,
        "print_energies": False,
        "resolve_orbit": False,
        "resolve_orbit_max_angle": 1.5 * np.pi,
    },
    "return_orbit_data": False,
    #
    "numdiff_config": {
        "finite_diff_batch": False,
        "method": "symmetric_difference_quotient",
        # "method"          : "first_order_divided_difference",
        "verbose": False,
    },
}
project_config["custom_handle_samples_func"] = custom_sample_handler
project_config["logger"].setLevel(logging.DEBUG)

####################
# distributions

# Simple 2-d
simple_2dMVN_distribution_specifications = {
    "mean": np.array(
        [60.0, 20.0],
        # dtype=np.float64
    ),
    "covariance": np.array(
        [[5.0, 0.0], [0.0, 5.0]],
        # dtype=np.float64
    ),
}

# Simple 3-d
simple_3dMVN_distribution_specifications = {
    "mean": np.array(
        [
            10.0,
            5.0,
            80.0,
        ]
    ),
    "covariance": np.array(
        [
            [5.0, 0.0, 0.0],
            [0.0, 2.0, 0.0],
            [0.0, 0.0, 9.0],
        ]
    ),
}

# Simple 4-d
simple_4dMVN_distribution_specifications = {
    "mean": np.array(
        [
            10.0,
            5.0,
            80.0,
            40.0,
        ]
    ),
    "covariance": np.array(
        [
            [5.0, 0.0, 0.0, 0.0],
            [0.0, 2.0, 0.0, 0.0],
            [0.0, 0.0, 9.0, 0.0],
            [0.0, 0.0, 0.0, 5.0],
        ]
    ),
}

# Simple 5-d
simple_5dMVN_distribution_specifications = {
    "mean": np.array(
        [
            10.0,
            5.0,
            80.0,
            40.0,
            -20.0,
        ]
    ),
    "covariance": np.array(
        [
            [5.0, 0.0, 0.0, 0.0, 0.0],
            [0.0, 2.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, 9.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 5.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, 2.0],
        ]
    ),
}
